# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'THERMEVO Shop',
    'version': '1.0',
    'author': 'Libre Comunication',
    'category': 'THERMEVO Shop',

    'description': """
        Module for processing drawings
        """,

    'depends': [
        'account',
        'delivery',
        'website_sale',
        'profile_management',
        'thermevo_website_management',
    ],

    'data': [
        'security/ir.model.access.csv',

        'data/data.xml',

        'views/layout.xml',
        'views/website_source.xml',
        # 'views/report_invoice.xml',
        # 'views/account_invoice_view.xml',
        'views/report_pricelist.xml',
        'views/delivery_carrier_view.xml',
        'views/print_pricelist.xml',
        'views/employee_commission_views.xml',
        'views/purchase_views.xml',
        'views/sale_order_views.xml',
        'views/res_partner_view.xml',

        'web/product_catalog.xml',
        'web/product_page.xml',
        'web/product_cart.xml',
        'web/delivery_address.xml',
        'web/order.xml',
        'web/quotation.xml',
        'web/price_quotation.xml',
        'web/supplier_order_history.xml',
        'web/supplier_order.xml',
        'web/issued_invoice.xml',
        'web/list_of_order.xml',
        'web/sale_order_list.xml',
        'web/invoice.xml',
        'web/delivery_order_list.xml',
        'web/delivery_order.xml',
        'web/personal_product_catalog.xml',
        'web/agent_product_catalog.xml',
        'web/opportunity_list.xml',
        'web/opportunity.xml',

        'wizard/supplier_pricelist_wizard.xml',
    ],
    'css': ['static/src/css/report.css'],

    'images': ['static/description/icon.png'],
    'installable': True,
    'auto_install': False,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
