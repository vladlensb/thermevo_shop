# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.addons.website_sale.controllers.main import \
    website_sale as odoo_website_sale
from openerp.addons.thermevo_shop.controllers.main import QueryURL
from datetime import datetime
from openerp import SUPERUSER_ID
from openerp.http import Controller, route, request
from openerp import http
import logging
from openerp.addons.email_template import email_template
from openerp.addons.web.controllers.main import content_disposition

_logger = logging.getLogger(__name__)

product_per_page = 200  # Products Per Page
product_per_row = 4  # Products Per Row


def get_filter_name(name):
    if name:
        name = name.lower()
        name = name.replace(' ', '_')
        name = name.replace('%', '')
        name = name.replace('/', '')
    return name


class WebsiteCabinetSale(odoo_website_sale):
    def check_agent(self, pool, cr, uid, context):
        res_users_pool = pool.get('res.users')
        user = res_users_pool.browse(cr, uid, uid, context=context)

        team_pool = pool.get('crm.case.section')
        team_ids = team_pool.search(cr, uid, [])
        teams = team_pool.browse(cr, SUPERUSER_ID, team_ids, context=context)
        agent_teams = []
        team_users = []
        agent = False
        for team in teams:
            if user in team.member_ids:
                agent = True
                agent_teams.append(team.id)
                for member in team.member_ids:
                    if member.id not in team_users:
                        team_users.append(member.id)
        return agent, team_users, agent_teams

    @http.route(['/shop/sale_quotation_list/page/<int:page>',
                 '/shop/sale_quotation_list'],
                type='http', auth="user", website=True)
    def sale_quotation(self, page=1, category=None,
                       orderby='', search='', search_text='', **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry

        keep = QueryURL('/shop/sale_quotation_list',
                        order=orderby, search=search, search_text=search_text)
        res_users_pool = pool.get('res.users')
        sale_order_pool = pool.get('sale.order')
        domain = []

        user = res_users_pool.browse(cr, uid, uid, context=context)
        # if not user.partner_id.supplier and not user.partner_id.customer:
        #     return request.redirect('/page/personal_profile')
        #
        # if not user.partner_id.customer:
        #     return request.redirect('/shop/supplier_order_list')

        partners = [user.partner_id.id] + [user.partner_id.parent_id.id] + \
                   [c.id for c in user.partner_id.child_ids] + \
                   [c.id for c in user.partner_id.parent_id.child_ids]

        partner_ids = [i for i in partners if i]

        agent, team_users, agent_teams = self.check_agent(
            pool, cr, uid, context=context)
        if agent:
            if search == 'agent':
                domain += [('state', 'not in', ('cancel',)),
                           '|', ('partner_id', 'in', partner_ids),
                           ('user_id', '=', uid), ]
            else:
                domain += [('state', 'not in', ('cancel',)),
                           '|', ('partner_id', 'in', partner_ids),
                           ('user_id', 'in', team_users), ]
        else:
            domain += [('partner_id', 'in', partner_ids),
                       ('state', 'not in', ('cancel',))]

        if search_text:
            search_dict = search_text.split(' ')
            sale_order_ids = []
            for search in search_dict:
                search_domain = ['|', '|', '|', '|', '|',
                                 ('name', 'ilike', search),
                                 ('partner_id.name', 'ilike', search),
                                 ('partner_id.city', 'ilike', search),
                                 ('order_line.name', 'ilike', search),
                                 ('order_line.product_id.name', 'ilike',
                                  search),
                                 ('order_line.product_id.article', 'ilike',
                                  search)]
                global_domain = domain + search_domain
                order_ids = sale_order_pool.search(cr, SUPERUSER_ID,
                                                   global_domain)
                for order_id in order_ids:
                    if order_id not in sale_order_ids:
                        sale_order_ids.append(order_id)

        else:
            sale_order_ids = sale_order_pool.search(cr, SUPERUSER_ID, domain)

        sale_orders = sale_order_pool.browse(
            cr, SUPERUSER_ID, sale_order_ids, context=context)
        total_order = len(sale_orders)
        unread_message = []
        for i in sale_orders:
            if i.message_unread == True:
                unread_message.append(i)
        sale_order_list = []
        if orderby == '' or orderby == 'byquotation' or orderby == 'bydate':
            orders = list(reversed(sorted(
                sale_orders, key=lambda quotation: quotation.id)))

            sale_order_list = orders[((page - 1) * self._post_per_page):(
                    self._post_per_page * page)]
        else:
            sale_orders = sale_order_pool.browse(
                cr, SUPERUSER_ID, sale_order_ids, context=context)
            if orderby == 'bystatus':
                get_sale_order_list_total = sorted(
                    sale_orders, key=lambda quotation: quotation.state)
                sale_order_list = get_sale_order_list_total[
                                  ((page - 1) * self._post_per_page):(
                                          self._post_per_page * page)]
            elif orderby == 'bytotal':
                get_sale_order_list_total = sorted(
                    sale_orders, key=lambda quotation: quotation.amount_total)
                sale_order_list = get_sale_order_list_total[
                                  ((page - 1) * self._post_per_page):(
                                          self._post_per_page * page)]

            elif orderby == 'bytotaltaxes':
                get_sale_order_list_total = sorted(
                    sale_orders,
                    key=lambda quotation: quotation.amount_untaxed)
                sale_order_list = get_sale_order_list_total[
                                  ((page - 1) * self._post_per_page):(
                                          self._post_per_page * page)]

        page_url = "/shop/sale_quotation_list"

        if orderby:
            post["orderby"] = orderby
        if search:
            post["search"] = search
        pager = request.website.pager(
            url=page_url,
            total=total_order,
            page=page,
            step=self._post_per_page,
            url_args=post,
        )
        question_obj = pool['price.quotation.questions.faq']
        question_ids = question_obj.search(
            cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        questions = question_obj.browse(
            cr, SUPERUSER_ID, question_ids, context=context)

        tax_total = 0
        total = 0
        for sale in sale_order_list:
            tax_total += sale.amount_untaxed
            total += sale.amount_total

        values = {
            'questions': questions,
            'orders': sale_order_list,
            'pager': pager,
            'keep': keep,
            'orderby': orderby,
            'search': search,
            'tax_total': tax_total,
            'search_text': search_text,
            'total': total,
            'agent': agent,
            'unread_message': len(unread_message)
        }
        return request.website.render("website.sale_quotation", values)

    # @http.route(['/shop/sale_order_list/page/<int:page>',
    #              '/shop/sale_order_list'],
    #             type='http', auth="user", website=True)
    # def sale_order_list(self, page=1, category=None,
    #                     orderby='', search='', search_text='', **post):
    #
    #     cr = request.cr
    #     uid = request.uid
    #     context = request.context
    #     pool = request.registry
    #
    #     keep = QueryURL('/shop/sale_order_list',
    #                     order=orderby, search=search, search_text=search_text)
    #     res_users_pool = pool.get('res.users')
    #     sale_order_pool = pool.get('sale.order')
    #     domain = []
    #     if search_text:
    #         search_domain = ['|', '|', '|',
    #                          ('name', 'ilike', search_text),
    #                          ('partner_id.name', 'ilike', search_text),
    #                          ('partner_id.city', 'ilike', search_text),
    #                          ('order_line.name', 'ilike', search_text)]
    #         domain += search_domain
    #
    #     user = res_users_pool.browse(cr, SUPERUSER_ID, uid, context=context)
    #
    #     # if not user.partner_id.supplier and not user.partner_id.customer:
    #     #     return request.redirect('/page/personal_profile')
    #     #
    #     # if not user.partner_id.customer:
    #     #     return request.redirect('/shop/supplier_order_list')
    #
    #     agent, team_users, agent_teams = self.check_agent(
    #         pool, cr, uid, context=context)
    #
    #     partner_ids = [user.partner_id.id] + \
    #                   [c.id for c in user.partner_id.child_ids] + \
    #                   [c.id for c in user.partner_id.parent_id.child_ids]
    #
    #     if agent:
    #         if search == 'agent':
    #             domain += ['|', ('partner_id', 'in', partner_ids),
    #                        ('user_id', '=', uid),
    #                        ('state', 'not in', ('cancel', ))]
    #         else:
    #             domain += ['|', ('partner_id', 'in', partner_ids),
    #                        ('user_id', 'in', team_users),
    #                        ('state', 'not in', ('cancel', ))]
    #     else:
    #         domain += [('partner_id', 'in', partner_ids),
    #                    ('state', 'not in', ('cancel', ))]
    #
    #     total_order = len(sale_order_pool.search(
    #         cr, SUPERUSER_ID, domain, context=context))
    #     sale_order_list = []
    #     if orderby == '' or orderby == 'byquotation' or orderby == 'bydate':
    #         sale_order_ids = sale_order_pool.search(cr, SUPERUSER_ID, domain)
    #
    #         sale_orders = sale_order_pool.browse(
    #             cr, SUPERUSER_ID, sale_order_ids, context=context)
    #         orders = list(reversed(sorted(
    #             sale_orders, key=lambda quotation: quotation.id)))
    #
    #         sale_order_list = orders[((page - 1) * self._post_per_page):(
    #             self._post_per_page * page)]
    #     else:
    #         sale_order_ids = sale_order_pool.search(
    #             cr, SUPERUSER_ID, domain, context=context)
    #         orders = sale_order_pool.browse(
    #             cr, SUPERUSER_ID, sale_order_ids, context=context)
    #         if orderby == 'bystatus':
    #             get_sale_order_list_total = sorted(
    #                 orders, key=lambda quotation: quotation.state)
    #             sale_order_list = get_sale_order_list_total[
    #                               ((page - 1) * self._post_per_page):(
    #                                   self._post_per_page * page)]
    #         elif orderby == 'bytotal':
    #             get_sale_order_list_total = sorted(
    #                 orders, key=lambda quotation: quotation.amount_total)
    #             sale_order_list = get_sale_order_list_total[
    #                               ((page - 1) * self._post_per_page):(
    #                                   self._post_per_page * page)]
    #
    #         elif orderby == 'bytotaltaxes':
    #             get_sale_order_list_total = sorted(
    #                 orders, key=lambda quotation: quotation.amount_untaxed)
    #             sale_order_list = get_sale_order_list_total[
    #                               ((page - 1) * self._post_per_page):(
    #                                   self._post_per_page * page)]
    #
    #     page_url = "/shop/sale_order_list"
    #
    #     if orderby:
    #         post["orderby"] = orderby
    #     if search:
    #         post["search"] = search
    #     pager = request.website.pager(
    #         url=page_url,
    #         total=total_order,
    #         page=page,
    #         step=self._post_per_page,
    #         url_args=post,
    #     )
    #     question_obj = pool['price.quotation.questions.faq']
    #     question_ids = question_obj.search(
    #         cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
    #     questions = question_obj.browse(
    #         cr, SUPERUSER_ID, question_ids, context=context)
    #
    #     tax_total = 0
    #     total = 0
    #     for sale in sale_order_list:
    #         tax_total += sale.amount_untaxed
    #         total += sale.amount_total
    #
    #     values = {
    #         'questions': questions,
    #         'orders': sale_order_list,
    #         'pager': pager,
    #         'keep': keep,
    #         'orderby': orderby,
    #         'search': search,
    #         'tax_total': tax_total,
    #         'search_text': search_text,
    #         'total': total,
    #         'agent': agent
    #     }
    #     return request.website.render("website.sale_order_list", values)

    @http.route(['/shop/print_saleorder/<int:order_id>'],
                type='http', auth="public", website=True)
    def print_saleorder(self, order_id, **form_data):
        cr = request.cr
        uid = SUPERUSER_ID
        context = request.context
        pool = request.registry
        if order_id:
            pdf = request.registry['report'].get_pdf(
                cr, uid, [order_id], 'sale.report_saleorder', data=None,
                context=context)

            report_xml = http.request.session.model('ir.actions.report.xml')
            report_ids = report_xml.search(
                [('report_name', '=', 'sale.report_saleorder')])
            for report in report_xml.browse(report_ids):
                if not report.download_filename:
                    continue
                objects = http.request.session.model(report.model) \
                    .browse([order_id] or [])
                generated_filename = email_template.mako_template_env \
                    .from_string(report.download_filename) \
                    .render({
                    'objects': objects,
                    'o': objects[:1],
                    'object': objects[:1],
                    'ext': report.report_type.replace('qweb-', ''),
                })
                contentdisposition = content_disposition(generated_filename)
            pdfhttpheaders = [('Content-Type', 'application/pdf'),
                              ('Content-Length', len(pdf)),
                              ('Content-Disposition', contentdisposition)]
            return request.make_response(pdf, headers=pdfhttpheaders)
        else:
            return request.redirect('/shop/order_confirm')

    @http.route(['/shop/print_invoice/<int:invoice_id>'],
                type='http', auth="public", website=True)
    def print_invoice(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        account_invoice_pool = pool.get('account.invoice')
        account_invoice = account_invoice_pool.browse(
            cr, uid, int(post.get('invoice_id')), context=context)
        if account_invoice:
            pdf = request.registry['report'].get_pdf(
                cr, uid, [account_invoice.id], 'account.report_invoice',
                data=None, context=context)
            pdfhttpheaders = [
                ('Content-Type', 'application/pdf'),
                ('Content-Length', len(pdf))]
            return request.make_response(pdf, headers=pdfhttpheaders)
        else:
            return request.redirect(
                "'/shop/invoice/%s' % post.get('invoice_id')")

    @http.route(['/shop/get_invoice'
                 ], type='json', auth="user", website=True)
    def get_invoice(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry

        sale_order = pool.get('sale.order').browse(
            cr, SUPERUSER_ID, int(post.get('sale_order')), context=context)
        invoices = sale_order.invoice_ids
        return_invoice = []
        for invoice in invoices:
            if invoice.state != 'cancel':
                return_invoice.append(invoice)
        answer = ''
        if return_invoice:
            body = u'<table width="800">' \
                   u'    <thead>' \
                   u'        <tr>' \
                   u'            <td align="center" width="200" ' \
                   u'                style="padding-left: 10px;">' \
                   u'                Ref. Document' \
                   u'            </td>' \
                   u'            <td align="center" width="150" ' \
                   u'                style="padding-left: 10px;">' \
                   u'                Date of Invoice' \
                   u'            </td>' \
                   u'            <td align="center" width="150" ' \
                   u'                style="padding-left: 10px;">' \
                   u'                Due Date' \
                   u'            </td>' \
                   u'            <td align="center" width="150" ' \
                   u'                style="padding-left: 10px;">' \
                   u'                Balance' \
                   u'            </td>' \
                   u'            <td align="center" width="100" ' \
                   u'                style="padding-left: 10px;">' \
                   u'                Status' \
                   u'            </td>' \
                   u'            <td align="center" width="50" ' \
                   u'                style="padding-left: 10px;"/>' \
                   u'        </tr>' \
                   u'    </thead>' \
                   u'    <tbody>' \
                   u'        %s' \
                   u'    </tbody>' \
                   u'</table>'
            res_table = u''
            for invoice in return_invoice:
                state = invoice.state
                state_val = dict(invoice._columns['state'].selection).get(
                    state)
                table = u'<tr>' \
                        u'            <td align="center" ' \
                        u'                style="padding-left: 10px;">' \
                        u'                %s' \
                        u'            </td>' \
                        u'            <td align="center" ' \
                        u'                style="padding-left: 10px;">' \
                        u'                %s' \
                        u'            </td>' \
                        u'            <td align="center" ' \
                        u'                style="padding-left: 10px;">' \
                        u'                %s' \
                        u'            </td>' \
                        u'            <td align="center" ' \
                        u'                style="padding-left: 10px;">' \
                        u'                %s' \
                        u'            </td>' \
                        u'            <td align="center" ' \
                        u'                style="padding-left: 10px;">' \
                        u'                %s' \
                        u'            </td>' \
                        u'            <td align="center" ' \
                        u'                style="padding-left: 10px;">' \
                        u'                <a href="%s" ' \
                        u'                    target="_blank" ' \
                        u'                    id="invoice_print">Print</a>' \
                        u'            </td>' \
                        u'        </tr>'

                number = u'-'
                date_invoice = u'-'
                date_due = u'-'
                if invoice.number:
                    number = invoice.number
                if invoice.date_invoice:
                    date_invoice = invoice.date_invoice
                if invoice.date_due:
                    date_due = invoice.date_due
                link = '/shop/print_invoice/%s' % invoice.id
                res_table += table % (number, date_invoice, date_due,
                                      invoice.amount_total, state_val, link)
            answer = body % res_table
        return {'return_invoice': answer}

    @http.route(['/shop/get_delivery'
                 ], type='json', auth="user", website=True)
    def get_delivery(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry

        sale_order = pool.get('sale.order').browse(
            cr, SUPERUSER_ID, int(post.get('sale_order')), context=context)
        stock_picking = sale_order.picking_ids
        return_picking = []
        for picking in stock_picking:
            if picking.state != 'cancel':
                return_picking.append(picking)
        answer = ''
        if return_picking:
            body = u'<table width="800">' \
                   u'    <thead>' \
                   u'        <tr>' \
                   u'            <td align="center" width="150" ' \
                   u'                style="padding-left: 10px;">' \
                   u'                Name' \
                   u'            </td>' \
                   u'            <td align="center" width="125" ' \
                   u'                style="padding-left: 10px;">' \
                   u'                Create Date' \
                   u'            </td>' \
                   u'            <td align="center" width="125" ' \
                   u'                style="padding-left: 10px;">' \
                   u'                Schedule Date' \
                   u'            </td>' \
                   u'            <td align="center" width="125" ' \
                   u'                style="padding-left: 10px;">' \
                   u'                Transfer Date' \
                   u'            </td>' \
                   u'            <td align="center" width="125" ' \
                   u'                style="padding-left: 10px;">' \
                   u'                Backorder' \
                   u'            </td>' \
                   u'            <td align="center" width="100" ' \
                   u'                style="padding-left: 10px;">' \
                   u'                Status' \
                   u'            </td>' \
                   u'            <td align="center" width="50" ' \
                   u'                style="padding-left: 10px;"/>' \
                   u'        </tr>' \
                   u'    </thead>' \
                   u'    <tbody>' \
                   u'        %s' \
                   u'    </tbody>' \
                   u'</table>'
            res_table = u''
            for picking in return_picking:
                state = picking.state
                state_val = dict(picking._columns['state'].selection).get(
                    state)
                table = u'<tr>' \
                        u'            <td align="center" ' \
                        u'                style="padding-left: 10px;">' \
                        u'                %s' \
                        u'            </td>' \
                        u'            <td align="center" ' \
                        u'                style="padding-left: 10px;">' \
                        u'                %s' \
                        u'            </td>' \
                        u'            <td align="center" ' \
                        u'                style="padding-left: 10px;">' \
                        u'                %s' \
                        u'            </td>' \
                        u'            <td align="center" ' \
                        u'                style="padding-left: 10px;">' \
                        u'                %s' \
                        u'            </td>' \
                        u'            <td align="center" ' \
                        u'                style="padding-left: 10px;">' \
                        u'                %s' \
                        u'            </td>' \
                        u'            <td align="center" ' \
                        u'                style="padding-left: 10px;">' \
                        u'                %s' \
                        u'            </td>' \
                        u'            <td align="center" ' \
                        u'                style="padding-left: 10px;">' \
                        u'                <a href="%s" ' \
                        u'                    target="_blank" ' \
                        u'                    id="invoice_print">Print</a>' \
                        u'            </td>' \
                        u'        </tr>'
                min_date = u'-'
                date_done = u'-'
                backorder = u'-'
                if picking.min_date:
                    min_date = picking.min_date
                if picking.date_done:
                    date_done = picking.date_done
                if picking.backorder_id:
                    backorder = picking.backorder_id.name
                link = '/shop/print_picking/%s' % picking.id
                res_table += table % (picking.name, picking.date, min_date,
                                      date_done, backorder, state_val, link)
            answer = body % res_table
        return {'return_invoice': answer}

    @http.route(['/shop/print_picking/<int:picking_id>'],
                type='http', auth="public", website=True)
    def print_picking(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        account_picking_pool = pool.get('stock.picking')
        picking = account_picking_pool.browse(
            cr, uid, int(post.get('picking_id')), context=context)
        if picking:
            pdf = request.registry['report'].get_pdf(
                cr, uid, [picking.id], 'stock.report_picking',
                data=None, context=context)
            pdfhttpheaders = [
                ('Content-Type', 'application/pdf'),
                ('Content-Length', len(pdf))]
            return request.make_response(pdf, headers=pdfhttpheaders)
        else:
            return request.redirect("/shop/sale_order_list")

    @http.route(['/shop/customer_invoice_list/page/<int:page>',
                 '/shop/customer_invoice_list'
                 ], type='http', auth="user", website=True)
    def invoice_list(self, page=1, category=None, orderby='', search='',
                     search_text='', **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        if not search:
            search = 'incoming'
        domain = []
        if search_text:
            search_domain = ['|', '|', '|', '|', '|', '|',
                             ('number', 'ilike', search_text),
                             ('origin', 'ilike', search_text),
                             ('origin', '=', search_text),
                             ('partner_id.name', 'ilike', search_text),
                             ('partner_id.city', 'ilike', search_text),
                             ('invoice_line.product_id.name',
                              'ilike', search_text),
                             ('invoice_line.name', 'ilike', search_text)]
            domain += search_domain

        keep = QueryURL('/shop/customer_invoice_list',
                        search=search, search_text=search_text)
        res_users_pool = pool.get('res.users')
        account_invoice_pool = pool.get('account.invoice')

        user = res_users_pool.browse(cr, SUPERUSER_ID, uid, context=context)

        invoice_list = []
        order_state_dic = {}

        agent, team_users, agent_teams = self.check_agent(
            pool, cr, uid, context=context)

        partner_ids = [user.partner_id.id] + [user.partner_id.parent_id.id] + \
                      [c.id for c in user.partner_id.child_ids] + \
                      [c.id for c in user.partner_id.parent_id.child_ids]

        if agent:
            if search == 'agent':
                domain += ['|', ('partner_id', '=', user.partner_id.id),
                           ('user_id', '=', uid)]
            else:

                domain += ['|', '|', ('partner_id', '=', user.partner_id.id),
                           ('partner_id', 'in', partner_ids),
                           ('user_id', 'in', team_users)]
        else:
            domain += ['|',
                       ('partner_id', '=', user.partner_id.id),
                       ('partner_id', 'in', partner_ids), ]

        journal_pool = pool.get('account.journal')
        if search == 'incoming':
            journal_id = journal_pool.search(
                cr, SUPERUSER_ID, [('type', '=', 'sale')])
        else:
            journal_id = journal_pool.search(
                cr, SUPERUSER_ID, [('type', '=', 'purchase')])

        domain += [('journal_id', 'in', journal_id)]
        invoice_ids = account_invoice_pool.search(
            cr, SUPERUSER_ID, domain, context=context)
        total = len(invoice_ids)
        invoices = account_invoice_pool.browse(
            cr, SUPERUSER_ID, invoice_ids, context=context)
        if orderby == '':
            i = list(reversed(sorted(
                invoices, key=lambda inv: inv.id)))

            invoice_list = i[((page - 1) * self._post_per_page):(
                    self._post_per_page * page)]
        elif orderby == 'byinvoicenumber':
            i = (sorted(
                invoices, key=lambda inv: inv.name))

            invoice_list = i[((page - 1) * self._post_per_page):(
                    self._post_per_page * page)]

        elif orderby == 'bydate':
            i = (sorted(
                invoices, key=lambda inv: inv.date_invoice))

            invoice_list = i[((page - 1) * self._post_per_page):(
                    self._post_per_page * page)]
        elif orderby == 'bybalance':
            i = (sorted(
                invoices, key=lambda inv: inv.amount_total))

            invoice_list = i[((page - 1) * self._post_per_page):(
                    self._post_per_page * page)]

        for invoice in invoice_list:
            state = ''
            if invoice.date_invoice:
                date_invoice = invoice.date_invoice
                date_due = invoice.date_due
                date_format = "%Y-%m-%d"
                if date_due:
                    diff_days = (datetime.strptime(
                        date_due, date_format) -
                                 datetime.strptime(
                                     date_invoice, date_format)).days
                else:
                    diff_days = 0

                if diff_days == 0:
                    state = "Draft"
                elif diff_days < 0:
                    state = "Rejected"

            credit_bal = 0.0
            for payment in invoice.payment_ids:
                credit_bal += payment.credit

            if state != 'Rejected':
                if invoice.state == 'draft':
                    state = 'Draft'
                elif credit_bal == 0.0:
                    state = 'Wait for payment'
                elif credit_bal == invoice.amount_total:
                    state = 'Full shipping'
                elif credit_bal < invoice.amount_total:
                    state = 'Part shipping'
            order_state_dic[invoice.id] = state

        if search:
            post["search"] = search
        if orderby:
            post["orderby"] = orderby
        if search_text:
            post['search_text'] = search_text

        page_url = "/shop/customer_invoice_list"
        pager = request.website.pager(
            url=page_url,
            total=total,
            page=page,
            step=self._post_per_page,
            url_args=post,
        )

        question_obj = pool['issued.invoices.questions.faq']
        question_ids = question_obj.search(
            cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        questions = question_obj.browse(
            cr, SUPERUSER_ID, question_ids, context=context)

        tax_total = 0
        total = 0
        for invoice in invoice_list:
            tax_total += invoice.amount_untaxed
            total += invoice.amount_total

        values = {
            'questions': questions,
            'invoices': invoice_list,
            'pager': pager,
            'state': order_state_dic,
            'keep': keep,
            'search': search,
            'orderby': orderby,
            'search_text': search_text,
            'total': total,
            'tax_total': tax_total,
            'agent': agent,
        }
        return request.website.render("website.issued_invoice", values)

    @http.route(['/shop/invoice/<int:invoice_id>'],
                type='http', auth="user", website=True)
    def invoice_page(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        account_invoice_pool = pool.get('account.invoice')
        account_invoice = account_invoice_pool.browse(
            cr, uid, int(post.get('invoice_id')), context=context)

        res_user = pool.get('res.users')
        res_user_odj = res_user.browse(cr, uid, uid, context=context)

        values = {
            'user': res_user_odj,
            'account_invoice': account_invoice,
        }

        return request.website.render("website.invoice", values)

    @http.route(['/shop/supplier_order_list/page/<int:page>',
                 '/shop/supplier_order_list'],
                type='http', auth="user", website=True)
    def supplier_order(self, page=1, category=None, search='', **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry

        keep = QueryURL('/shop/supplier_order_list', search=search)
        res_users_pool = pool.get('res.users')
        purchase_order_pool = pool.get('purchase.order')

        user = res_users_pool.browse(cr, uid, uid, context=context)
        # if not user.partner_id.supplier and not user.partner_id.customer:
        #     return request.redirect('/page/personal_profile')
        # if not user.partner_id.supplier:
        #     return request.redirect('/shop/sale_quotation_list')

        total_order = len(purchase_order_pool.search(
            cr, uid, [('partner_id', '=', user.partner_id.id)],
            context=context))

        supplier_order_list = []
        if search == '' or search == 'byquotation' or search == 'bydate':

            partner_order_ids = purchase_order_pool.search(
                cr, uid, [('partner_id', '=', user.partner_id.id)],
                offset=(page - 1) * self._post_per_page,
                limit=self._post_per_page,
                context=context)

            sale_order_ids = (partner_order_ids or [])

            sale_order_ids = reversed(sorted(sale_order_ids))
            for order in sale_order_ids:
                orders = purchase_order_pool.browse(cr, uid, order,
                                                    context=context)
                supplier_order_list.append(orders)

        else:
            partner_order_ids = purchase_order_pool.search(cr, uid, [
                ('partner_id', '=', user.partner_id.id)], context=context)

            user_order_ids = purchase_order_pool.search(cr, uid, [
                ('user_id', '=', uid)], context=context)
            sale_order_ids = (partner_order_ids or []) + (user_order_ids or [])
            orders = purchase_order_pool.browse(
                cr, uid, sale_order_ids, context=context)
            if search == 'bystatus':
                get_supplier_order_list_total = sorted(
                    orders, key=lambda quotation: quotation.state)
                supplier_order_list = get_supplier_order_list_total[
                                      ((page - 1) * self._post_per_page):(
                                              self._post_per_page * page)]
            elif search == 'bytotal':
                get_supplier_order_list_total = sorted(
                    orders, key=lambda quotation: quotation.amount_total)
                supplier_order_list = get_supplier_order_list_total[
                                      ((page - 1) * self._post_per_page):(
                                              self._post_per_page * page)]

            elif search == 'bytotaltaxes':
                get_supplier_order_list_total = sorted(
                    orders, key=lambda quotation: quotation.amount_untaxed)
                supplier_order_list = get_supplier_order_list_total[
                                      ((page - 1) * self._post_per_page):(
                                              self._post_per_page * page)]

        if search:
            post["search"] = search

        page_url = "/shop/supplier_order_list"
        pager = request.website.pager(
            url=page_url,
            total=total_order,
            page=page,
            step=self._post_per_page,
            url_args=post,
        )
        question_obj = pool['price.quotation.questions.faq']
        question_ids = question_obj.search(
            cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        questions = question_obj.browse(
            cr, SUPERUSER_ID, question_ids, context=context)
        values = {
            'questions': questions,
            'orders': supplier_order_list,
            'pager': pager,
            'keep': keep,
            'search': search,
        }
        return request.website.render("website.supplier_order_history", values)

    @http.route([
        "/shop/show_supplier_order/<int:order_id>",
        "/shop/show_supplier_order/<int:order_id>/<token>"
    ], type='http', auth="user", website=True)
    def show_supplier_order(self, order_id, token=None, message=False, **post):
        res_users_pool = request.registry.get('res.users')
        user = res_users_pool.browse(request.cr, request.uid,
                                     request.uid, context=request.context)
        order = request.registry.get('purchase.order').browse(
            request.cr, SUPERUSER_ID, order_id, request.context)
        if order.partner_id.id != user.partner_id.id:
            return request.redirect('/shop/supplier_order_list')
        mail_message_pool = request.registry.get('mail.message')
        mail_message_id = mail_message_pool.search(
            request.cr, SUPERUSER_ID,
            [('model', '=', 'purchase.order'),
             ('res_id', '=', order.id),
             ('type', '=', 'comment')])
        mail_message = mail_message_pool.browse(
            request.cr, SUPERUSER_ID, mail_message_id, request.context)
        values = {
            'order': order,
            'messages': mail_message,
        }

        return request.website.render('website.supplier_order', values)

    @http.route(['/shop/supplier_order/show_comment/<int:message_id>/'],
                type='json', auth="user", website=True)
    def show_comment(self, message_id, **post):
        cr = request.cr
        context = request.context
        pool = request.registry
        if message_id:
            try:
                message = pool.get('mail.message').browse(
                    cr, SUPERUSER_ID, int(message_id), context=context)

                return_message = message.body
                for attachment in message.attachment_ids:
                    download_link = '%s/web/binary/saveas?model=' \
                                    'ir.attachment&field=datas&filename_field' \
                                    '=name&id=%s' % (
                                        request.website.name, attachment.id)
                    link = '<a href="%s">%s</a>' % (
                        download_link, attachment.name)
                    return_message += '\n' + link

                return return_message
            except:
                pass
        return True

    @http.route(['/shop/supplier_order/update'],
                type='http', auth="user", website=True,
                methods=['GET', 'POST'])
    def update_supplier_order(self, **post):
        order_id = int(post.get('purchase_order'))
        attachment_pool = request.registry.get('ir.attachment')
        purchase_order_pool = request.registry.get('purchase.order')
        order = purchase_order_pool.browse(
            request.cr, SUPERUSER_ID, order_id, request.context)
        value = {}
        attach_value = {
            'type': 'binary',
            'res_model': 'purchase.order',
            'res_id': order_id,
        }

        if post.get('purchase_confirmation_document'):
            purchase_confirmation_document = post.get(
                'purchase_confirmation_document')
            file_name = purchase_confirmation_document.filename
            attach_value['name'] = file_name
            attach_value['res_name'] = file_name
            attach_value[
                'datas'] = purchase_confirmation_document.read().encode(
                'base64')

            attachment_id = attachment_pool.create(
                request.cr, request.uid, attach_value, request.context)
            value['purchase_confirmation_document'] = attachment_id
        if post.get('date_confirmation'):
            date_confirmation = post.get('date_confirmation')
            value['date_confirmation'] = datetime.strptime(
                date_confirmation.replace('-', ''), "%Y%m%d").date()

        if post.get('delivery_document'):
            delivery_document = post.get('delivery_document')
            file_name = delivery_document.filename
            attach_value['name'] = file_name
            attach_value['res_name'] = file_name
            attach_value['datas'] = delivery_document.read().encode('base64')

            attachment_id = attachment_pool.create(
                request.cr, request.uid, attach_value, request.context)

            purchase_delivery_document_pool = request.registry.get(
                'purchase.delivery.document')

            val = {'purchase_order_id': order_id,
                   'attachment_id': attachment_id, }
            if post.get('scheduled_date'):
                scheduled_date = post.get('scheduled_date')
                val['scheduled_date'] = datetime.strptime(
                    scheduled_date.replace('-', ''), "%Y%m%d").date()

            delivery_doc_id = purchase_delivery_document_pool.create(
                request.cr, request.uid, val, request.context)

            delivery_doc_ids = [a.id for a in order.delivery_document]
            delivery_doc_ids += [delivery_doc_id]
            value['delivery_document'] = [(6, 0, delivery_doc_ids)]

        if post.get('invoice_document'):
            invoice_document = post.get('invoice_document')
            file_name = invoice_document.filename
            attach_value['name'] = file_name
            attach_value['res_name'] = file_name
            attach_value['datas'] = invoice_document.read().encode('base64')
            attachment_id = attachment_pool.create(
                request.cr, request.uid, attach_value, request.context)

            purchase_invoice_document_pool = request.registry.get(
                'purchase.invoice.document')

            val = {'purchase_order_id': order_id,
                   'attachment_id': attachment_id, }
            invoice_doc_id = purchase_invoice_document_pool.create(
                request.cr, request.uid, val, request.context)

            invoice_doc_ids = [a.id for a in order.invoice_document]
            invoice_doc_ids += [invoice_doc_id]
            value['invoice_document'] = [(6, 0, invoice_doc_ids)]
        if value:
            purchase_order_pool.write(request.cr, SUPERUSER_ID, order_id,
                                      value, request.context)

        return request.redirect('/shop/show_supplier_order/%s' % order.id)

    @http.route(['/shop/supplier_order/add_comment/<int:order_id>/'],
                type='http', auth="user", website=True)
    def add_comment(self, order_id, **post):
        if post.get('comment'):
            message_pool = request.registry.get('mail.message')
            comment = post.get('comment')
            # comment = u''.join(comment.decode('utf-8'))
            partner_id = request.registry.get('res.users').browse(
                request.cr, SUPERUSER_ID, request.uid,
                request.context).partner_id.id
            values = {
                'type': 'comment',
                'model': 'purchase.order',
                'website_published': True,
                'res_id': order_id,
                'body': comment.encode('utf8'),
                'author_id': partner_id
            }
            message_id = message_pool.create(
                request.cr, SUPERUSER_ID, values, request.context)

            if post.get('comment_document'):
                attachment_pool = request.registry.get('ir.attachment')
                comment_document = post.get('comment_document')
                file_name = comment_document.filename

                attach_value = {
                    'name': file_name,
                    'res_name': file_name,
                    'datas': comment_document.read().encode('base64'),
                    'type': 'binary',
                    'res_model': 'mail.message',
                    'res_id': message_id,
                }
                attachment_id = attachment_pool.create(
                    request.cr, request.uid, attach_value, request.context)
                message_pool.write(
                    request.cr, request.uid, message_id,
                    {'attachment_ids': [(6, 0, [attachment_id])]},
                    request.context)

        return request.redirect('/shop/show_supplier_order/%s' % order_id)

    @http.route(['/shop/supplier_order/add_scheduled_date/'],
                type='json', auth="user", website=True)
    def add_scheduled_date(self, **post):
        document_scheduled_date = post.get('document_scheduled_date')
        if document_scheduled_date:
            scheduled_date = datetime.strptime(
                document_scheduled_date.replace('-', ''), "%Y%m%d").date()
            purchase_delivery_document_pool = request.registry.get(
                'purchase.delivery.document')
            purchase_delivery_document_pool.write(
                request.cr, SUPERUSER_ID,
                int(post.get('delivery_document_id')),
                {'scheduled_date': scheduled_date}, request.context)
            res = '<span>%s</span>' % scheduled_date.strftime('%d/%m/%Y')
            return {'scheduled_date': res}
        return {}

    @http.route(['/shop/delivery_order_list/page/<int:page>',
                 '/shop/delivery_order_list'],
                type='http', auth="user", website=True)
    def delivery_order_list(self, page=1, category=None,
                            orderby='', search='', search_text='', **post):

        if not orderby:
            orderby = 'byorigin'

        if not search:
            search = 'outgoing'

        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry

        keep = QueryURL('/shop/sale_quotation_list',
                        order=orderby, search_text=search_text)
        res_users_pool = pool.get('res.users')
        stock_picking_pool = pool.get('stock.picking')
        domain = [('state', '!=', 'cancel')]
        if search_text:
            search_domain = ['|', '|',
                             ('name', 'ilike', search_text),
                             ('partner_id.name', 'ilike', search_text),
                             ('origin', 'ilike', search_text)]
            domain += search_domain

        user = res_users_pool.browse(cr, SUPERUSER_ID, uid, context=context)

        # if not user.partner_id.supplier and not user.partner_id.customer:
        #     return request.redirect('/page/personal_profile')
        #
        # if not user.partner_id.customer:
        #     return request.redirect('/shop/supplier_order_list')

        agent, team_users, agent_teams = self.check_agent(
            pool, cr, uid, context=context)
        partner_ids = [user.partner_id.id] + [user.partner_id.parent_id.id] + \
                      [c.id for c in user.partner_id.child_ids] + \
                      [c.id for c in user.partner_id.parent_id.child_ids]
        if agent:
            if search == 'agent':
                domain += ['|', ('partner_id', '=', user.partner_id.id),
                           ('create_uid', '=', uid)]
            else:

                domain += ['|', '|', ('partner_id', '=', user.partner_id.id),
                           ('partner_id', 'in', partner_ids),
                           ('create_uid', 'in', team_users)]
        else:
            domain += ['|',
                       ('partner_id', '=', user.partner_id.id),
                       ('partner_id', 'in', partner_ids), ]

        picking_type_pool = pool.get('stock.picking.type')
        if search == 'incoming':
            picking_type_id = picking_type_pool.search(
                cr, SUPERUSER_ID, [('code', '=', 'incoming')])
        else:
            picking_type_id = picking_type_pool.search(
                cr, SUPERUSER_ID, [('code', '=', 'outgoing')])

        domain += [('picking_type_id', 'in', picking_type_id),
                   ('origin', 'not ilike', 'Transit Location')]
        total_stock_picking = len(stock_picking_pool.search(
            cr, SUPERUSER_ID, domain, context=context))
        sale_order_list = []
        if orderby == 'byquotation' or orderby == 'bydate':
            stock_picking_ids = stock_picking_pool.search(
                cr, SUPERUSER_ID, domain)

            stock_pickings = stock_picking_pool.browse(
                cr, SUPERUSER_ID, stock_picking_ids, context=context)
            pickings = list(reversed(sorted(
                stock_pickings, key=lambda quotation: quotation.id)))

            stock_picking_list = pickings[((page - 1) * self._post_per_page):(
                    self._post_per_page * page)]
        else:
            stock_picking_ids = stock_picking_pool.search(
                cr, SUPERUSER_ID, domain, context=context)
            stock_pickings = stock_picking_pool.browse(
                cr, SUPERUSER_ID, stock_picking_ids, context=context)
            if orderby == 'bystatus':
                get_stock_picking_total = sorted(
                    stock_pickings, key=lambda quotation: quotation.state)
                stock_picking_list = get_stock_picking_total[
                                     ((page - 1) * self._post_per_page):(
                                             self._post_per_page * page)]
            else:
                get_stock_picking_total = sorted(
                    stock_pickings, key=lambda quotation: quotation.origin)
                stock_picking_list = get_stock_picking_total[
                                     ((page - 1) * self._post_per_page):(
                                             self._post_per_page * page)]

        page_url = "/shop/delivery_order_list"

        if orderby:
            post["orderby"] = orderby
        if search:
            post["search"] = search
        pager = request.website.pager(
            url=page_url,
            total=total_stock_picking,
            page=page,
            step=self._post_per_page,
            url_args=post,
        )
        question_obj = pool['price.quotation.questions.faq']
        question_ids = question_obj.search(
            cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        questions = question_obj.browse(
            cr, SUPERUSER_ID, question_ids, context=context)

        tax_total = 0
        total = 0
        for sale in sale_order_list:
            tax_total += sale.amount_untaxed
            total += sale.amount_total

        values = {
            'questions': questions,
            'stock_picking_list': stock_picking_list,
            'pager': pager,
            'keep': keep,
            'orderby': orderby,
            'search': search,
            'tax_total': tax_total,
            'search_text': search_text,
            'total': total,
            'agent': agent,
        }
        return request.website.render("website.delivery_order_list", values)

    @http.route(['/shop/delivery_order/<int:delivery_id>'],
                type='http', auth="user", website=True)
    def delivery_order(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        stock_picking_pool = pool.get('stock.picking')
        stock_picking = stock_picking_pool.browse(
            cr, SUPERUSER_ID, int(post.get('delivery_id')), context=context)
        res_user = pool.get('res.users')
        res_user_odj = res_user.browse(cr, uid, uid, context=context)

        values = {
            'user': res_user_odj,
            'stock_picking': stock_picking,
        }

        return request.website.render("website.delivery_order", values)

    @http.route(['/shop/opportunity_list/page/<int:page>',
                 '/shop/opportunity_list'],
                type='http', auth="user", website=True)
    def opportunity_list(self, page=1,
                         orderby='', search='', search_text='', **post):

        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry

        keep = QueryURL('/shop/opportunity_list',
                        order=orderby, search=search, search_text=search_text)
        res_users_pool = pool.get('res.users')
        crm_lead_pool = pool.get('crm.lead')
        domain = [('type', '=', 'opportunity')]
        if search_text:
            search_domain = ['|', '|', '|', '|', '|', '|', '|', '|',
                             ('email_from', 'ilike', search_text),
                             ('name', 'ilike', search_text),
                             ('street', 'ilike', search_text),
                             ('street2', 'ilike', search_text),
                             ('phone', 'ilike', search_text),
                             ('partner_name', 'ilike', search_text),
                             ('contact_name', 'ilike', search_text),
                             ('city', 'ilike', search_text),
                             ('zip', 'ilike', search_text)]
            domain += search_domain

        user = res_users_pool.browse(cr, SUPERUSER_ID, uid, context=context)

        # if not user.partner_id.supplier and not user.partner_id.customer:
        #     return request.redirect('/page/personal_profile')
        #
        # if not user.partner_id.customer:
        #     return request.redirect('/shop/supplier_order_list')

        agent, team_users, agent_teams = self.check_agent(
            pool, cr, uid, context=context)

        partner_ids = [user.partner_id.id] + [user.partner_id.parent_id.id] + \
                      [c.id for c in user.partner_id.child_ids] + \
                      [c.id for c in user.partner_id.parent_id.child_ids]

        if agent:
            if search == 'agent':
                domain += ['|', ('partner_id', 'in', partner_ids),
                           ('user_id', '=', uid)]
            else:
                domain += ['|', ('partner_id', '=', user.partner_id.id),
                           ('user_id', 'in', team_users)]
        else:
            domain += [('partner_id', 'in', partner_ids)]
        total_order = crm_lead_pool.search_count(
            cr, SUPERUSER_ID, domain, context=context)
        sale_order_list = []
        if orderby == '':
            sale_order_ids = crm_lead_pool.search(cr, SUPERUSER_ID, domain)

            sale_orders = crm_lead_pool.browse(
                cr, SUPERUSER_ID, sale_order_ids, context=context)
            orders = list(reversed(sorted(
                sale_orders, key=lambda quotation: quotation.id)))

            sale_order_list = orders[((page - 1) * self._post_per_page):(
                    self._post_per_page * page)]
        else:
            sale_order_ids = crm_lead_pool.search(
                cr, SUPERUSER_ID, domain, context=context)
            orders = crm_lead_pool.browse(
                cr, SUPERUSER_ID, sale_order_ids, context=context)

            if orderby == 'by_name':
                get_sale_order_list_total = sorted(
                    orders, key=lambda quotation: quotation.name)
                sale_order_list = get_sale_order_list_total[
                                  ((page - 1) * self._post_per_page):(
                                          self._post_per_page * page)]
            elif orderby == 'by_customer':
                get_sale_order_list_total = sorted(
                    orders, key=lambda quotation: quotation.partner_id)
                sale_order_list = get_sale_order_list_total[
                                  ((page - 1) * self._post_per_page):(
                                          self._post_per_page * page)]
            elif orderby == 'by_next_action':
                get_sale_order_list_total = sorted(
                    orders, key=lambda quotation: quotation.title_action)
                sale_order_list = get_sale_order_list_total[
                                  ((page - 1) * self._post_per_page):(
                                          self._post_per_page * page)]
            elif orderby == 'by_expected_closing':
                get_sale_order_list_total = sorted(
                    orders, key=lambda quotation: quotation.date_deadline)
                sale_order_list = get_sale_order_list_total[
                                  ((page - 1) * self._post_per_page):(
                                          self._post_per_page * page)]
            elif orderby == 'by_priority':
                get_sale_order_list_total = sorted(
                    orders, key=lambda quotation: quotation.priority)
                sale_order_list = get_sale_order_list_total[
                                  ((page - 1) * self._post_per_page):(
                                          self._post_per_page * page)]
        page_url = "/shop/opportunity_list"

        if orderby:
            post["orderby"] = orderby
        if search:
            post["search"] = search
        pager = request.website.pager(
            url=page_url,
            total=total_order,
            page=page,
            step=self._post_per_page,
            url_args=post,
        )
        question_obj = pool['price.quotation.questions.faq']
        question_ids = question_obj.search(
            cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        questions = question_obj.browse(
            cr, SUPERUSER_ID, question_ids, context=context)

        values = {
            'questions': questions,
            'orders': sale_order_list,
            'pager': pager,
            'keep': keep,
            'orderby': orderby,
            'search': search,
            # 'tax_total': tax_total,
            'search_text': search_text,
            # 'total': total,
            'agent': agent
        }
        return request.website.render("website.opportunity_list", values)

    @http.route(['/shop/opportunity/<int:opportunity_id>'],
                type='http', auth="user", website=True)
    def view_opportunity(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        crm_lead_pool = pool.get('crm.lead')
        opportunity = crm_lead_pool.browse(
            cr, uid, post.get('opportunity_id'), context=context)

        values = {'opportunity': opportunity}
        return request.website.render('website.opportunity', values)

    @http.route(['/shop/change_opportunity/<int:opportunity_id>',
                 '/shop/change_opportunity'],
                type='http', auth="user", website=True)
    def change_opportunity(self, **post):
        opportunity = request.env['crm.lead'].browse(
            post.get('opportunity_id'))
        crm_case_section_pool = request.env['crm.case.section']
        res_users_pool = request.env['res.users']
        res_user = res_users_pool.browse(request.env.uid)
        crm_case_sections = crm_case_section_pool.search([])
        user_crm_case_section = []
        sale_person = []
        for crm_case_section in crm_case_sections:
            for member in crm_case_section.member_ids:
                sale_person.append(member)
            if res_user in crm_case_section.member_ids:
                user_crm_case_section.append(crm_case_section.id)
        res_partner_pool = request.env['res.partner']
        per_res_partners = res_partner_pool.search(
            [('user_id', '=', request.env.uid)])
        team_res_partners = res_partner_pool.search(
            [('section_id', 'in', user_crm_case_section)])
        res_partners = [p for p in per_res_partners] + \
                       [p for p in team_res_partners]
        salespersons = set(sale_person)
        sales_team = request.env['crm.case.section'].search([])
        tags = request.env['crm.case.categ'].search([])
        values = {'opportunity': opportunity,
                  'customers': res_partners,
                  'salespersons': salespersons,
                  'sales_team': sales_team,
                  'tags': tags}
        return request.website.render('website.change_opportunity', values)

    @http.route(['/shop/save_changed_opportunity/<int:opportunity_id>',
                 '/shop/save_changed_opportunity'],
                type='http', auth="user", website=True)
    def save_opportunity(self, **post):
        opportunity_pool = request.env['crm.lead']
        tags = request.httprequest.form.getlist('tags')
        categ_ids = []
        if tags:
            categ_ids = [int(tag) for tag in tags if tag]

        values = {
            'name': post.get('name'),
            'planned_revenue': float(post.get('planned_revenue')) or None,
            'probability': float(post.get('probability')) or None,
            'partner_id': int(post.get('customer')) or None,
            'email_from': post.get('email_from') or None,
            'phone': post.get('phone') or None,
            'user_id': int(post.get('salesperson')) or None,
            'section_id': int(post.get('sales_team')) or None,
            'title_action': post.get('next_action') or None,
            'date_deadline': post.get('expected_closing') or None,
            'date_action': post.get('date_action') or None,
            'part_pallet': post.get('checkbox_part_pallet') or None,
            'categ_ids': [(6, 0, categ_ids)],
        }
        if post.get('opportunity_id'):
            opportunity_id = post.get('opportunity_id')
            opportunity_pool.sudo().browse(
                post.get('opportunity_id')).write(values)
        else:
            opportunity = opportunity_pool.sudo().create(values)
            opportunity_id = opportunity.id
        return request.redirect('/shop/opportunity/{}'.format(opportunity_id))

    @http.route(['/shop/personal_product_catalog',
                 '/shop/personal_product_catalog/page/<int:page>',
                 '/shop/personal_product_catalog/material_filter/<string:filter_name>',
                 ], type='http', auth="public", website=True)
    def personal_product_catalog(self, page=0, search='',
                                 filter_name='', **post):

        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        domain = [('sale_ok', '=', True)]
        customer_domain = []
        product_material_pool = pool["product.material"]
        product_template_pool = pool['product.template']
        category_pool = pool['product.public.category']
        res_users_pool = pool['res.users']

        product_material_ids = product_material_pool.search(
            cr, SUPERUSER_ID, [('activ', '=', True),
                               ('foam', '=', False)],
            context=context)
        product_materials = product_material_pool.browse(
            cr, SUPERUSER_ID, product_material_ids, context=context)
        filter_material_id = ''
        if not filter_name:
            filter_name = post.get('select_filter_name')
            if not filter_name:
                if product_materials:
                    if len(product_materials) > 1:
                        filter_name = product_materials[0].filter_name
                        filter_material_id = product_materials[0].id
                    else:
                        filter_name = product_materials.filter_name
                        filter_material_id = product_materials.id
            else:
                product_material_ids = product_material_pool.search(
                    cr, SUPERUSER_ID,
                    [('activ', '=', True),
                     ('foam', '=', False),
                     ('filter_name', '=', filter_name),
                     ],
                    context=context)
                product_material = product_material_pool.browse(
                    cr, SUPERUSER_ID, product_material_ids, context=context)
                if len(product_material) > 1:
                    filter_material_id = product_material[0].id
                else:
                    filter_material_id = product_material.id
        else:
            product_material_ids = product_material_pool.search(
                cr, SUPERUSER_ID,
                [('activ', '=', True),
                 ('foam', '=', False),
                 ('filter_name', '=', filter_name),
                 ],
                context=context)
            product_material = product_material_pool.browse(
                cr, SUPERUSER_ID, product_material_ids, context=context)
            if len(product_material) > 1:
                filter_material_id = product_material[0].id
            else:
                filter_material_id = product_material.id

        material = product_material_pool.browse(
            cr, SUPERUSER_ID, filter_material_id, context=context)

        if search:
            search_domain = []
            search_string = search.split(" ")
            if len(search_string) > 1:
                temp_search = []
                temp_search.append(search)
                for srch in temp_search:
                    search_domain += ['|', '|', '|', '|',
                                      ('name', 'ilike', srch),
                                      ('article', 'ilike', srch),
                                      ('competitors_lines.article', 'ilike',
                                       srch),
                                      (
                                          'competitors_lines.name', 'ilike',
                                          srch),
                                      ('public_categ_ids.name', 'ilike', srch)]
            else:
                for srch in search.split(" "):
                    search_domain += ['|', '|', '|', '|',
                                      ('name', 'ilike', srch),
                                      ('article', 'ilike', srch),
                                      ('competitors_lines.article', 'ilike',
                                       srch),
                                      (
                                          'competitors_lines.name', 'ilike',
                                          srch),
                                      ('public_categ_ids.name', 'ilike', srch)]

            domain += search_domain
            customer_domain += search_domain

        user_domain = [('website_published', '=', 'True')]
        res_user = res_users_pool.browse(
            cr, SUPERUSER_ID, request.uid, context=context)
        customer_domain += [
            ('owned_by', '=', 'Customer'),
            ('custom_product_customer', '=', res_user.partner_id.id)]
        domain += user_domain
        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_set = []
        for i in attrib_list:
            attrib_set.append(str(i))

        url = "/shop/personal_product_catalog"
        if 'material_filter' in request.httprequest.path:
            url = '/shop/personal_product_catalog/material_filter/%s' % filter_name
        keep = QueryURL(url, search=search, attrib=attrib_list)

        all_product_template = product_template_pool.search(
            cr, SUPERUSER_ID, customer_domain)
        customer_product_ids = []
        for product in product_template_pool.browse(
                cr, uid, all_product_template, context=context):
            if material:
                for prod_material in product.product_material_ids:
                    if prod_material == material:
                        customer_product_ids.append(product.id)
            else:
                customer_product_ids.append(product.id)

        customer_domain += [('id', 'in', customer_product_ids)]
        customer_product_count = product_template_pool.search_count(
            cr, SUPERUSER_ID, customer_domain, context=context)
        if search:
            post["search"] = search

        pager = request.website.pager(url=url,
                                      total=customer_product_count,
                                      page=page,
                                      step=product_per_page,
                                      scope=7,
                                      url_args=post)
        customer_product_ids = product_template_pool.search(
            cr, SUPERUSER_ID, customer_domain,
            limit=product_per_page,
            offset=pager['offset'],
            order='element_width asc',
            context=context)
        customer_products = product_template_pool.browse(
            cr, SUPERUSER_ID, customer_product_ids, context=context)

        min_max_width = {}
        pub_cat_ids = category_pool.search(
            cr, SUPERUSER_ID, [('width', '=', True)], context=context)
        pub_cat_id = category_pool.browse(
            cr, SUPERUSER_ID, pub_cat_ids, context=context)
        for categs_id in pub_cat_id:
            key = categs_id.id
            if min_max_width.has_key(key):
                min_max_width[key] = categs_id.name
            else:
                min_max_width[key] = categs_id.name

        max_width = max(min_max_width.items(),
                        key=lambda cat: float(cat[1].split()[0]))
        min_width = min(min_max_width.items(),
                        key=lambda cat: float(cat[1].split()[0]))

        style_style_pool = pool.get('product.style')
        style_ids = style_style_pool.search(
            cr, SUPERUSER_ID, [], context=context)
        styles = style_style_pool.browse(
            cr, SUPERUSER_ID, style_ids, context=context)
        filtered_products = []
        for p in customer_products:
            for fp in p.public_categ_ids:
                if str(fp.id) in attrib_set:
                    if p not in filtered_products:
                        filtered_products.append(p)
        if attrib_set:
            customer_products = filtered_products
        category_name_sort = []

        cat_ids = []
        for category in category_name_sort:
            cat_ids.append(category[0])
        groups = category_pool.browse(
            cr, SUPERUSER_ID, cat_ids, context=context)

        order = request.website.sale_get_order()
        buy_products = []
        if order:
            for line in order.order_line:
                if line.product_id.product_tmpl_id.id not in buy_products:
                    buy_products.append(line.product_id.product_tmpl_id.id)
        values = {
            'search': search,
            'pager': pager,
            'products': customer_products,
            'max_width': max_width,
            'min_width': min_width,
            'rows': product_per_row,
            'styles': styles,
            'keep': keep,
            'filter_name': filter_name,
            'product_materials': product_materials,
            'groups': groups,
            'material': material,
            'buy_products': buy_products,
            'agent': False,
            'url': url,
        }
        return request.website.render(
            "website.personal_product_catalog", values)

    @http.route(['/shop/agent_product_catalog'],
                type='http', auth="public", website=True)
    def agent_product_catalog(self, page=0, **post):

        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        domain = [('sale_ok', '=', True)]
        customer_domain = []
        product_material_pool = pool["product.material"]
        product_template_pool = pool['product.template']
        res_partner_pool = pool['res.partner']

        product_material_ids = product_material_pool.search(
            cr, SUPERUSER_ID, [('activ', '=', True),
                               ('foam', '=', False)],
            context=context)
        product_materials = product_material_pool.browse(
            cr, SUPERUSER_ID, product_material_ids, context=context)
        filter_material_id = ''
        filter_name = ''
        if product_materials:
            if len(product_materials) > 1:
                filter_name = product_materials[0].filter_name
                filter_material_id = product_materials[0].id
            else:
                filter_name = product_materials.filter_name
                filter_material_id = product_materials.id

        material = product_material_pool.browse(
            cr, SUPERUSER_ID, filter_material_id, context=context)

        user_domain = [('website_published', '=', 'True')]

        agent, team_users, agent_teams = self.check_agent(
            pool, cr, uid, context=context)
        partner_ids = pool['res.partner'].search(
            cr, uid, [('section_id', 'in', agent_teams)])
        customer_owned_search = [
            ('owned_by', '=', 'Customer'),
            ('custom_product_customer', 'in', partner_ids)]

        customer_domain += customer_owned_search
        domain += user_domain
        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_set = []
        for i in attrib_list:
            attrib_set.append(str(i))

        url = "/shop/personal_product_catalog"
        keep = QueryURL(url, attrib=attrib_list)

        all_product_template = product_template_pool.search(
            cr, SUPERUSER_ID, customer_domain)
        customer_product_ids = []
        for product in product_template_pool.browse(
                cr, uid, all_product_template, context=context):
            if material:
                for prod_material in product.product_material_ids:
                    if prod_material == material:
                        customer_product_ids.append(product.id)
            else:
                customer_product_ids.append(product.id)

        customer_domain += [('id', 'in', customer_product_ids)]
        customer_product_count = product_template_pool.search_count(
            cr, SUPERUSER_ID, customer_domain, context=context)

        pager = request.website.pager(url=url,
                                      total=customer_product_count,
                                      page=page,
                                      step=product_per_page,
                                      scope=7,
                                      url_args=post)
        customer_product_ids = product_template_pool.search(
            cr, SUPERUSER_ID, customer_domain,
            limit=product_per_page,
            offset=pager['offset'],
            order='element_width asc',
            context=context)
        customer_products = product_template_pool.browse(
            cr, SUPERUSER_ID, customer_product_ids, context=context)

        filtered_products = []
        for p in customer_products:
            for fp in p.public_categ_ids:
                if str(fp.id) in attrib_set:
                    if p not in filtered_products:
                        filtered_products.append(p)
        if attrib_set:
            customer_products = filtered_products

        partners = res_partner_pool.browse(
            cr, SUPERUSER_ID, partner_ids, context=context)
        groups = []
        for partner in partners:
            if product_template_pool.search(
                    cr, SUPERUSER_ID,
                    [('custom_product_customer', '=', partner.id)]):
                groups.append(partner)

        order = request.website.sale_get_order()
        buy_products = []
        if order:
            for line in order.order_line:
                if line.product_id.product_tmpl_id.id not in buy_products:
                    buy_products.append(line.product_id.product_tmpl_id.id)
        values = {
            'pager': pager,
            'products': customer_products,
            'rows': product_per_row,
            'keep': keep,
            'filter_name': filter_name,
            'product_materials': product_materials,
            'groups': groups,
            'material': material,
            'buy_products': buy_products,
            'agent': agent,
            'url': url,
        }
        sale_order_line_pool = pool.get('sale.order.line')
        stock_move_pool = pool.get('stock.move')
        stock_picking_pool = pool.get('stock.picking')
        product_statistic = []
        for product in customer_products:
            product_template_dict = {'template_id': product}

            products = [p for p in product.product_variant_ids
                        if p.material_id.id == material.id]
            product_products = []
            for p in products:
                product_product_dict = {'product_id': p}

                sale_order_line_ids = sale_order_line_pool.search(
                    cr, uid, [
                        ('product_id', '=', p.id)], order='order_id')
                sale_order_lines = sale_order_line_pool.browse(
                    cr, uid, sale_order_line_ids, context=context)
                order_line_dicts = []
                for order_line in sale_order_lines:
                    order_line_dict = {'order_line': order_line,
                                       'line_amount': order_line.product_uom_qty}

                    procurement_group_id = \
                        order_line.order_id.procurement_group_id.id
                    if procurement_group_id:
                        picking_ids = stock_picking_pool.search(
                            cr, uid,
                            [('group_id', '=', procurement_group_id)])
                        stock_move_ids = stock_move_pool.search(
                            cr, uid, [('product_id', '=', p.id),
                                      ('picking_id', 'in', picking_ids)])
                        stock_move = stock_move_pool.browse(
                            cr, uid, stock_move_ids, context=context)
                        delivery_amount = 0
                        invoice_amount = 0
                        for move in stock_move:
                            delivery_amount += move.product_uom_qty

                            for invoice in order_line.order_id.invoice_ids:
                                if move.picking_id.name == invoice.origin:
                                    invoice_amount += invoice.amount_total
                        order_line_dict.update(
                            stock_move=stock_move,
                            delivery_amount=delivery_amount,
                            invoice_amount=invoice_amount)

                        order_line_dicts.append(order_line_dict)
                product_product_dict['order_lines'] = order_line_dicts

                product_products.append(product_product_dict)
            product_template_dict['product_product'] = product_products

            product_statistic.append(product_template_dict)

        product_dicts = []
        new_groups = []
        for product in customer_products:
            products = [p for p in product.product_variant_ids
                        if p.material_id.id == material.id]

            for p in products:
                if p.material_id == material:
                    sale_order_line_ids = sale_order_line_pool.search(
                        cr, uid, [
                            ('product_id', '=', p.id)], order='order_id')
                    sale_order_lines = sale_order_line_pool.browse(
                        cr, uid, sale_order_line_ids, context=context)
                    procurement_group_ids = [
                        line.order_id.procurement_group_id.id
                        for line in sale_order_lines
                        if line.order_id.state not in ['draft', 'cancel']]
                    if procurement_group_ids:
                        picking_ids = stock_picking_pool.search(
                            cr, uid, [
                                ('group_id', 'in', procurement_group_ids)])

                        stock_move_ids = stock_move_pool.search(
                            cr, uid, [
                                ('product_id', '=', p.id),
                                ('picking_id', 'in', picking_ids)])
                        stock_move = stock_move_pool.browse(
                            cr, uid, stock_move_ids, context=context)
                        product_dict = {'product_template': product,
                                        'product_product': p,
                                        'sale_order_lines':
                                            sale_order_lines,
                                        'stock_move': stock_move}
                        product_dicts.append(product_dict)
                        if p.custom_product_customer not in new_groups \
                                and product_dict:
                            new_groups.append(p.custom_product_customer)
        values['product_dicts'] = product_statistic
        values['groups'] = new_groups
        return request.website.render(
            "website.agent_product_catalog", values)

    @http.route(['/shop/get_personal_product'],
                type='json', auth="public", website=True)
    def get_personal_product(self, **post):

        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.env
        product_material_pool = pool["product.material"]
        product_product_pool = pool['product.product']
        res_partner_pool = pool['res.partner']
        sale_order_pool = pool['sale.order']
        sale_order_line_pool = pool['sale.order.line']
        stock_picking_pool = pool['stock.picking']
        stock_move_pool = pool['stock.move']
        product_category_pool = pool['product.category']

        filter_name = get_filter_name(post.get('filter_name'))

        product_material = product_material_pool.search(
            [('activ', '=', True),
             ('foam', '=', False),
             ('filter_name', '=', filter_name),
             ])
        if len(product_material) > 1:
            filter_material = product_material[0]
        else:
            filter_material = product_material
        search = post.get('search')
        agent, team_users, agent_teams = self.check_agent(
            request.registry, cr, uid, context=context)

        result = ''
        if search:
            search_string = search.split(" ")
            partners = []
            products = []
            for s in search_string:
                partners += res_partner_pool.search(
                    [('name', 'ilike', s),
                     ('section_id', 'in', agent_teams)])
                products += product_product_pool.search(
                    ['|', ('name', 'ilike', s),
                     ('article', 'ilike', s)])
            product_category = product_category_pool.search(
                [('name', 'in',
                  ('Wooden Box', 'Steel Box', 'Service Units'))])
            if partners and not products:
                for partner in partners:
                    partner_ids = [partner.id] + [partner.parent_id.id] + \
                                  [c.id for c in partner.child_ids] + \
                                  [c.id for c in partner.parent_id.child_ids]

                    partner_company = []
                    for partner_id in partner_ids:
                        if partner_id and partner_id not in partner_company:
                            partner_company.append(partner_id)

                    all_sale_products = []
                    sale_orders, sale_order_ids = get_sale_order(
                        sale_order_pool, partner_company)

                    pickings = []
                    for sale_order in sale_orders:
                        procurement_group_ids = []
                        try:
                            procurement_group_ids.append(
                                sale_order.procurement_group_id.id)
                        except:
                            pass
                        if procurement_group_ids:
                            pickings += stock_picking_pool.search(
                                [('group_id', 'in', procurement_group_ids),
                                 ('name', 'ilike', 'OUT')])

                        for line in sale_order.order_line:

                            if line.product_id not in all_sale_products:
                                if not line.product_id.extruder:
                                    if line.product_id.categ_id \
                                            not in product_category:
                                        all_sale_products.append(
                                            line.product_id)
                    sale_products = []

                    for product in all_sale_products:
                        if product.material_id == filter_material:
                            if post.get('product_type') == 'personal':
                                if product.owned_by == 'Customer':
                                    sale_products.append(product)
                            elif post.get('product_type') == 'company':
                                if product.owned_by == 'thermevo':
                                    sale_products.append(product)
                            elif post.get('product_type') == 'all':
                                sale_products.append(product)

                    if sale_products:
                        product_block = get_product_block()
                        product_body = ''
                        for product in sale_products:
                            line_count = 0
                            transfer_count = 0
                            invoice_count = 0
                            sale_order_block = ''
                            sale_order_lines = sale_order_line_pool.search(
                                [('product_id', '=', product.id),
                                 ('order_id', 'in', sale_order_ids)])
                            for line in sale_order_lines:
                                picking_ids = [p.id for p in pickings]

                                stock_move = stock_move_pool.search([
                                    ('product_id', '=', product.id),
                                    ('picking_id', 'in', picking_ids)])

                                picking_block = ''
                                (invoice_count,
                                 picking_block,
                                 transfer_count) = \
                                    get_invoice_and_transfer_block(
                                        stock_move, line, invoice_count,
                                        picking_block, transfer_count)

                                sale_order_block += get_sale_order_block(
                                    line, picking_block)
                                line_count += line.product_uom_qty

                            product_name = product.name.encode('utf8')
                            qty_in_pallet = str(product.qty_in_pallet).encode(
                                'utf8')

                            summary_count_block = '<tr style="background:#d6edfd" class="prod_line_name">' \
                                                  '<td/><td/><td/><td/><td/>' \
                                                  '<td>{line_count}</td>' \
                                                  '<td/>' \
                                                  '<td>{transfer_count}</td>' \
                                                  '<td/><td/>' \
                                                  '<td>{invoice_count} €</td>' \
                                                  '<td/><td/>' \
                                                  '</tr>'.format(
                                line_count=line_count,
                                transfer_count=transfer_count,
                                invoice_count=invoice_count)

                            product_body += '<tbody>' \
                                            '<tr class="prod_line_name">' \
                                            '<td class="td18 prod_name">' \
                                            '{product_name}' \
                                            '</td>' \
                                            '<td class="td18">{qty_in_pallet}</td>' \
                                            '<td/><td/><td/><td/><td/>' \
                                            '<td/><td/><td/><td/><td/>' \
                                            '</tr>' \
                                            '{sale_order_block}' \
                                            '{summary_count_block}' \
                                            '</<tbody>'.format(
                                product_name=product_name,
                                qty_in_pallet=qty_in_pallet,
                                sale_order_block=sale_order_block,
                                summary_count_block=summary_count_block)

                        product_block = product_block.format(body=product_body)

                        group_block = '<div class="group_product">' \
                                      '<div class="group_product_title">' \
                                      '{group_name}</div>' \
                                      '<div class="group_subgroup">' \
                                      '{product_block}' \
                                      '</div>' \
                                      '</div>'.format(group_name=partner.name,
                                                      product_block=product_block)
                        result += group_block

            elif partners and products:
                for partner in partners:
                    partner_company = [partner.id] + [partner.parent_id.id] + \
                                      [c.id for c in partner.child_ids] + \
                                      [c.id for c in
                                       partner.parent_id.child_ids]
                    all_sale_products = []
                    group_block = ''
                    sale_products = []
                    for product in products:
                        if product.material_id == filter_material:
                            if post.get('product_type') == 'personal':
                                if product.owned_by == 'Customer':
                                    all_sale_products.append(product)
                            elif post.get('product_type') == 'company':
                                if product.owned_by == 'thermevo':
                                    all_sale_products.append(product)
                            elif post.get('product_type') == 'all':
                                all_sale_products.append(product)
                    for product in all_sale_products:
                        if product.categ_id not in product_category:
                            sale_products.append(product)
                    all_sale_orders = sale_order_pool.search(
                        [('partner_id', 'in', partner_company),
                         ('state', 'not in', ('cancel', 'draft'))])
                    sale_orders = []
                    for order in all_sale_orders:
                        for line in order.order_line:
                            if line.product_id in products:
                                sale_orders.append(order)

                    order_ids = [s.id for s in sale_orders]

                    sale_order_ids = list(reversed(order_ids))
                    pickings = []
                    for sale_order in sale_orders:
                        procurement_group_ids = []
                        try:
                            procurement_group_ids.append(
                                sale_order.procurement_group_id.id)
                        except:
                            pass
                        if procurement_group_ids:
                            pickings += stock_picking_pool.search(
                                [('group_id', 'in', procurement_group_ids),
                                 ('name', 'ilike', 'OUT')])

                    if sale_products:
                        product_block = get_product_block()
                        product_body = ''
                        for product in sale_products:
                            line_count = 0
                            transfer_count = 0
                            invoice_count = 0
                            sale_order_block = ''
                            sale_order_lines = sale_order_line_pool.search(
                                [('product_id', '=', product.id),
                                 ('order_id', 'in', sale_order_ids)])
                            for line in sale_order_lines:
                                picking_ids = [p.id for p in pickings]

                                stock_move = stock_move_pool.search([
                                    ('product_id', '=', product.id),
                                    ('picking_id', 'in', picking_ids)])
                                picking_block = ''

                                (invoice_count,
                                 picking_block,
                                 transfer_count) = \
                                    get_invoice_and_transfer_block(
                                        stock_move, line, invoice_count,
                                        picking_block, transfer_count)

                                sale_order_block += get_sale_order_block(
                                    line, picking_block)
                                line_count += line.product_uom_qty

                            product_name = product.name.encode('utf8')
                            qty_in_pallet = str(product.qty_in_pallet).encode(
                                'utf8')

                            summary_count_block = '<tr style="background:#d6edfd" class="prod_line_name">' \
                                                  '<td/><td/><td/><td/><td/>' \
                                                  '<td>{line_count}</td>' \
                                                  '<td/>' \
                                                  '<td>{transfer_count}</td>' \
                                                  '<td/><td/>' \
                                                  '<td>{invoice_count} €</td>' \
                                                  '<td/><td/>' \
                                                  '</tr>'.format(
                                line_count=line_count,
                                transfer_count=transfer_count,
                                invoice_count=invoice_count)

                            product_body += '<tbody>' \
                                            '<tr class="prod_line_name">' \
                                            '<td class="td18 prod_name">' \
                                            '{product_name}' \
                                            '</td>' \
                                            '<td class="td18">{qty_in_pallet}</td>' \
                                            '<td/><td/><td/><td/><td/>' \
                                            '<td/><td/><td/><td/><td/>' \
                                            '</tr>' \
                                            '{sale_order_block}' \
                                            '{summary_count_block}' \
                                            '</<tbody>'.format(
                                product_name=product_name,
                                qty_in_pallet=qty_in_pallet,
                                sale_order_block=sale_order_block,
                                summary_count_block=summary_count_block)
                        product_block = product_block.format(body=product_body)

                        group_block = '<div class="group_product">' \
                                      '<div class="group_product_title">' \
                                      '{group_name}</div>' \
                                      '<div class="group_subgroup">' \
                                      '{product_block}' \
                                      '</div>' \
                                      '</div>'.format(group_name=partner.name,
                                                      product_block=product_block)
                        result += group_block

            elif not partners and products:
                partners = []
                all_sale_products = []
                sale_products = []
                for product in products:
                    if product.material_id == filter_material:
                        if post.get('product_type') == 'personal':
                            if product.owned_by == 'Customer':
                                all_sale_products.append(product)
                        elif post.get('product_type') == 'company':
                            if product.owned_by == 'thermevo':
                                all_sale_products.append(product)
                        elif post.get('product_type') == 'all':
                            all_sale_products.append(product)

                for product in all_sale_products:
                    if product.categ_id not in product_category:
                        sale_products.append(product)

                product_ids = [p.id for p in sale_products]
                sale_order_lines = sale_order_line_pool.search(
                    [('product_id', 'in', product_ids)])
                sale_orders = []
                for line in sale_order_lines:
                    if line.order_id.state not in ('cancel', 'draft'):
                        if line.order_id not in sale_orders:
                            sale_orders.append(line.order_id)
                        if line.order_id.partner_id not in partners:
                            partners.append(line.order_id.partner_id)
                order_ids = [s.id for s in sale_orders]

                sale_order_ids = list(reversed(order_ids))
                pickings = []
                for sale_order in sale_orders:
                    procurement_group_ids = []
                    try:
                        procurement_group_ids.append(
                            sale_order.procurement_group_id.id)
                    except:
                        pass
                    if procurement_group_ids:
                        pickings += stock_picking_pool.search(
                            [('group_id', 'in', procurement_group_ids),
                             ('name', 'ilike', 'OUT')])
                if sale_products:
                    for partner in partners:
                        product_block = get_product_block()

                        product_body = ''
                        for product in sale_products:
                            line_count = 0
                            transfer_count = 0
                            invoice_count = 0
                            sale_order_block = ''
                            sale_order_lines = sale_order_line_pool.search(
                                [('product_id', '=', product.id),
                                 ('order_id', 'in', sale_order_ids)])
                            for line in sale_order_lines:
                                picking_ids = [p.id for p in pickings]

                                stock_move = stock_move_pool.search([
                                    ('product_id', '=', product.id),
                                    ('picking_id', 'in', picking_ids)])
                                picking_block = ''

                                (invoice_count,
                                 picking_block,
                                 transfer_count) = \
                                    get_invoice_and_transfer_block(
                                        stock_move, line, invoice_count,
                                        picking_block, transfer_count)

                                sale_order_block += get_sale_order_block(
                                    line, picking_block)
                                line_count += line.product_uom_qty

                            product_name = product.name.encode('utf8')
                            qty_in_pallet = str(product.qty_in_pallet).encode(
                                'utf8')

                            summary_count_block = '<tr style="background:#d6edfd" class="prod_line_name">' \
                                                  '<td/><td/><td/><td/><td/>' \
                                                  '<td>{line_count}</td>' \
                                                  '<td/>' \
                                                  '<td>{transfer_count}</td>' \
                                                  '<td/><td/>' \
                                                  '<td>{invoice_count} €</td>' \
                                                  '<td/><td/>' \
                                                  '</tr>'.format(
                                line_count=line_count,
                                transfer_count=transfer_count,
                                invoice_count=invoice_count)

                            product_body += '<tbody>' \
                                            '<tr class="prod_line_name">' \
                                            '<td class="td18 prod_name">' \
                                            '{product_name}' \
                                            '</td>' \
                                            '<td class="td18">{qty_in_pallet}</td>' \
                                            '<td/><td/><td/><td/><td/>' \
                                            '<td/><td/><td/><td/><td/>' \
                                            '</tr>' \
                                            '{sale_order_block}' \
                                            '{summary_count_block}' \
                                            '</<tbody>'.format(
                                product_name=product_name,
                                qty_in_pallet=qty_in_pallet,
                                sale_order_block=sale_order_block,
                                summary_count_block=summary_count_block)
                        product_block = product_block.format(body=product_body)

                        group_block = '<div class="group_product">' \
                                      '<div class="group_product_title">' \
                                      '{group_name}</div>' \
                                      '<div class="group_subgroup">' \
                                      '{product_block}' \
                                      '</div>' \
                                      '</div>'.format(group_name=partner.name,
                                                      product_block=product_block)
                        result += group_block

            else:
                result = '<div class="group_product">' \
                         'Sorry, nothing was found' \
                         '</div>'

        if not result:
            result = '<div class="group_product">' \
                     'Sorry, nothing was found' \
                     '</div>'

        return {'result': result}

    # def product_search(self, search, filter_name, product_type):

    @http.route(['/shop/product/<model("product.template"):product>'],
                type='http', auth="public", website=True)
    def product(self, product):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        agent, team_users, agent_teams = self.check_agent(
            pool, cr, uid, context=context)
        values = {'product': product,
                  'agent': agent}

        return request.website.render("thermevo_shop.product_page", values)


def get_product_block():
    block = '<div class="agent_product_item">' \
            '<table class="my-articles-table product_information">' \
            '<thead>' \
            '<tr class="prod_line_header">' \
            '<th>Name</th>' \
            '<th>Qty In The Pallet</th>' \
            '<th>SO</th>' \
            '<th>SO Date</th>' \
            '<th>SO Valid. Date</th>' \
            '<th>Qty</th>' \
            '<th>DO</th>' \
            '<th>D.Qty</th>' \
            '<th>D.S.</th>' \
            '<th>I</th>' \
            '<th>Amount</th>' \
            '<th>I.S.</th>' \
            '</tr>' \
            '</thead>{body}' \
            '</table>' \
            '</div>'
    return block


def get_sale_order(sale_order_pool, partner_company):
    sale_orders = sale_order_pool.search(
        [('partner_id', 'in', partner_company),
         ('state', 'not in', ('cancel', 'draft'))])
    order_ids = [s.id for s in sale_orders]

    sale_order_ids = list(reversed(order_ids))
    return sale_orders, sale_order_ids


def get_invoice_and_transfer_block(
        stock_move, line, invoice_count, picking_block, transfer_count):
    for move in stock_move:
        if move.picking_id.group_id == line.order_id.procurement_group_id:
            invoice_block = ''
            for invoice in line.order_id.invoice_ids:
                if move.picking_id.name == invoice.origin:
                    for invoice_line in invoice.invoice_line:
                        if invoice_line.product_id == move.product_id:
                            invoice_number = invoice.number
                            if not invoice_number:
                                invoice_number = 'invoice'
                            state = invoice.state

                            state_val = u''
                            if state:
                                state_val = unicode(dict(
                                    invoice._columns['state'].selection).get(
                                    state), 'utf-8')
                            date_invoice = invoice.date_invoice
                            if date_invoice:
                                date_invoice = date_invoice.split(' ')[0]

                            invoice_block = '<tr class="prod_line_stat">' \
                                            '<td/><td/><td/><td/><td/><td/><td/><td/><td/>' \
                                            '<td>' \
                                            '<a href="/shop/invoice/{invoice_id}">' \
                                            '{invoice_number}</a>' \
                                            '</td>' \
                                            '<td>' \
                                            '{amount_total} €' \
                                            '</td>' \
                                            '<td class="invoice_state">' \
                                            '{state}</br>' \
                                            '<span>{date_invoice}</span>' \
                                            '</td>' \
                                            '</tr>'.format(
                                invoice_id=invoice.id,
                                invoice_number=invoice_number,
                                amount_total=str(
                                    invoice_line.price_subtotal),
                                state=state_val,
                                date_invoice=date_invoice)
                            invoice_count += invoice_line.price_subtotal

            date_done = move.picking_id.date_done
            if date_done:
                date_done = date_done.split(' ')[0]
            else:
                date_done = '-'

            state = move.picking_id.state

            state_val = u''
            if state:
                state_val = unicode(
                    dict(move.picking_id._columns['state'].selection).get(
                        state), 'utf-8')

            picking_block += '<tr class="prod_line_stat">' \
                             '<td/> <td/><td/><td/><td/><td/>' \
                             '<td class="td18">' \
                             '<a href="/shop/delivery_order/{picking_id}">' \
                             '{picking_name}</a>' \
                             '</td>' \
                             '<td class="td18">' \
                             '{product_uom_qty}' \
                             '</td>' \
                             '<td class="td18 pick_state">' \
                             '{state}</br>' \
                             '<span>' \
                             '{date_done}' \
                             '</span>' \
                             '</td>' \
                             '<td/><td/><td/>' \
                             '</tr>{invoice_block}'.format(
                picking_id=str(move.picking_id.id),
                picking_name=move.picking_id.name,
                product_uom_qty=str(
                    move.product_uom_qty),
                state=state_val,
                date_done=date_done,
                invoice_block=invoice_block)
            transfer_count += move.product_uom_qty
    return invoice_count, picking_block, transfer_count


def get_sale_order_block(line, picking_block):
    date_order = line.order_id.date_order
    if date_order:
        date_order = date_order.split(' ')[0]
    else:
        date_order = '-'

    validity_date = line.order_id.validity_date
    if validity_date:
        validity_date = validity_date.split(' ')[0]
    else:
        validity_date = '-'
    sale_order_block = '<tr class="prod_line_stat">' \
                       '<td/>' \
                       '<td/>' \
                       '<td class="td18">' \
                       '<a href="/quote/{order_id}">' \
                       '{order_name}</a>' \
                       '</td>' \
                       '<td class="td18">' \
                       '<span>{date_order}</span>' \
                       '</td>' \
                       '<td class="td18">' \
                       '<span>{validity_date}</span>' \
                       '</td>' \
                       '<td class="td18">' \
                       '{product_uom_qty}' \
                       '</td>' \
                       '<td/><td/><td/><td/><td/><td/>' \
                       '</tr>{picking_block}'. \
        format(order_id=line.order_id.id,
               order_name=line.order_id.name,
               date_order=date_order,
               validity_date=validity_date,
               product_uom_qty=line.product_uom_qty,
               picking_block=picking_block)

    return sale_order_block
