# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.addons.website_sale.controllers.main import \
    website_sale as odoo_website_sale
from openerp.addons.website_quote.controllers.main import \
    sale_quote as odoo_website_quote
import werkzeug
from openerp import SUPERUSER_ID
from openerp.http import Controller, route, request
from openerp import http
import math
import logging
from datetime import datetime
import time
from openerp.tools.translate import _
from openerp.addons.thermevo_shop.google_script \
    import google_sheet_dublicate

product_per_page = 50  # Products Per Page
product_per_row = 4  # Products Per Row
dup_unit_price = {}

_logger = logging.getLogger(__name__)


def create_name(lead, product_value_list):
    name = 'Insulating Strip THERMEVO EvoBreak ref#: %s; ' \
           'Width: %s; Height: %s; Length: %s; No ' % (
               lead.dxf_filename,
               round(float(
                   product_value_list.get(
                       'product_information').get('width')), 2),
               round(float(product_value_list.get(
                   'product_information').get('height')), 2),
               round(float(product_value_list.get(
                   'product_information').get('length')), 2),
           )
    return name


class WebsiteSale(odoo_website_sale):
    _post_per_page = 10

    @http.route(['/shop',
                 '/shop/page/<int:page>',
                 '/shop/material_filter/<string:filter_name>',
                 '/shop/material_filter/<string:filter_name>/page/<int:page>',
                 ], type='http', auth="public", website=True)
    def shop(self, page=1, category=None, search='',
             product_type='', sort_width='', filter_name='', **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        domain = [('sale_ok', '=', True)]
        customer_domain = []
        product_material_pool = pool["product.material"]
        product_template_pool = pool['product.template']
        question_pool = pool['catalog.faq']
        category_pool = pool['product.public.category']
        res_users_pool = pool['res.users']
        if search:
            search_domain = []
            search_string = search.split(" ")
            if len(search_string) > 1:
                temp_search = []
                temp_search.append(search)
                for srch in temp_search:
                    search_domain += ['|', '|', '|', '|',
                                      ('name', 'ilike', srch),
                                      ('article', 'ilike', srch),
                                      ('competitors_lines.article', 'ilike',
                                       srch),
                                      ('competitors_lines.name', 'ilike', srch),
                                      ('public_categ_ids.name', 'ilike', srch)]
            else:
                for srch in search.split(" "):
                    search_domain += ['|', '|', '|', '|',
                                      ('name', 'ilike', srch),
                                      ('article', 'ilike', srch),
                                      ('competitors_lines.article', 'ilike',
                                       srch),
                                      ('competitors_lines.name', 'ilike', srch),
                                      ('public_categ_ids.name', 'ilike', srch)]

            domain += search_domain
            customer_domain += search_domain
        if not product_type:
            product_type = ''

        sort_width = post.get('sort')
        if not sort_width:
            sort_width = 'narrow_wide'

        product_material_ids = product_material_pool.search(
            cr, SUPERUSER_ID, [('activ', '=', True),
                               ('foam', '=', False)],
            context=context)
        product_materials = product_material_pool.browse(
            cr, SUPERUSER_ID, product_material_ids, context=context)
        filter_material_id = ''
        if not filter_name:
            filter_name = post.get('select_filter_name')
            if not filter_name:
                if product_materials:
                    if len(product_materials) > 1:
                        filter_name = product_materials[0].filter_name
                        filter_material_id = product_materials[0].id
                    else:
                        filter_name = product_materials.filter_name
                        filter_material_id = product_materials.id
            else:
                product_material_ids = product_material_pool.search(
                    cr, SUPERUSER_ID,
                    [('activ', '=', True),
                     ('foam', '=', False),
                     ('filter_name', '=', filter_name),
                     ],
                    context=context)
                product_material = product_material_pool.browse(
                    cr, SUPERUSER_ID, product_material_ids, context=context)
                if len(product_material) > 1:
                    filter_material_id = product_material[0].id
                else:
                    filter_material_id = product_material.id
        else:
            product_material_ids = product_material_pool.search(
                cr, SUPERUSER_ID,
                [('activ', '=', True),
                 ('foam', '=', False),
                 ('filter_name', '=', filter_name),
                 ],
                context=context)
            product_material = product_material_pool.browse(
                cr, SUPERUSER_ID, product_material_ids, context=context)
            if len(product_material) > 1:
                filter_material_id = product_material[0].id
            else:
                filter_material_id = product_material.id

        material = product_material_pool.browse(
            cr, SUPERUSER_ID, filter_material_id, context=context)

        user_domain = [('website_published', '=', 'True')]
        res_user = res_users_pool.browse(
            cr, SUPERUSER_ID, request.uid, context=context)

        owned_search = [('owned_by', '=', 'thermevo')]

        agent, team_users, agent_teams = self.check_agent(
            pool, cr, uid, context=context)
        if agent:
            partner_ids = pool['res.partner'].search(
                cr, uid, [('section_id', 'in', agent_teams)])
            customer_owned_search = [
                ('owned_by', '=', 'Customer'),
                ('custom_product_customer', 'in', partner_ids)]
        else:
            customer_owned_search = [
                ('owned_by', '=', 'Customer'),
                ('custom_product_customer', '=', res_user.partner_id.id)]

        customer_domain += customer_owned_search
        domain += user_domain
        domain += owned_search

        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_set = []
        for i in attrib_list:
            attrib_set.append(str(i))

        url = "/shop"
        if 'material_filter' in request.httprequest.path:
            url = '/shop/material_filter/%s' % filter_name
        keep = QueryURL(url, search=search, attrib=attrib_list)

        all_product_template = product_template_pool.search(
            cr, SUPERUSER_ID, domain) + product_template_pool.search(
            cr, SUPERUSER_ID, customer_domain)

        need_product_ids = []
        for product in product_template_pool.browse(
                cr, uid, all_product_template, context=context):
            for prod_material in product.product_material_ids:
                if prod_material == material:
                    need_product_ids.append(product.id)

        product_count = len(need_product_ids)
        if search:
            post["search"] = search
        if sort_width:
            post["sort_width"] = sort_width

        pager = request.website.pager(url=url,
                                      total=product_count,
                                      page=page,
                                      step=product_per_page,
                                      scope=7,
                                      url_args=post)

        products = product_template_pool.browse(
            cr, SUPERUSER_ID, need_product_ids, context=context)

        i = (sorted(products, key=lambda prod: prod.element_width))
        all_products = i[((page - 1) * product_per_page):(
            product_per_page * page)]

        min_max_width = {}
        pub_cat_ids = category_pool.search(
            cr, SUPERUSER_ID, [('width', '=', True)], context=context)
        pub_cat_id = category_pool.browse(
            cr, SUPERUSER_ID, pub_cat_ids, context=context)
        for categs_id in pub_cat_id:
            key = categs_id.id
            if min_max_width.has_key(key):
                min_max_width[key] = categs_id.name
            else:
                min_max_width[key] = categs_id.name

        max_width = max(min_max_width.items(),
                        key=lambda cat: float(cat[1].split()[0]))
        min_width = min(min_max_width.items(),
                        key=lambda cat: float(cat[1].split()[0]))

        style_style_pool = pool.get('product.style')
        style_ids = style_style_pool.search(
            cr, SUPERUSER_ID, [], context=context)
        styles = style_style_pool.browse(
            cr, SUPERUSER_ID, style_ids, context=context)
        filtered_products = []
        for p in all_products:
            for fp in p.public_categ_ids:
                if str(fp.id) in attrib_set:
                    if p not in filtered_products:
                        filtered_products.append(p)
        if attrib_set:
            all_products = filtered_products
        # ----Find maximum and minimum width from product_public_category------
        category_name_sort = []
        cat_name = {}

        if sort_width == 'narrow_wide':
            for prod in all_products:
                for prod_public_cat in prod.public_categ_ids:
                    if prod_public_cat.width:
                        key = prod_public_cat.id
                        if cat_name.has_key(key):
                            cat_name[key] = prod_public_cat.name
                        else:
                            cat_name[key] = prod_public_cat.name
            category_name_sort = sorted(cat_name.items(),
                                        key=lambda cat: float(
                                            cat[1].split()[0]))

        if sort_width == 'wide_narrow':
            for prod in all_products:
                for prod_public_cat in prod.public_categ_ids:
                    if prod_public_cat.width:
                        key = prod_public_cat.id
                        if cat_name.has_key(key):
                            cat_name[key] = prod_public_cat.name
                        else:
                            cat_name[key] = prod_public_cat.name
            category_name_sort = sorted(
                cat_name.items(),
                key=lambda cat: float(cat[1].split()[0]), reverse=True)

        question_ids = question_pool.search(cr, SUPERUSER_ID, [], offset=0,
                                            limit=1, context=context)
        questions = question_pool.browse(cr, SUPERUSER_ID, question_ids,
                                         context=context)

        cat_ids = []
        for category in category_name_sort:
            cat_ids.append(category[0])
        groups = category_pool.browse(
            cr, SUPERUSER_ID, cat_ids, context=context)

        order = request.website.sale_get_order()
        buy_products = []
        if order:
            for line in order.order_line:
                if line.product_id.product_tmpl_id.id not in buy_products:
                    buy_products.append(line.product_id.product_tmpl_id.id)

        values = {
            'search': search,
            'sort_width': sort_width,
            'category': category,
            'pager': pager,
            'products': all_products,
            'max_width': max_width,
            'min_width': min_width,
            'rows': product_per_row,
            'styles': styles,
            'keep': keep,
            'filter_name': filter_name,
            'faq_questions': questions,
            'product_type': product_type,
            'product_materials': product_materials,
            'groups': groups,
            'material': material,
            'buy_products': buy_products,
            'url': url
        }
        return request.website.render("website.shop_product_catalog", values)

    @http.route(['/shop/advance_search_filter'], type='json',
                auth="public", methods=['POST'], website=True)
    def advance_search(self, minimu_width, maximum_width, category_id,
                       product_data, foam_check, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        domain = request.website.sale_product_domain()

        product_public_category_pool = pool['product.public.category']
        product_template_pool = pool.get('product.template')
        product_weight_category_pool = pool.get('product.weight.category')

        if category_id == "not_category":
            category = None
        else:
            category = category_id

        if category:
            domain += [('public_categ_ids', 'child_of', int(category))]

        if not category:
            default_cat_id = []
            default_category = pool.get('product.public.category')
            if default_category:
                default_cat_id = product_public_category_pool.search(
                    cr, SUPERUSER_ID, [('default', '=', 'True')],
                    context=context)
            domain += [('public_categ_ids', 'child_of', default_cat_id)]

        if request.website.user_id.id == request.uid:
            domain += [('res_id', '=', '')]
        else:
            domain += [('res_id', 'in', (int(request.uid), ''))]

        cat_prod_dic = {}

        product_ids = product_template_pool.search(
            cr, SUPERUSER_ID, domain, context=context)
        for prod in product_ids:
            key = prod
            product_obj = product_template_pool.browse(
                cr, uid, int(prod), context=context)
            if cat_prod_dic.has_key(key):
                cat_prod_dic[key] = product_obj
            else:
                cat_prod_dic[key] = product_obj

        foam_sorting = {}
        if foam_check:
            for prod_temp_id, prod_temp_obj in cat_prod_dic.items():
                for attr_rang in prod_temp_obj['product_weight_category_pool']:
                    product_weight_id = product_weight_category_pool.browse(
                        cr, SUPERUSER_ID, int(attr_rang), context=context)
                    has_foam = product_weight_id.attribute_name.foam
                    if has_foam:
                        key = prod_temp_obj['id']
                        if foam_sorting.has_key(key):
                            foam_sorting[key] = prod_temp_obj
                        else:
                            foam_sorting[key] = prod_temp_obj

        # ------------------------For Width filtering only---------------------
        if minimu_width and maximum_width and not product_data:
            prod_name = {}
            prod_website = {}
            prod_id = {}
            product_details = {}
            both_sorting = []

            if foam_sorting:
                for p_id, p_obj in foam_sorting.items():
                    for cate in p_obj['public_categ_ids']:
                        if cate.width:
                            c_name = cate.name
                            c_name_number = c_name.split()[0]
                            if (float(c_name_number) >= float(minimu_width)
                                and (float(c_name_number) <=
                                         float(maximum_width))):
                                product_data = product_template_pool.browse(
                                    cr, SUPERUSER_ID, p_id, context=context)
                                p_key = product_data.id

                                prod_arr = []
                                prod_arr.append(product_data.id)
                                prod_name['name'] = product_data.name
                                prod_id['id'] = product_data.id
                                prod_website['website_published'] = (
                                    product_data.website_published)

                                product_details[p_key] = dict(
                                    prod_name.items() +
                                    prod_id.items() +
                                    prod_website.items())

                                if product_data not in both_sorting:
                                    both_sorting.append(product_data)
                get_all_dictionary = self.make_dictionary(both_sorting)
                prod_info = get_all_dictionary['prod_info']
                prod_cat = get_all_dictionary['prod_cat']
                cat_info = get_all_dictionary['cat_info']
                length = len(product_details)
                if length == 0:
                    return {
                        'error': 'no_product'
                    }
                else:
                    return {
                        'products': product_details,
                        'length': length,
                        'prod_cat': prod_cat,
                        'prod_info': prod_info,
                        'cat_info': cat_info,
                    }
            else:
                both_sorting = []
                for p_id, prod_obj in cat_prod_dic.items():
                    for cate in prod_obj['public_categ_ids']:
                        if cate.width:
                            c_name = cate.name
                            c_name_number = c_name.split()[0]
                            if (float(c_name_number) >= float(
                                    minimu_width) and float(
                                c_name_number) <= float(maximum_width)):
                                product_data = product_template_pool.browse(
                                    cr, SUPERUSER_ID, p_id, context=context)
                                p_key = product_data.id
                                prod_arr = []
                                prod_arr.append(product_data.id)
                                prod_name['name'] = product_data.name
                                prod_id['id'] = product_data.id
                                prod_website[
                                    'website_published'] = (
                                    product_data.website_published)

                                product_details[p_key] = dict(
                                    prod_name.items() +
                                    prod_id.items() +
                                    prod_website.items())

                                if product_data not in both_sorting:
                                    both_sorting.append(product_data)
                get_all_dictionary = self.make_dictionary(both_sorting)
                prod_info = get_all_dictionary['prod_info']
                prod_cat = get_all_dictionary['prod_cat']
                cat_info = get_all_dictionary['cat_info']
                length = len(product_details)
                if length == 0:
                    return {
                        'error': 'no_product'
                    }
                else:
                    return {
                        'products': product_details,
                        'length': length,
                        'prod_cat': prod_cat,
                        'prod_info': prod_info,
                        'cat_info': cat_info,
                    }
        # -------------For both width and geomerty Filtering-------------------
        if product_data and minimu_width and maximum_width:

            prod_name = {}
            prod_website = {}
            prod_id = {}
            product_details = {}

            attrib_list = product_data
            attrib_set = []

            for i in attrib_list:
                attrib_set.append(str(i))

            if attrib_list:
                post['attrib'] = attrib_list

            if foam_sorting:
                both_sorting = []
                for p_id, p_obj in foam_sorting.items():
                    for cate in p_obj['public_categ_ids']:
                        if cate.width:
                            c_name = cate.name
                            c_name_number = c_name.split()[0]
                            if (float(c_name_number) >= float(
                                    minimu_width) and float(
                                c_name_number) <= float(maximum_width)):
                                product_data_obj = product_template_pool.browse(
                                    cr, SUPERUSER_ID, p_id, context=context)
                                for cat in product_data_obj['public_categ_ids']:
                                    for att_id in attrib_set:
                                        if int(cat.id) == int(att_id):
                                            p_key = product_data_obj.id
                                            prod_name[
                                                'name'] = product_data_obj.name
                                            prod_id['id'] = product_data_obj.id
                                            prod_website[
                                                'website_published'] = (
                                                product_data_obj.website_published)

                                            product_details[p_key] = dict(
                                                prod_name.items() +
                                                prod_id.items() +
                                                prod_website.items())
                                            if product_data_obj not in \
                                                    both_sorting:
                                                both_sorting.append(
                                                    product_data_obj)
                get_all_dictionary = self.make_dictionary(both_sorting)
                prod_info = get_all_dictionary['prod_info']
                prod_cat = get_all_dictionary['prod_cat']
                cat_info = get_all_dictionary['cat_info']
                length = len(product_details)
                if length == 0:
                    return {
                        'error': 'no_product'
                    }
                else:
                    return {
                        'products': product_details,
                        'length': length,
                        'prod_cat': prod_cat,
                        'prod_info': prod_info,
                        'cat_info': cat_info,
                    }
            else:
                both_sorting = []
                for p_id, prod_obj in cat_prod_dic.items():
                    for cate in prod_obj['public_categ_ids']:
                        if cate.width:
                            c_name = cate.name
                            c_name_number = c_name.split()[0]
                            if float(c_name_number) >= float(
                                    minimu_width) and float(
                                c_name_number) <= float(maximum_width):
                                product_data_obj = product_template_pool.browse(
                                    cr, SUPERUSER_ID, p_id, context=context)
                                for cat in product_data_obj['public_categ_ids']:
                                    for att_id in attrib_set:
                                        if int(cat.id) == int(att_id):
                                            p_key = product_data_obj.id
                                            prod_name[
                                                'name'] = product_data_obj.name
                                            prod_id['id'] = product_data_obj.id
                                            prod_website[
                                                'website_published'] = (
                                                product_data_obj.website_published)
                                            product_details[p_key] = dict(
                                                prod_name.items() +
                                                prod_id.items() +
                                                prod_website.items())
                                            if (product_data_obj not in
                                                    both_sorting):
                                                both_sorting.append(
                                                    product_data_obj)
                get_all_dictionary = self.make_dictionary(both_sorting)
                prod_info = get_all_dictionary['prod_info']
                prod_cat = get_all_dictionary['prod_cat']
                cat_info = get_all_dictionary['cat_info']
                length = len(product_details)
                if length == 0:
                    return {
                        'error': 'no_product'
                    }
                else:
                    return {
                        'products': product_details,
                        'length': length,
                        'prod_cat': prod_cat,
                        'prod_info': prod_info,
                        'cat_info': cat_info,
                    }

    def make_dictionary(self, both_sorting):
        cr = request.cr
        context = request.context
        pool = request.registry

        product_public_category_pool = pool['product.public.category']

        data = both_sorting
        prod_cat = {}
        prod_info = {}
        cat_name = {}
        cat_width = {}
        cat_geometrie = {}
        category_name = {}
        cat_main = {}
        cat_info = {}
        for d in data:
            key = d['id']
            for dd in d['public_categ_ids']:
                cat_ids = product_public_category_pool.search(
                    cr, SUPERUSER_ID, [('id', '=', int(dd.id))],
                    context=context)
                cat_id = product_public_category_pool.browse(
                    cr, SUPERUSER_ID, cat_ids, context=context)
                cat_key = cat_id.id
                category_name['name'] = cat_id.name
                cat_width['width'] = cat_id.width
                cat_geometrie['geometrie'] = cat_id.geometrie
                cat_main['main_category'] = cat_id.main_category

                cat_info[cat_key] = dict(
                    category_name.items() +
                    cat_width.items() +
                    cat_geometrie.items() +
                    cat_main.items())

                cat_name[dd] = cat_id.name
                if prod_cat.has_key(key):
                    old = []
                    old.extend(prod_cat[key])
                    old.append(cat_name[dd])
                    prod_cat[key] = old
                else:
                    prod_cat[key] = [cat_name[dd]]

                if prod_info.has_key(key):
                    old_info = []
                    old_info.extend(prod_info[key])
                    old_info.append(cat_key)
                    prod_info[key] = old_info
                else:
                    prod_info[key] = [cat_key]
        return {
            'prod_cat': prod_cat,
            'prod_info': prod_info,
            'cat_info': cat_info,
        }

    @http.route(['/shop/cart_update'],
                type='json', auth='public', website=True)
    def cart_update_json(self, *kw, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        res = cart_update(cr, uid, pool, kw, post, context=context)
        return res

    @http.route(['/shop/shopping_cart', '/shop/shopping_cart/<int:order_id>'],
                type='http', auth="user", website=True)
    def shopping_cart(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        sale_order_pool = pool.get('sale.order')
        sale_order_line_pool = pool.get('sale.order.line')
        product_material_pool = pool.get('product.material')

        product_material_ids = product_material_pool.search(
            cr, uid,
            [('foam', '=', False),
             ('activ', '=', True)],
            context=context)
        product_material_objects = product_material_pool.browse(
            cr, uid, product_material_ids, context=context)
        if not post.get('order_id'):
            sale_order = request.website.sale_get_order()
        else:
            sale_order = sale_order_pool.browse(
                cr, uid, int(post.get('order_id')), context=context)
            request.session['sale_order_id'] = int(post.get('order_id'))
        if not sale_order:
            return request.redirect('/shop')
        sale_order_line_ids = sale_order_line_pool.search(
            cr, SUPERUSER_ID, [('order_id', '=', sale_order.id)],
            context=context)
        sale_order_lines = sale_order_line_pool.browse(
            cr, SUPERUSER_ID, sale_order_line_ids, context=context)

        product_attribute_value_pool = pool.get('product.attribute.value')
        foam_pe_value_ids = product_attribute_value_pool.search(cr, uid, [
            ('attribute_id', '=', 'Foam PE Dimensions')], context=context)
        foam_pe_value_objects = product_attribute_value_pool.browse(
            cr, uid, foam_pe_value_ids, context=context)
        foam_pe_width_steps = []
        foam_pe_height_steps = []
        for foam_pe_value_object in foam_pe_value_objects:
            value = foam_pe_value_object.name
            foam_values = value.split('*')
            if foam_values and foam_values[0] not in foam_pe_width_steps:
                #     if float(foam_values[0]) < float(product.product_element_width):
                foam_pe_width_steps.append(foam_values[0])
            if foam_values and foam_values[1] not in foam_pe_height_steps:
                foam_pe_height_steps.append(foam_values[1])
        new_foam_pe_height_steps = []
        for foam_pe in foam_pe_height_steps:
            new_foam_pe_height_steps.append(float(foam_pe))
        foam_pe_height_steps = new_foam_pe_height_steps
        foam_pe_width_steps.sort()
        foam_pe_height_steps.sort()
        foam_pe_width_steps_range = len(foam_pe_width_steps)
        foam_pe_width_steps_range = range(1, foam_pe_width_steps_range + 1)
        foam_pe_height_steps_range = len(foam_pe_height_steps)
        foam_pe_height_steps_range = range(1, foam_pe_height_steps_range + 1)

        foam_pur_width_steps = []
        foam_pur_height_steps = []
        foam_pur_value_ids = product_attribute_value_pool.search(cr, uid, [
            ('attribute_id', '=', 'Foam Dimensions')], context=context)
        foam_pur_value_objects = product_attribute_value_pool.browse(
            cr, uid, foam_pur_value_ids, context=context)
        for foam_pur_value_object in foam_pur_value_objects:
            value = foam_pur_value_object.name
            foam_values = value.split('*')
            if foam_values and foam_values[0] not in foam_pur_width_steps:
                # if float(foam_values[0]) < float(product.product_element_width):
                foam_pur_width_steps.append(foam_values[0])
            if foam_values and foam_values[1] not in foam_pur_height_steps:
                foam_pur_height_steps.append(foam_values[1])
        foam_pur_width_steps.sort()
        foam_pur_height_steps.sort()
        foam_pur_width_steps_range = len(foam_pur_width_steps)
        foam_pur_width_steps_range = range(1, foam_pur_width_steps_range + 1)
        foam_pur_height_steps_range = len(foam_pur_height_steps)
        foam_pur_height_steps_range = range(1, foam_pur_height_steps_range + 1)

        crm_case_section_pool = pool.get('crm.case.section')
        res_users_pool = pool.get('res.users')
        res_user = res_users_pool.browse(cr, uid, uid, context=context)
        crm_case_section_ids = crm_case_section_pool.search(
            cr, uid, [], context=context)
        crm_case_sections = crm_case_section_pool.browse(
            cr, uid, crm_case_section_ids, context=context)
        user_crm_case_section = []
        for crm_case_section in crm_case_sections:
            if res_user in crm_case_section.member_ids:
                user_crm_case_section.append(crm_case_section.id)

        saleperson = False
        res_partner_pool = pool.get('res.partner')
        per_res_partner_ids = res_partner_pool.search(
            cr, uid, [('user_id', '=', uid)],
            context=context)
        team_res_partner_ids = res_partner_pool.search(
            cr, uid, [('section_id', 'in', user_crm_case_section)],
            context=context)
        res_partner_ids = per_res_partner_ids + team_res_partner_ids
        res_partners = res_partner_pool.browse(
            cr, uid, res_partner_ids, context=context)
        if user_crm_case_section:
            saleperson = True

        further_processing_pool = pool.get('further.processing')
        further_processing_ids = further_processing_pool.search(
            cr, uid, [('active', '=', True)])
        further_processing_objects = further_processing_pool.browse(
            cr, uid, further_processing_ids, context=context)

        employee_commission_pool = pool.get('employee.commission')
        employee_commission_ids = employee_commission_pool.search(cr, uid, [])
        employee_commissions = employee_commission_pool.browse(
            cr, uid, employee_commission_ids, context=context)
        max_commiss = 0
        min_commiss = 0
        for employee_commission in employee_commissions:
            if employee_commission.range_on < min_commiss:
                min_commiss = employee_commission.range_on
            if employee_commission.range_of > max_commiss:
                max_commiss = employee_commission.range_of

        crm_opportunity_pool = pool.get('crm.lead')
        crm_opportunity_ids = crm_opportunity_pool.search(
            cr, uid,
            [('partner_id', '=', sale_order.partner_id.id),
             ('type', '=', 'opportunity')])
        crm_opportunity = crm_opportunity_pool.browse(
            cr, uid, crm_opportunity_ids, context=context)
        default_pricelist = True
        product_pricelist_pool = pool.get('product.pricelist')
        product_pricelist_id = product_pricelist_pool.search(
            cr, uid, [('default', '=', True)])
        try:
            product_pricelist_id = product_pricelist_id[0]
        except:
            pass

        if sale_order.pricelist_id.id != product_pricelist_id:
            default_pricelist = False
        have_specific_pricelist = False
        if sale_order.partner_id.property_product_pricelist.id:
            if product_pricelist_id != sale_order.partner_id.property_product_pricelist.id:
                have_specific_pricelist = True

        have_contract_pricelist = False
        if sale_order.crm_lead_id:
            default_sale_order_id = sale_order_pool.search(
                cr, uid, [('crm_lead_id', '=', sale_order.crm_lead_id.id),
                          ('id', '!=', sale_order.id)])
            if default_sale_order_id:
                have_contract_pricelist = True
        values = {'materials': product_material_objects,
                  'sale_order': sale_order,
                  'default_pricelist': default_pricelist,
                  'have_specific_pricelist': have_specific_pricelist,
                  'have_contract_pricelist': have_contract_pricelist,
                  'sale_order_lines': sale_order_lines,
                  'saleperson': saleperson,
                  'customers': res_partners,
                  'opportunity': crm_opportunity,
                  'min_length': product_material_objects[
                      0].restriction_material_length_min,
                  'max_length': product_material_objects[
                      0].restriction_material_length_max,
                  'foam_pe_width_steps': foam_pe_width_steps,
                  'foam_pe_height_steps': foam_pe_height_steps,
                  'foam_pe_width_steps_range': foam_pe_width_steps_range,
                  'foam_pe_height_steps_range': foam_pe_height_steps_range,
                  'foam_pur_width_steps': foam_pur_width_steps,
                  'foam_pur_height_steps': foam_pur_height_steps,
                  'foam_pur_width_steps_range': foam_pur_width_steps_range,
                  'foam_pur_height_steps_range': foam_pur_height_steps_range,
                  'foam_pe_width_height': ['15.0*1', '15.0*5', '15.0*7',
                                           '25.0*1', '25.0*5', '25.0*7'],
                  'foam_pur_width_height': ['15.0*1', '15.0*2'],
                  'further_objects': further_processing_objects,
                  'max_commiss': max_commiss,
                  'min_commiss': min_commiss
                  }
        return request.website.render("website.shop_product_cart", values)

    @http.route(['/shop/shopping_cart_change'],
                type='json', auth='user', website=True)
    def shopping_cart_change(self, **post):
        cr = request.cr
        uid = request.uid
        res = {}
        context = request.context
        pool = request.registry

        sale_order_line_pool = pool.get('sale.order.line')
        sale_order_pool = pool.get('sale.order')
        sale_order_id = request.session.get('sale_order_id')
        sale_order = sale_order_pool.browse(
            cr, uid, sale_order_id, context=context)
        new_parameters = {}
        if post:
            post_data = post.get('data')
            if post_data and post_data.get('type') == 'foam':
                print 'CHANGE FOAM  >>>>>>>>>>>>>>>>>>>>'
            if post_data and post_data.get('type') == 'option':
                line_ids = post_data.get('products')
                params = post_data.get('form')

                for param in params:
                    if param.get('name') == 'material':
                        new_parameters['material_id'] = param.get('value')
                    elif param.get('name') == 'length':
                        if param.get('value'):
                            new_parameters['length'] = param.get('value')
                    elif param.get('name') == 'e_low_open_groove':
                        new_parameters['e_low_open_groove'] = param.get('value')
                    elif param.get('name') == 'glue':
                        new_parameters['glue'] = param.get('value')
                    elif param.get('name') == 'color_laser_marking':
                        new_parameters[
                            'color_laser_marking'] = param.get('value')
                    elif param.get('name') == 'print_logo':
                        new_parameters['print_logo'] = param.get('value')
                    elif param.get('name') == 'logo_for_print':
                        new_parameters['logo_for_print'] = param.get('value')

                further_processing_pool = pool.get('further.processing')
                further_processing_ids = further_processing_pool.search(
                    cr, uid, [('active', '=', True)])
                further_processing_objects = further_processing_pool.browse(
                    cr, uid, further_processing_ids, context=context)
                for further_processing in further_processing_objects:
                    for param in params:
                        if param.get('name') == further_processing.identifier:
                            new_parameters[further_processing.identifier] = True

                if new_parameters:
                    line_ids = change_line_option(
                        cr, uid, line_ids, pool, context, new_parameters)
                    res['line_ids'] = line_ids
            if post_data and post_data.get('type') == 'dublicate':
                line_ids = dublicate_lines(cr, uid, pool, context,
                                           post=post_data)
                res['line_ids'] = line_ids
            if post_data and post_data.get('type') == 'delete':
                res_dict = delete_lines(cr, uid, pool, context, post=post_data)
                res['materials'] = res_dict
            if post.get('type') == 'line_change':
                res = line_change_function(cr, uid, pool, context, post=post)

            function_type = post.get('type')
            if not function_type:
                function_type = post_data.get('type')
            res['type'] = function_type
        if len(new_parameters) == 1 and not new_parameters.get('length'):
            calculate_new_price(cr, SUPERUSER_ID, pool, res, context)
        elif len(new_parameters) > 1:
            calculate_new_price(cr, SUPERUSER_ID, pool, res, context)

        sale_order_line_ids = sale_order_line_pool.search(
            cr, uid,
            [('order_id', '=', sale_order.id)],
            context=context)
        sale_order_lines = sale_order_line_pool.browse(
            cr, uid, sale_order_line_ids, context=context)
        calculate_commission(sale_order)
        sale_order.button_dummy()
        return shopping_cart_html(sale_order, sale_order_lines)

    @http.route(['/shop/shopping_cart/change_customer'],
                type='json', auth="user", website=True)
    def change_customer(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        try:
            customer_id = int(post.get('customer'))
        except:
            customer_id = None
        sale_order_pool = pool.get('sale.order')
        res_partner_pool = pool.get('res.partner')
        sale_order_id = request.session.get('sale_order_id')
        sale_order = sale_order_pool.browse(
            cr, uid, sale_order_id, context=context)
        customer_name = post.get('customer_name')
        res_users = pool.get('res.users').browse(cr, uid, uid, context=context)
        try:
            res_users = res_users[0]
        except:
            pass
        crm_case_section_pool = pool.get('crm.case.section')
        crm_case_section_ids = crm_case_section_pool.search(
            cr, uid, [], context=context)
        crm_case_sections = crm_case_section_pool.browse(
            cr, uid, crm_case_section_ids, context=context)
        section = False
        section_id = ''
        for crm_case_section in crm_case_sections:
            if not section:
                for user in crm_case_section.member_ids:
                    if user == res_users:
                        section_id = crm_case_section.id
        if customer_id or customer_name:
            if customer_id != 0:
                values = sale_order_pool.onchange_partner_id(
                    cr, SUPERUSER_ID, [sale_order_id],
                    customer_id, context=context)['value']
                partner = res_partner_pool.browse(
                    cr, uid, customer_id, context=context)
                sale_order_pool.write(
                    cr, uid, sale_order.id,
                    {'partner_id': customer_id,
                     'fiscal_position': values.get('fiscal_position'),
                     'partner_invoice_id': values.get('partner_invoice_id'),
                     'partner_shipping_id': values.get('partner_shipping_id'),
                     'payment_term': values.get('payment_term'),
                     'pricelist_id': values.get('pricelist_id'),
                     'section_id': partner.section_id.id,
                     },
                    context=context)
            elif customer_name and customer_id == 0:

                res_partner = res_partner_pool.create(
                    cr, uid, {
                        'name': customer_name,
                        'section_id': section_id,
                        'user_id': None,
                        'is_company': True,
                    }
                )
                values = sale_order_pool.onchange_partner_id(
                    cr, SUPERUSER_ID, [sale_order_id],
                    res_partner, context=context)['value']

                sale_order_pool.write(
                    cr, uid, sale_order.id,
                    {'partner_id': res_partner,
                     'fiscal_position': values.get('fiscal_position'),
                     'partner_invoice_id': values.get('partner_invoice_id'),
                     'partner_shipping_id': values.get('partner_shipping_id'),
                     'payment_term': values.get('payment_term'),
                     'pricelist_id': values.get('pricelist_id'),
                     'user_id': None,
                     'section_id': section_id,
                     },
                    context=context)
        return True

    @http.route(['/shop/shopping_cart/change_case'],
                type='json', auth="user", website=True)
    def change_case(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        try:
            case_id = int(post.get('case'))
        except:
            case_id = None
        sale_order_pool = pool.get('sale.order')
        sale_order_id = request.session.get('sale_order_id')
        sale_order = sale_order_pool.browse(
            cr, uid, sale_order_id, context=context)
        case_name = post.get('case_name')
        crm_lead_pool = pool.get('crm.lead')
        if case_id or case_name:
            if case_id != 0:
                sale_order_pool.write(
                    cr, SUPERUSER_ID, sale_order.id,
                    {'crm_lead_id': case_id,
                     'sale_use_default_pricelist': 2
                     },
                    context=context)
            elif case_name and case_id == 0:

                case_id = crm_lead_pool.create(
                    cr, SUPERUSER_ID, {'name': case_name,
                                       'type': 'opportunity',
                                       'user_id': sale_order.user_id.id,
                                       'section_id': sale_order.section_id.id,
                                       'partner_id': sale_order.partner_id.id,
                                       },
                    context=context)

                values = crm_lead_pool.on_change_partner_id(
                    cr, SUPERUSER_ID, case_id, sale_order.partner_id.id,
                    context=context)
                crm_lead_pool.write(
                    cr, SUPERUSER_ID, case_id, values.get('value'),
                    context=context)
                sale_order_pool.write(cr, SUPERUSER_ID, sale_order.id,
                                      {'crm_lead_id': case_id,
                                       'sale_use_default_pricelist': 2},
                                      context=context)
                return {'link': '/shop/opportunity/{}'.format(case_id)}

            if case_id:
                sale_order.check_opportunity()
        return True

    @http.route(['/shop/shopping_cart/use_default_pricelist'],
                type='json', auth="user", website=True)
    def use_default_pricelist(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        sale_order_pool = pool.get('sale.order')
        product_pricelist_pool = pool.get('product.pricelist')
        sale_order_id = request.session.get('sale_order_id')
        sale_order = sale_order_pool.browse(
            cr, uid, sale_order_id, context=context)
        default_pricelist = int(post.get('default_pricelist'))
        product_pricelist_id = product_pricelist_pool.search(
            cr, uid, [('default', '=', True)])
        try:
            product_pricelist_id = product_pricelist_id[0]
        except:
            pass
        if default_pricelist == 2:
            sale_order.write({'pricelist_id': product_pricelist_id})
            sale_order.write({'sale_use_default_pricelist': default_pricelist})
            sale_order.check_opportunity()
            return True
        else:
            partner_id = sale_order.partner_id
            product_pricelist_id = partner_id.property_product_pricelist.id
        val = {'pricelist_id': product_pricelist_id,
               'sale_use_default_pricelist': default_pricelist}
        if default_pricelist != 2:
            val['crm_lead_id'] = None
        sale_order.write(val)
        res = {'line_ids': [line.id for line in sale_order.order_line],
               'type': u'option'}
        sale_order.calculate_new_price(res)
        sale_order.button_dummy()
        return True

    @http.route(['/shop/assign_to_me'],
                type='json', auth="user", website=True)
    def assign_to_me(self):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        sale_order_id = request.session.get('sale_order_id')
        partner = pool.get('res.users').browse(
            cr, uid, uid, context=context).partner_id
        sale_order_pool = pool.get('sale.order')
        values = sale_order_pool.onchange_partner_id(
            cr, SUPERUSER_ID, [sale_order_id],
            partner.id, context=context)['value']

        sale_order_pool.write(
            cr, uid, sale_order_id,
            {'partner_id': partner.id,
             'fiscal_position': values.get('fiscal_position'),
             'partner_invoice_id': values.get('partner_invoice_id'),
             'partner_shipping_id': values.get('partner_shipping_id'),
             'payment_term': values.get('payment_term'),
             'pricelist_id': values.get('pricelist_id'),
             },
            context=context)
        return {}

    @http.route(['/shop/shopping_cart/change_product_parametr/'],
                type='json', auth='user', website=True)
    def change_parametr(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        sale_order_line_pool = pool.get('sale.order.line')
        sale_order_pool = pool.get('sale.order')
        product_product_pool = pool.get('product.product')
        sale_order_line = sale_order_line_pool.browse(
            cr, uid, int(post.get('line_id')), context=context)
        order_id = sale_order_line.order_id.id
        if post.get('type') == 'name':
            sale_order_line.product_id.write({'second_name': post.get('text')})
            sale_order_line_ids = sale_order_line_pool.search(
                cr, uid,
                [('order_id', '=', order_id)],
                context=context)
            sale_order_lines = sale_order_line_pool.browse(
                cr, uid, sale_order_line_ids, context=context)
            order = sale_order_pool.browse(cr, uid, order_id, context=context)
            return shopping_cart_html(order, sale_order_lines)
        new_area = ''
        if post.get('type') == 'width':
            new_width = post.get('text').replace(',', '.')
            old_width = sale_order_line.product_id.product_element_width
            k = float(new_width) / old_width
            old_area = sale_order_line.product_id.product_square
            new_area = round((old_area * k), 2)
        else:
            new_width = sale_order_line.product_id.product_element_width

        if post.get('type') == 'height':
            new_height = post.get('text').replace(',', '.')
            old_height = sale_order_line.product_id.product_element_height
            k = float(new_height) / old_height
            old_area = sale_order_line.product_id.product_square
            new_area = round((old_area * k), 2)
        else:
            new_height = sale_order_line.product_id.product_element_height

        if post.get('type') == 'area':
            new_area = float(post.get('text').replace(',', '.'))
        elif not new_area:
            new_area = sale_order_line.product_id.product_square

        sale_order_line.product_id.create_second_name()

        product_id = product_product_pool.search(
            cr, uid,
            [
                ('material_id', '=', sale_order_line.product_id.material_id.id),
                ('length', '=', sale_order_line.product_id.length),
                ('product_tmpl_id', '=',
                 sale_order_line.product_id.product_tmpl_id.id),
                ('product_element_height', '=', new_height),
                ('product_element_width', '=', new_width),
                ('product_square', '=', new_area),
            ], context=context)
        if not product_id:
            product_tmpl_id = sale_order_line.product_id.product_tmpl_id
            categ_id = \
                sale_order_line.product_id.material_id.categ_id.id
            if sale_order_line.product_id.owned_by == 'thermevo':
                categ_id = \
                    sale_order_line.product_id.material_id.thermevo_categ_id.id
            vals = {
                'profile_density':
                    sale_order_line.product_id.material_id.density,
                'categ_id': categ_id,
                'material_id': sale_order_line.product_id.material_id.id,
                'have_glue': False,
                'length': 6.0,
                'product_element_width': new_width,
                'product_element_height': new_height,
                'product_square': new_area,
                'product_tmpl_id': product_tmpl_id.id,
                'active': True
            }
            product_id = product_product_pool.create(
                cr, SUPERUSER_ID, vals, context=context)
            product_product = product_product_pool.browse(
                cr, uid, product_id, context=context)
            product_product.create_article()
            product_product.create_second_name()

            extruder_vals = {
                'product_tmpl_id':
                    product_tmpl_id.extruder_template_id.id,
                'second_name': '%s for <%s>' % (
                    product_tmpl_id.extruder_template_id.name,
                    product_product.second_name)
            }
            extruder_product_id = product_product_pool.create(
                cr, uid, extruder_vals, context=context)

            product_product_pool.write(
                cr, uid, product_product.id,
                {'extruder_product_id': extruder_product_id},
                context=context)
            line = request.website.sale_get_order(force_create=1)._cart_update(
                product_id=int(product_id),
                add_qty=sale_order_line.product_uom_qty,
                set_qty=sale_order_line.product_uom_qty)
            if product_tmpl_id.owned_by == 'Customer':
                add_extruder_product(
                    cr, SUPERUSER_ID, pool, int(line.get('line_id')),
                    {'final_tooling_price': 0},
                    context)
            delete_line(cr, SUPERUSER_ID, pool,
                        context, order_id, sale_order_line)
            line_ids = [int(line.get('line_id'))]
        else:
            extruder_name = (
                sale_order_line.product_id.extruder_product_id.product_tmpl_id.name)
            extruder_second_name = '%s for < %s >' % (
                extruder_name, sale_order_line.product_id.second_name)
            sale_order_line.product_id.extruder_product_id.write(
                {'second_name': extruder_second_name})
            old_product_id = sale_order_line.product_id
            need_sale_order_lines = sale_order_line_pool.search(
                cr, uid,
                [('product_id', '=', old_product_id.id)],
                context=context)

            sale_order_line.product_id.write(
                {'product_id': product_id[0]})
            line_ids = [int(post.get('line_id'))] + need_sale_order_lines
        res = {
            'line_ids': line_ids,
            'type': 'option'
        }
        calculate_new_price(cr, SUPERUSER_ID, pool, res, context)

        sale_order_line_ids = sale_order_line_pool.search(
            cr, uid,
            [('order_id', '=', order_id)],
            context=context)
        sale_order_lines = sale_order_line_pool.browse(
            cr, SUPERUSER_ID, sale_order_line_ids, context=context)
        order = sale_order_pool.browse(cr, SUPERUSER_ID, order_id,
                                       context=context)
        calculate_commission(order)
        order.check_opportunity()
        return shopping_cart_html(order, sale_order_lines)

    @http.route(['/shop/change_product_material/'], type='json', auth="user",
                methods=['POST'], website=True)
    def change_product_material(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        product_material_pool = pool.get('product.material')
        try:
            material_id = int(post.get('material_id'))
        except:
            return {'min_length': 0, 'max_length': 0, 'foam_type': '-',
                    'interval': 0, 'gluewire': 0, 'minimum_purchase': 0, }
        product_material_objects = product_material_pool.browse(
            cr, uid, material_id, context=context)

        foam_type = ''
        if product_material_objects.additional_pur:
            foam_type = 'PUR'
        elif product_material_objects.additional_pe:
            foam_type = 'PE'

        res = {
            'min_length':
                product_material_objects.restriction_material_length_min,
            'max_length':
                product_material_objects.restriction_material_length_max,
            'foam_type': ' ' + foam_type,
            'interval':
                product_material_objects.restriction_material_length_step,
            'gluewire': product_material_objects.additional_gluewire,
            'minimum_purchase': product_material_objects.minimum_purchase,
        }
        return res

    @http.route(['/shop/delivery_address'],
                type='http', auth="user", website=True)
    def check_delivery_address(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry

        res_users_pool = pool.get('res.users')
        product_template_pool = pool.get('product.template')
        product_product_pool = pool.get('product.product')

        sale_order_pool = pool.get('sale.order')
        sale_order_id = request.session.get('sale_order_id')
        sale_order = sale_order_pool.browse(cr, uid, sale_order_id,
                                            context=context)

        partner = res_users_pool.browse(
            cr, uid, uid, context=context).partner_id
        partner_id = partner.id
        product_template_ids = product_template_pool.search(
            cr, uid, [('custom_product_customer', '=', partner_id),
                      ('owned_by', '=', 'Customer')],
            context=context
        )
        redirection = None
        try:
            #     clean_database(cr, uid, pool, product_template_ids, context=context)
            redirection = self.checkout_redirection(sale_order)
        except:
            pass
        if redirection:
            return redirection

        question_pool = pool.get('delivery.address.questions.faq')
        question_ids = question_pool.search(
            cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        questions = question_pool.browse(
            cr, SUPERUSER_ID, question_ids, context=context)
        crm_case_section_pool = pool.get('crm.case.section')
        res_users_pool = pool.get('res.users')
        res_user = res_users_pool.browse(cr, uid, uid, context=context)
        crm_case_section_ids = crm_case_section_pool.search(
            cr, uid, [], context=context)
        crm_case_sections = crm_case_section_pool.browse(
            cr, uid, crm_case_section_ids, context=context)
        user_crm_case_section = []
        for crm_case_section in crm_case_sections:
            if res_user in crm_case_section.member_ids:
                user_crm_case_section.append(crm_case_section.id)
        saleperson = False
        res_partner_pool = pool.get('res.partner')
        per_res_partner_ids = res_partner_pool.search(
            cr, uid, [('user_id', '=', uid)],
            context=context)
        team_res_partner_ids = res_partner_pool.search(
            cr, uid, [('section_id', 'in', user_crm_case_section)],
            context=context)
        res_partner_ids = per_res_partner_ids + team_res_partner_ids
        res_partners = res_partner_pool.browse(
            cr, uid, res_partner_ids, context=context)
        if user_crm_case_section:
            saleperson = True
        values = {
            'questions': questions,
            'partner': partner,
            'sale_order': sale_order,
            'saleperson': saleperson,
            'customers': res_partners,
        }
        values.update(self.checkout_values())
        return request.website.render("website.shop_delivery_address", values)

    @http.route(['/shop/change_shipping_address'],
                type='json', auth="public", website=True)
    def change_shipping_address(self, delivery_address_id):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        res = {}
        partner_pool = pool.get('res.partner')
        try:
            res_partner = partner_pool.browse(
                cr, uid, int(delivery_address_id), context=context)
            if res_partner:
                res = {
                    'shipping_name': res_partner.name,
                    'shipping_street': res_partner.street,
                    'shipping_city': res_partner.city,
                    'shipping_country_id': res_partner.country_id.id,
                    'shipping_phone': res_partner.phone,
                    'shipping_zip': res_partner.zip,
                }
        except:
            pass
        return res

    @http.route(['/shop/delivery_address/check_country'],
                type='json', auth="public", website=True)
    def check_country_verify(self, **kw):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        country = kw.get('country')
        res_users_pool = pool.get('res.users')
        res_country_pool = pool.get('res.country')
        country_group_pool = pool.get('res.country.group')
        res_users_object = res_users_pool.browse(cr, uid, uid, context=context)
        partner = res_users_object.partner_id
        res_country_object = res_country_pool.browse(
            cr, uid, int(country), context=context)

        country_group_ids = country_group_pool.search(
            cr, uid, [('default_country_group', '=', True)], context=context)
        country_group_objects = country_group_pool.browse(
            cr, uid, country_group_ids, context=context)

        property_account_position = (
            country_group_objects.default_account_fiscal_position_id)
        country_group_ids = country_group_pool.search(cr, uid, [],
                                                      context=context)
        country_group_objects = country_group_pool.browse(
            cr, uid, country_group_ids, context=context)
        res = {}
        for country_group_object in country_group_objects:
            if res_country_object in country_group_object.country_ids:
                property_account_position = (
                    country_group_object.default_account_fiscal_position_id)
                if country_group_object.name == 'Banned countries(stop quote)':
                    res['banned'] = 1

        partner.write({'property_account_position': property_account_position,
                       'country_id': country})
        msg = ' %s ' % property_account_position.name
        res['msg'] = msg
        return res

    @http.route(['/shop/delivery_address/checkvat'],
                type='json', auth="public", website=True)
    def check_vat_varify(self, **kw):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        res_partner = pool.get('res.partner')

        vat = kw.get('vat')
        country = kw.get('country')
        country_code = vat[0:2]
        res_users_pool = pool.get('res.users')
        res_users_object = res_users_pool.browse(cr, uid, uid, context=context)
        partner = res_users_object.partner_id
        check_vat = False

        res_country_pool = pool.get('res.country')
        country_group_pool = pool.get('res.country.group')
        res_country_objects = res_country_pool.browse(
            cr, uid, int(country), context=context)
        if res_country_objects and country_code != res_country_objects.code:
            country_group_ids = country_group_pool.search(
                cr, uid, [('default_country_group', '=', True)],
                context=context)
            country_group_objects = country_group_pool.browse(
                cr, uid, country_group_ids, context=context)

            res_partner.write(cr, SUPERUSER_ID, partner.id, {
                'property_account_position':
                    country_group_objects.default_account_fiscal_position_id.id,
                'country_id': country}, context=context)
            msg = ' %s ' % (
                country_group_objects.default_account_fiscal_position_id.name)
            res = {'check_vat': check_vat, 'msg': msg}
            return res

        if not check_vat:
            try:
                res_partner.write(cr, SUPERUSER_ID, partner.id,
                                  {'vat': vat, 'country_id': country},
                                  context=context)
            except:
                pass

            try:
                check_vat = res_partner.check_vat(
                    cr, uid, partner.id, context=context)
            except:
                check_vat = False

        msg = ''
        if check_vat:
            country_code = vat[0:2]
            res_country_id = res_country_pool.search(
                cr, uid, [('code', '=', country_code)], context=context)
            res_country_object = res_country_pool.browse(
                cr, uid, res_country_id, context=context)
            country_group_ids = country_group_pool.search(
                cr, uid, [], context=context)
            country_group_objects = country_group_pool.browse(
                cr, uid, country_group_ids, context=context)

            for country_group_object in country_group_objects:
                if res_country_object in country_group_object.country_ids:
                    res_partner.write(cr, SUPERUSER_ID, partner.id, {
                        'property_account_position':
                            country_group_object.account_fiscal_position_id.id,
                        'country_id': country}, context=context)
                    msg = ' %s ' % (
                        country_group_object.account_fiscal_position_id.name)
        else:
            country_group_ids = country_group_pool.search(
                cr, uid, [('default_country_group', '=', True)],
                context=context)
            country_group_objects = country_group_pool.browse(
                cr, uid, country_group_ids, context=context)
            res_partner.write(
                cr, SUPERUSER_ID, partner.id,
                {'property_account_position':
                     country_group_objects.default_account_fiscal_position_id.id,
                 'country_id': country, 'vat': ''
                 }, context=context)
            msg = ' %s ' % (
                country_group_objects.default_account_fiscal_position_id.name)

        res = {'check_vat': check_vat, 'msg': msg}
        return res

    @http.route(['/shop/order_confirm'],
                type='http', auth="public", website=True)
    def order_confirm(self, page=0, category=None, search='',
                      **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        vat_varify = post.get('vat_verify')
        vat = post.get('vat')
        country = post.get('country_id')
        res_country_group_pool = pool.get('res.country.group')
        account_fiscal_position_pool = pool.get('account.fiscal.position')
        res_partner_pool = pool.get('res.partner')

        sale_order_pool = pool.get('sale.order')
        sale_order_id = request.session.get('sale_order_id')
        order = sale_order_pool.browse(
            cr, uid, sale_order_id, context=context)
        partner_id = order.partner_id.id
        country_groups_ids = res_country_group_pool.search(
            cr, SUPERUSER_ID, [], context=context)
        country_groups = res_country_group_pool.browse(
            cr, SUPERUSER_ID, country_groups_ids, context=context)

        if not order:
            return request.redirect("/shop")
        fiscal_position_ids = []
        for group in country_groups:
            for country_id in group.country_ids.ids:
                if int(country) == country_id:
                    fiscal_group = account_fiscal_position_pool.search(
                        cr, SUPERUSER_ID, [('country_group_id', '=', group.id)],
                        context=context)
                    # _logger.info('fiscal_group = %s', fiscal_group)
                    if fiscal_group:
                        if vat_varify == 'true':
                            ids = account_fiscal_position_pool.search(
                                cr, SUPERUSER_ID,
                                [('country_group_id', '=', group.id),
                                 ('vat_required', '=', True)], context=context)
                        else:
                            ids = account_fiscal_position_pool.search(
                                cr, SUPERUSER_ID,
                                [('country_group_id', '=', group.id),
                                 ('vat_required', '=', False)], context=context)
                        fiscal_position_ids = fiscal_position_ids + ids
                    else:
                        # sale_order_pool.unlink(cr, SUPERUSER_ID, [order.id],
                        #                       context=context)
                        return request.redirect("/page/banned")
        res_partner_value = {}
        if fiscal_position_ids:
            res_partner_value = {
                'property_account_position': int(fiscal_position_ids[0]),
                'vat': vat,
            }
            send_shop_test_message(
                {'project_name': 'first_step_cart',
                 'sale': fiscal_position_ids})

        if post.get('name'):
            res_partner_value['name'] = post.get('name')
        if post.get('last_name'):
            res_partner_value['last_name'] = post.get('last_name')
        if post.get('company'):
            res_partner_value['company_name'] = post.get('company')
        if post.get('email'):
            res_partner_value['email'] = post.get('email')
        if post.get('zip'):
            res_partner_value['zip'] = post.get('zip')
        if post.get('phone'):
            res_partner_value['phone'] = post.get('phone')
        if post.get('country_id'):
            res_partner_value['country_id'] = int(post.get('country_id'))
        if post.get('city'):
            res_partner_value['city'] = post.get('city')
        if post.get('property_account_position'):
            name = post.get('property_account_position')
            property_account_position = pool.get(
                'account.fiscal.position').search(cr, uid,
                                                  [('name', '=', name)])
            if property_account_position:
                try:
                    property_account_position = property_account_position[0]
                except:
                    pass
                res_partner_value[
                    'property_account_position'] = property_account_position
        if post.get('vat'):
            res_partner_value['vat'] = post.get('vat')
        if post.get('street'):
            res_partner_value['street'] = post.get('street')
        res_partner_pool.write(cr, SUPERUSER_ID, partner_id,
                               res_partner_value, context=context)

        request.website.sale_get_order(update_pricelist=True, context=context)
        if post.get('shipping_id'):
            try:
                order.write({'carrier_id': int(post.get('shipping_id'))})
            except:
                pass
        if post.get('another_address'):
            if int(post.get('delivery_address')) != 0:
                sale_order_pool.write(
                    cr, uid, order.id,
                    {'partner_shipping_id': int(post.get('delivery_address'))},
                    context=context)
            else:
                shipping_value = {
                    'name': post.get('shipping_name'),
                    'city': post.get('shipping_city'),
                    'phone': post.get('shipping_phone'),
                    'zip': post.get('shipping_zip'),
                    'country_id': post.get('shipping_country_id'),
                    'street': post.get('shipping_street'),
                    'type': 'delivery',
                    'parent_id': order.partner_id.id,
                    'use_parent_address': False,
                }
                partner_shipping_id = res_partner_pool.create(
                    cr, uid, shipping_value, context=context)
                sale_order_pool.write(
                    cr, uid, order.id,
                    {'partner_shipping_id': partner_shipping_id},
                    context=context)
                order.button_dummy()

        now = datetime.now()
        days = 0
        if order.validity_date:
            days = ((datetime.strptime(order.validity_date,
                                       '%Y-%m-%d') -
                     datetime.now()).days + 1)
        values = {
            'quotation': order,
            'message': False,
            'option': bool(filter(lambda x: not x.line_id, order.options)),
            'order_valid': (not order.validity_date) or (
                now <= (datetime.strptime(order.validity_date,
                                          '%Y-%m-%d'))),
            'days_valid': days,
        }
        send_shop_test_message(
            {'project_name': 'price_q',
             'sale': order})
        return request.website.render('website_quote.so_quotation', values)

    @http.route(['/shop/order/<model("sale.order"):sale>'],
                type='http', auth="user", website=True)
    def sale_order(self, page=0, category=None, search='', **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry

        sale_order = post.get('sale')

        res_user = pool.get('res.users')
        res_user_ids = res_user.search(
            cr, uid, [('id', '=', uid)], context=context)
        res_user_odj = res_user.browse(cr, uid, res_user_ids, context=context)

        question_pool = pool.get('commercial.proposal.questions.faq')
        question_ids = question_pool.search(
            cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        questions = question_pool.browse(
            cr, SUPERUSER_ID, question_ids, context=context)

        commercial_proposal_pool = pool.get('commercial.proposal')
        commercial_proposal_ids = commercial_proposal_pool.search(
            cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        commercial_proposals = commercial_proposal_pool.browse(
            cr, SUPERUSER_ID, commercial_proposal_ids, context=context)
        order_link = self.get_order_link(
            cr, uid, sale_order.id, context=context)
        values = {
            'questions': questions,
            'user': res_user_odj,
            'sale_order': sale_order,
            'commercial_proposal_content': commercial_proposals,
            'order_link': order_link
        }
        return request.website.render("website.sale_order", values)

    @http.route(['/shop/list_of_order/page/<int:page>',
                 '/shop/list_of_order'
                 ], type='http', auth="user", website=True)
    def list_of_order_page(self, page=1, category=None, search='', **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry

        keep = QueryURL('/shop/sale_quotation_list', search=search)
        res_users_pool = pool.get('res.users')
        sale_order = pool.get('sale.order')
        user = res_users_pool.browse(cr, uid, uid, context=context)

        total = len(sale_order.search(cr, uid, [
            ('partner_id', '=', user.partner_id.id),
            ('state', 'in', ['manual', 'progress'])], context=context))

        sale_order_list = []
        order_state_dic = {}
        state = ''
        if search == '' or search == 'byordernumber' or search == 'byorderdate':
            sale_order_data = sale_order.search(
                cr, uid, [('partner_id', '=', user.partner_id.id),
                          ('state', 'in', ['manual', 'progress'])],
                offset=(page - 1) * self._post_per_page,
                limit=self._post_per_page,
                context=context)
            for order in sale_order_data:
                orders = sale_order.browse(cr, uid, order, context=context)
                sale_order_list.append(orders)

                credit_bal = 0.0
                for payment in orders.invoice_ids.payment_ids:
                    credit_bal += payment.credit

                if credit_bal == 0.0:
                    state = 'Wait for payment'
                elif credit_bal == orders.amount_total:
                    state = 'Full shipping'
                elif credit_bal < orders.amount_total:
                    state = 'Part shipping'
                order_state_dic[orders.id] = state

        if search == 'bystatus':
            sale_order_data = sale_order.search(
                cr, uid, [
                    ('partner_id', '=', user.partner_id.id),
                    ('state', 'in', ['manual', 'progress'])], context=context)
            for order in sale_order_data:
                orders = sale_order.browse(cr, uid, order, context=context)
                sale_order_list.append(orders)

                credit_bal = 0.0
                for payment in orders.invoice_ids.payment_ids:
                    credit_bal += payment.credit

                if credit_bal == 0.0:
                    state = 'Wait for payment'
                elif credit_bal == orders.amount_total:
                    state = 'Full shipping'
                elif credit_bal < orders.amount_total:
                    state = 'Part shipping'
                order_state_dic[orders.id] = state
            sorted_keys = sorted(order_state_dic, key=order_state_dic.get)
            sale_order_list = sale_order.browse(
                cr, uid, sorted_keys, context=context)
            sale_order_list = sale_order_list[
                              ((page - 1) * self._post_per_page):(
                                  self._post_per_page * page)]

        if search == 'bytotal':
            sale_order_data = sale_order.search(cr, uid, [
                ('partner_id', '=', user.partner_id.id),
                ('state', 'in', ['manual', 'progress'])], context=context)
            for order in sale_order_data:
                orders = sale_order.browse(cr, uid, order, context=context)
                sale_order_list.append(orders)

                credit_bal = 0.0
                for payment in orders.invoice_ids.payment_ids:
                    credit_bal += payment.credit

                if credit_bal == 0.0:
                    state = 'Wait for payment'
                elif credit_bal == orders.amount_total:
                    state = 'Full shipping'
                elif credit_bal < orders.amount_total:
                    state = 'Part shipping'
                order_state_dic[orders.id] = state
            get_sale_order_list_total = sorted(
                sale_order_list,
                key=lambda quotation: quotation.amount_total)
            sale_order_list = get_sale_order_list_total[
                              ((page - 1) * self._post_per_page):(
                                  self._post_per_page * page)]

        if search == 'bytotaltaxes':
            sale_order_data = sale_order.search(cr, uid, [
                ('partner_id', '=', user.partner_id.id),
                ('state', 'in', ['manual', 'progress'])], context=context)
            for order in sale_order_data:
                orders = sale_order.browse(cr, uid, order, context=context)
                sale_order_list.append(orders)

                credit_bal = 0.0
                for payment in orders.invoice_ids.payment_ids:
                    credit_bal += payment.credit

                if credit_bal == 0.0:
                    state = 'Wait for payment'
                elif credit_bal == orders.amount_total:
                    state = 'Full shipping'
                elif credit_bal < orders.amount_total:
                    state = 'Part shipping'
                order_state_dic[orders.id] = state
            get_sale_order_list_total = sorted(
                sale_order_list,
                key=lambda quotation: quotation.amount_untaxed)
            sale_order_list = get_sale_order_list_total[
                              ((page - 1) * self._post_per_page):(
                                  self._post_per_page * page)]

        if search:
            post["search"] = search

        page_url = "/shop/list_of_order"
        pager = request.website.pager(
            url=page_url,
            total=total,
            page=page,
            step=self._post_per_page,
            url_args=post,
        )

        question_obj = pool['list.of.orders.questions.faq']
        question_ids = question_obj.search(
            cr, SUPERUSER_ID, [], offset=0, limit=1, context=context)
        questions = question_obj.browse(
            cr, SUPERUSER_ID, question_ids, context=context)

        values = {
            'questions': questions,
            'orders': sale_order_list,
            'pager': pager,
            'state': order_state_dic,
            'keep': keep,
            'search': search,
        }
        return request.website.render("website.list_of_order", values)

    @http.route(['/shop/create_new_order'],
                type='http', auth="user", website=True)
    def create_new_order(self):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        sale_order_pool = pool.get('sale.order')
        res_users_pool = pool.get('res.users')
        user = res_users_pool.browse(cr, uid, uid, context=context)
        value = sale_order_pool.onchange_partner_id(
            cr, SUPERUSER_ID, [], user.partner_id.id,
            context=context).get('value')
        value['partner_id'] = user.partner_id.id
        new_sale_order_id = sale_order_pool.create(
            cr, SUPERUSER_ID, value, context=context)
        request.session['sale_order_id'] = new_sale_order_id
        return request.redirect("/shop")

    @http.route(['/shop/invoice/invoice_send'
                 ], type='json', auth="user", website=True)
    def invoice_page_mail_send(self, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry

        account_invoice_pool = pool.get('account.invoice')
        account_invoice = account_invoice_pool.browse(
            cr, uid, int(post.get('account_invoice')), context=context)

        email_template_obj = pool['email.template']

        template_ids = email_template_obj.search(cr, uid, [
            ('name', '=', 'Invoice - Send by Email')], context=context)

        email = email_template_obj.browse(cr, uid, template_ids[0])
        email_template_obj.write(cr, uid, template_ids,
                                 {'email_from': email.email_from,
                                  'email_to': email.email_to,
                                  'subject': email.subject,
                                  })

        email_template_obj.send_mail(
            cr, uid, template_ids[0], account_invoice.id, True, context=context)

        return True

    @http.route(['/shop/ordered_goods/page/<int:page>',
                 '/shop/ordered_goods'],
                type='http', auth="user", website=True)
    def ordered_goods_page(self, page=1, category=None, search='', **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        keep = QueryURL('/shop/ordered_goods', search=search)
        sale_order = pool.get('sale.order')
        res_users = pool.get('res.users')
        sale_order_line = pool.get('sale.order.line')

        user = res_users.browse(cr, uid, uid, context=context)

        sale_order_data = sale_order.search(cr, uid, [
            ('partner_id', '=', user.partner_id.id)], context=context)
        total_line = len(sale_order_line.search(cr, uid, [
            ('order_id', 'in', sale_order_data)], context=context))

        sale_order_list = []
        if search == '' or search == 'byarticle':
            sale_order_data_line = sale_order_line.search(
                cr, uid, [('order_id', 'in', sale_order_data)],
                offset=(page - 1) * self._post_per_page,
                limit=self._post_per_page,
                context=context)
            for order in sale_order_data_line:
                orders = sale_order_line.browse(cr, uid, order,
                                                context=context)
                sale_order_list.append(orders)

        if search == 'byname':
            sale_order_data_line = sale_order_line.search(
                cr, uid, [('order_id', 'in', sale_order_data)], context=context)
            orders = sale_order_line.browse(
                cr, uid, sale_order_data_line, context=context)
            get_sale_order_list_total = sorted(
                orders, key=lambda quotation: quotation.name)
            sale_order_list = get_sale_order_list_total[
                              ((page - 1) * self._post_per_page):(
                                  self._post_per_page * page)]

        if search:
            post["search"] = search

        pageUrl = "/shop/ordered_goods"
        pager = request.website.pager(
            url=pageUrl,
            total=total_line,
            page=page,
            step=self._post_per_page,
            url_args=post,
        )

        values = {
            'orders': sale_order_list,
            'pager': pager,
            'keep': keep,
            'search': search,
        }
        return request.website.render("website.ordered_goods", values)

    def checkout_values(self, data=None):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry

        res_partner_pool = pool.get('res.partner')
        res_users_pool = pool.get('res.users')
        res_country_pool = pool.get('res.country')
        state_pool = pool.get('res.country.state')

        country_ids = res_country_pool.search(
            cr, SUPERUSER_ID, [], context=context)
        countries = res_country_pool.browse(
            cr, SUPERUSER_ID, country_ids, context)
        states_ids = state_pool.search(cr, SUPERUSER_ID, [], context=context)
        states = state_pool.browse(cr, SUPERUSER_ID, states_ids, context)
        partner = res_users_pool.browse(
            cr, SUPERUSER_ID, request.uid, context).partner_id

        order = request.website.sale_get_order(force_create=1, context=context)

        shipping_id = None
        shipping_ids = []
        user_info = {}
        if not data:
            if request.uid != request.website.user_id.id:
                user_info.update(self.user_info_parse("billing", partner))
                shipping_ids = res_partner_pool.search(
                    cr, SUPERUSER_ID, [
                        ("parent_id", "=", partner.id),
                        ('type', "=", 'delivery')],
                    context=context)
            else:
                order = request.website.sale_get_order(
                    force_create=1, context=context)
                if order.partner_id:
                    user_ids = res_users_pool.search(
                        cr, SUPERUSER_ID,
                        [("partner_id", "=", order.partner_id.id)],
                        context=dict(context or {}, active_test=False))
                    if not user_ids or request.website.user_id.id \
                            not in user_ids:
                        user_info.update(
                            self.user_info_parse("billing", order.partner_id))
        else:
            user_info = self.user_info_parse('billing', data)
            try:
                shipping_id = int(data["shipping_id"])
            except:
                pass
            if shipping_id == -1:
                user_info.update(self.user_info_parse('shipping', data))

        if shipping_id is None:
            if not order:
                sale_order_pool = pool.get('sale.order')
                sale_order_id = request.session.get('sale_order_id')
                order = sale_order_pool.browse(
                    cr, uid, sale_order_id, context=context)
            if order.partner_shipping_id:
                shipping_id = order.partner_shipping_id.id

        ctx = dict(context, show_address=1)
        delivery_carrier_pool = pool.get('delivery.carrier')
        delivery_carrier_ids = delivery_carrier_pool.search(
            cr, uid, [])
        delivery_carriers = delivery_carrier_pool.browse(
            cr, uid, delivery_carrier_ids, context=context)
        user_info['shipping_id'] = shipping_id
        delivery_address_ids = res_partner_pool.search(
            cr, uid, [('parent_id', '=', order.partner_id.id)])
        delivery_address_ids += [shipping_id]
        if order.partner_id.id not in delivery_address_ids:
            delivery_address_ids += [order.partner_id.id]
        delivery_address = res_partner_pool.browse(
            cr, uid, delivery_address_ids, context=context)
        user_info['delivery_address'] = set(delivery_address)
        # Default search by user country
        if not user_info.get('country_id'):
            country_code = request.session['geoip'].get('country_code')
            if country_code:
                country_ids = res_country_pool.search(
                    cr, uid, [('code', '=', country_code)], context=context)
                if country_ids:
                    user_info['country_id'] = country_ids[0]
        values = {
            'countries': countries,
            'states': states,
            'user_info': user_info,
            'shipping_id': partner.id != shipping_id and shipping_id or 0,
            'delivery_carriers': delivery_carriers,
            'error': {},
            'has_check_vat': hasattr(pool['res.partner'], 'check_vat')
        }
        return values

    def user_info_parse(self, address_type, data, remove_prefix=False):
        assert address_type in ('billing', 'shipping')
        if address_type == 'billing':
            all_fields = (self.mandatory_billing_fields +
                          self.optional_billing_fields)
            prefix = ''
        else:
            all_fields = (self.mandatory_shipping_fields +
                          self.optional_shipping_fields)
            prefix = 'shipping_'

        if isinstance(data, dict):
            query = dict((prefix + field_name, data[prefix + field_name])
                         for field_name in all_fields if
                         data.get(prefix + field_name))
        else:
            query = dict((prefix + field_name, getattr(data, field_name))
                         for field_name in all_fields if
                         getattr(data, field_name))
            if address_type == 'billing' and data.parent_id:
                query[prefix + 'street'] = data.parent_id.name

        if query.get(prefix + 'state_id'):
            query[prefix + 'state_id'] = int(query[prefix + 'state_id'])

        if query.get(prefix + 'last_name'):
            query[prefix + 'last_name'] = (query[prefix + 'last_name'])

        if query.get(prefix + 'country_id'):
            query[prefix + 'country_id'] = int(query[prefix + 'country_id'])

        if query.get(prefix + 'vat'):
            query[prefix + 'vat_subjected'] = True

        if not remove_prefix:
            return query

        return dict((field_name, data[prefix + field_name])
                    for field_name in all_fields
                    if data.get(prefix + field_name))

    def get_order_link(self, cr, uid, ids, context=None):
        crm_case_section_pool = request.registry.get('crm.case.section')
        crm_case_section_ids = crm_case_section_pool.search(
            cr, uid, [], context=context)
        crm_case_sections = crm_case_section_pool.browse(
            cr, uid, crm_case_section_ids, context=context)
        user = request.registry.get('res.users').browse(
            cr, uid, uid, context=context)
        order_link = ''
        for crm_case_section in crm_case_sections:
            if user in crm_case_section.member_ids:
                order_link = '/web#id=%s&view_type=form&model=sale.order' % ids
        return order_link

    def checkout_redirection(self, order):
        context = request.context

        # must have a draft sale order with lines at this point, otherwise reset
        if not order or order.state != 'draft':
            request.session['sale_order_id'] = None
            request.session['sale_transaction_id'] = None
            return request.redirect('/shop')

        # if transaction pending / done: redirect to confirmation
        tx = context.get('website_sale_transaction')
        if tx and tx.state != 'draft':
            return request.redirect('/page/confirmation')
        return False

    def checkout_form_validate(self, data):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry

        # Validation
        error = dict()
        for field_name in self.mandatory_billing_fields:
            if not data.get(field_name):
                error[field_name] = 'missing'

        if data.get("vat") and hasattr(pool["res.partner"], "check_vat"):
            if request.website.company_id.vat_check_vies:
                # force full VIES online check
                check_func = pool["res.partner"].vies_vat_check
            else:
                # quick and partial off-line checksum validation
                check_func = pool["res.partner"].simple_vat_check
            vat_country, vat_number = pool["res.partner"]._split_vat(
                data.get("vat"))
            if not check_func(cr, uid, vat_country, vat_number, context=None):
                error["vat"] = 'error'

        if data.get("shipping_id") == -1:
            for field_name in self.mandatory_shipping_fields:
                field_name = 'shipping_' + field_name
                if not data.get(field_name):
                    error[field_name] = 'missing'

        return error

    def checkout_form_save(self, checkout):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry

        sale_order_pool = pool.get('sale.order')
        sale_order_id = request.session.get('sale_order_id')
        order = sale_order_pool.browse(
            cr, uid, sale_order_id, context=context)

        orm_partner = pool.get('res.partner')
        order_obj = request.registry.get('sale.order')

        partner_lang = request.lang if request.lang in [
            lang.code for lang in request.website.language_ids] else None

        billing_info = {}
        if partner_lang:
            billing_info['lang'] = partner_lang
        billing_info.update(self.user_info_parse('billing', checkout, True))

        partner_id = order.partner_id.id
        if partner_id and request.website.partner_id.id != partner_id:
            orm_partner.write(
                cr, SUPERUSER_ID, [partner_id], billing_info, context=context)
        else:
            # create partner
            partner_id = orm_partner.create(
                cr, SUPERUSER_ID, billing_info, context=context)

        # create a new shipping partner
        if checkout.get('shipping_id') == -1:
            shipping_info = {}
            if partner_lang:
                shipping_info['lang'] = partner_lang
            shipping_info.update(self.user_info_parse(
                'shipping', checkout, True))
            shipping_info['type'] = 'delivery'
            shipping_info['parent_id'] = partner_id
            checkout['shipping_id'] = orm_partner.create(
                cr, SUPERUSER_ID, shipping_info, context)

        order_info = {
            'partner_id': partner_id,
            'message_follower_ids': [(4, partner_id),
                                     (3, request.website.partner_id.id)],
            'partner_invoice_id': partner_id,
        }
        order_info.update(order_obj.onchange_partner_id(
            cr, SUPERUSER_ID, [], partner_id, context=context)['value'])
        address_change = order_obj.onchange_delivery_id(
            cr, SUPERUSER_ID, [], order.company_id.id, partner_id,
            checkout.get('shipping_id'), None, context=context)['value']
        order_info.update(address_change)
        if address_change.get('fiscal_position'):
            fiscal_update = order_obj.onchange_fiscal_position(
                cr, SUPERUSER_ID, [], address_change['fiscal_position'],
                [(4, l.id) for l in order.order_line], context=None)['value']
            order_info.update(fiscal_update)

        order_info.pop('user_id')
        order_info.update(
            partner_shipping_id=checkout.get('shipping_id') or partner_id)

        order_obj.write(
            cr, SUPERUSER_ID, [order.id], order_info, context=context)

    @http.route(['/shop/print_pricelist'],
                type='http', auth="user", website=True)
    def print_pricelist(self, **post):

        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        domain = [('sale_ok', '=', True)]
        customer_domain = []
        product_material_pool = pool["product.material"]
        product_template_pool = pool['product.template']
        category_pool = pool['product.public.category']
        res_users_pool = pool['res.users']
        filter_name = post.get('material_name')
        product_material_ids = product_material_pool.search(
            cr, SUPERUSER_ID,
            [('activ', '=', True),
             ('foam', '=', False),
             ('filter_name', '=', filter_name),
             ],
            context=context)
        product_material = product_material_pool.browse(
            cr, SUPERUSER_ID, product_material_ids, context=context)
        if len(product_material) > 1:
            filter_material_id = product_material[0].id
        else:
            filter_material_id = product_material.id

        material = product_material_pool.browse(
            cr, SUPERUSER_ID, filter_material_id, context=context)
        user_domain = [('website_published', '=', 'True')]
        res_user = res_users_pool.browse(
            cr, SUPERUSER_ID, request.uid, context=context)

        owned_search = [('owned_by', '=', 'thermevo')]
        customer_owned_search = [
            ('owned_by', '=', 'Customer'),
            ('custom_product_customer', '=', res_user.partner_id.id)]

        customer_domain += customer_owned_search
        domain += user_domain
        domain += owned_search

        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_set = []
        for i in attrib_list:
            attrib_set.append(str(i))

        product_ids = product_template_pool.search(
            cr, SUPERUSER_ID, domain,
            order='website_published desc, website_sequence desc',
            context=context)
        products = product_template_pool.browse(cr, SUPERUSER_ID, product_ids,
                                                context=context)
        thermevo_product_ids = []
        for product in products:
            if material in product.product_material_ids:
                thermevo_product_ids.append(product.id)
        thermevo_products = product_template_pool.browse(
            cr, SUPERUSER_ID, thermevo_product_ids, context=context)
        customer_product_ids = product_template_pool.search(
            cr, SUPERUSER_ID, customer_domain,
            order='website_published desc, website_sequence desc',
            context=context)
        customer_products = product_template_pool.browse(
            cr, SUPERUSER_ID, customer_product_ids, context=context)
        product_ids = thermevo_product_ids + customer_product_ids
        all_products = thermevo_products + customer_products
        min_max_width = {}
        pub_cat_ids = category_pool.search(
            cr, SUPERUSER_ID, [('width', '=', True)], context=context)
        pub_cat_id = category_pool.browse(
            cr, SUPERUSER_ID, pub_cat_ids, context=context)
        for categs_id in pub_cat_id:
            key = categs_id.id
            if min_max_width.has_key(key):
                min_max_width[key] = categs_id.name
            else:
                min_max_width[key] = categs_id.name

        filtered_products = []
        for p in all_products:
            for fp in p.public_categ_ids:
                if str(fp.id) in attrib_set:
                    if p not in filtered_products:
                        filtered_products.append(p)
        if attrib_set:
            all_products = filtered_products
        cat_name = {}

        for prod in all_products:
            for prod_public_cat in prod.public_categ_ids:
                if prod_public_cat.width:
                    key = prod_public_cat.id
                    if cat_name.has_key(key):
                        cat_name[key] = prod_public_cat.name
                    else:
                        cat_name[key] = prod_public_cat.name
        category_name_sort = sorted(cat_name.items(),
                                    key=lambda cat: float(
                                        cat[1].split()[0]))
        cat_ids = []
        for category in category_name_sort:
            cat_ids.append(category[0])

        print_pricelist_pool = pool["print.pricelist"]
        print_pricelist_last_id = print_pricelist_pool.search(
            cr, uid, [], context=context)
        value = {'product_ids': [(6, 0, product_ids)],
                 'category_ids': [(6, 0, cat_ids)],
                 'filter': post.get('material_name'),
                 }
        if print_pricelist_last_id:
            print_pricelist_last_id = print_pricelist_last_id[-1]
            print_pricelist_last = print_pricelist_pool.browse(
                cr, uid, print_pricelist_last_id, context=context)
            value['image'] = print_pricelist_last.image
        print_pricelist = print_pricelist_pool.create(
            cr, uid, value, context=context)
        datas = []
        pdf = request.registry['report'].get_pdf(
            cr, SUPERUSER_ID, [print_pricelist], 'thermevo_shop.report_product',
            data=datas, context=context)
        pdfhttpheaders = [('Content-Type', 'application/pdf'),
                          ('Content-Length', len(pdf))]
        return request.make_response(pdf, headers=pdfhttpheaders)

    @http.route(['/shop/added_discount'],
                type='json', auth="user", website=True)
    def added_discount(self, discount=None, line_id=None, **post):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        sale_order_line_pool = pool.get('sale.order.line')
        try:
            discount = float(discount)
        except:
            discount = 0.00
        employee_commission_pool = pool.get('employee.commission')
        employee_commission_ids = employee_commission_pool.search(cr, uid, [])
        employee_commissions = employee_commission_pool.browse(
            cr, uid, employee_commission_ids, context=context)
        new_discount = False
        for employee_commission in employee_commissions:
            if (employee_commission.range_of >= discount >=
                    employee_commission.range_on):
                sale_order_line_pool.write(
                    cr, uid, int(line_id),
                    {'discount': discount}, context=context)
                new_discount = True
        if not new_discount:
            discount = sale_order_line_pool.browse(
                cr, uid, int(line_id), context=context).discount
            if not discount:
                discount = 0.00

        order_line = sale_order_line_pool.browse(
            cr, uid, int(line_id), context=context)

        commissions = calculate_commission(order_line.order_id)
        res = {'price_subtotal': round(order_line.price_subtotal, 2),
               'amount_total': round(order_line.order_id.amount_total, 2),
               'amount_tax': round(order_line.order_id.amount_tax, 2),
               'amount_untaxed': round(order_line.order_id.amount_untaxed, 2),
               'commission': round(commissions.get('commission'), 2),
               'percent_commission':
                   ' ({percent_commission}%)'.format(
                       percent_commission=round(
                           commissions.get('percent_commission'), 2)),
               'discount': discount,
               'line_id': '#discount_%s' % order_line.id}
        order_line.order_id.check_opportunity()
        return res


def calculate_commission(sale_order):
    cr = request.cr
    uid = request.uid
    context = request.context
    pool = request.registry
    employee_commission_pool = pool.get('employee.commission')
    employee_commission_ids = employee_commission_pool.search(cr, uid, [])
    employee_commissions = employee_commission_pool.browse(
        cr, uid, employee_commission_ids, context=context)
    commissions = 0
    if sale_order.pricelist_id.default:
        for line in sale_order.order_line:
            if not line.product_id.extruder:
                discount = 0.0
                if line.discount:
                    discount = line.discount
                for employee_commission in employee_commissions:
                    if (employee_commission.range_of >= discount >=
                            employee_commission.range_on):
                        tax_percent = 1
                        for tax in line.tax_id:
                            tax_percent += tax.amount
                        commission = (
                            employee_commission.commission *
                            line.price_subtotal * tax_percent / 100)
                        commissions += commission
                        line.write({'commission': commission})
    try:
        percent_commission = commissions / sale_order.amount_total * 100.0
    except:
        percent_commission = 0
    sale_order.write({'commission': commissions,
                      'percent_commission': percent_commission})
    return {'commission': commissions,
            'percent_commission': percent_commission}


class sale_quote(odoo_website_quote):
    @http.route([
        "/quote/<int:order_id>",
        "/quote/<int:order_id>/<token>"
    ], type='http', auth="public", website=True)
    def view(self, order_id, token=None, message=False, **post):
        # use SUPERUSER_ID allow to access/view order for public user
        # only if he knows the private token
        user = request.registry.get('res.users').browse(
            request.cr, SUPERUSER_ID, request.uid, context=request.context)
        partner = None
        if user.partner_id:
            partner = user.partner_id
        order = request.registry.get('sale.order').browse(
            request.cr, token and SUPERUSER_ID or request.uid, order_id,
            request.context)
        request.session['sale_order_id'] = order_id
        now = time.strftime('%Y-%m-%d')

        if token:
            if token != order.access_token:
                return request.website.render('website.404')
            # Log only once a day
            if request.session.get('view_quote', False) != now:
                request.session['view_quote'] = now
                body = _('Quotation viewed by customer')
                self.__message_post(body, order_id, type='comment')
        days = 0
        if order.validity_date:
            days = (datetime.strptime(order.validity_date,
                                      '%Y-%m-%d') - datetime.now()).days + 1
        values = {
            'quotation': order,
            'message': message and int(message) or False,
            'option': bool(filter(lambda x: not x.line_id, order.options)),
            'order_valid': (not order.validity_date) or (
                now <= order.validity_date),
            'days_valid': days,
            'partner': partner,
        }
        return request.website.render('website_quote.so_quotation', values)

    @http.route(['/quote/accept'], type='json', auth="public", website=True)
    def accept(self, order_id, token=None, signer=None, sign=None, **post):
        order_obj = request.registry.get('sale.order')
        order = order_obj.browse(request.cr, SUPERUSER_ID, order_id)
        if token != order.access_token:
            return request.website.render('website.404')
        order.action_user_confirm()
        attachments = sign and [('signature.png', sign.decode('base64'))] or []
        message = _('Order signed by %s') % (signer,)
        self.__message_post(message, order_id, type='comment',
                            subtype='mt_comment', attachments=attachments)
        return True

    @http.route(['/quote/delete/<int:order_id>'],
                type='http', auth="public", website=True)
    def delete(self, order_id):
        request.registry.get('sale.order').unlink(
            request.cr, SUPERUSER_ID, [order_id])
        return request.redirect('/shop/sale_quotation_list')

    @http.route(['/quote/copy_to_cart/<int:order_id>'],
                type='http', auth="public", website=True)
    def copy_to_cart(self, order_id):
        cr = request.cr
        uid = request.uid
        context = request.context
        pool = request.registry
        order = pool.get('sale.order').browse(
            cr, uid, order_id, context=context)
        for line in order.order_line:
            if line and line.product_id \
                    and not line.product_id.product_tmpl_id.extruder \
                    and not 'Flixo' in line.product_id.name:
                cart_update(cr, uid, pool, (),
                            {'product_id': line.product_id.product_tmpl_id.id,
                             'material_name':
                                 line.product_id.material_id.filter_name},
                            context=context)
        return request.redirect('/shop/sale_quotation_list')


def change_line_option(cr, uid, line_ids, pool, context, new_parameters):
    res = []
    order_line_pool = pool.get('sale.order.line')
    product_material_pool = pool.get('product.material')
    product_product_pool = pool.get('product.product')
    extruder_line_id = ''
    sale_order_pool = pool.get('sale.order')
    sale_order_id = request.session.get('sale_order_id')
    sale_order = sale_order_pool.browse(
        cr, uid, sale_order_id, context=context)
    for line_id in line_ids:

        line = order_line_pool.browse(cr, uid, int(line_id), context=context)
        try:
            amount = line.product_uom_qty
        except:
            pass
        material = ''
        if new_parameters.get('material_id'):
            material_id = new_parameters.get('material_id')
            if line.product_id.owned_by == 'thermevo' \
                    and material_id in \
                            line.product_id.product_tmpl_id.product_material_ids:
                material = product_material_pool.browse(
                    cr, uid, int(new_parameters.get('material_id')),
                    context=context)
            else:
                material = product_material_pool.browse(
                    cr, uid, int(new_parameters.get('material_id')),
                    context=context)
        if not material:
            material = line.product_id.material_id
        option_good = True

        if option_good:
            if new_parameters.get('length'):
                length = float(new_parameters.get('length'))
                lq = math.ceil(line.product_uom_qty / length)
                amount = length * lq
            else:
                length = line.product_id.length
            product_id = product_product_pool.search(
                cr, uid,
                [('product_tmpl_id', '=', line.product_id.product_tmpl_id.id),
                 ('material_id', '=', material.id),
                 ('length', '=', length),
                 ('product_element_height', '=',
                  line.product_id.product_element_height),
                 ('product_element_width', '=',
                  line.product_id.product_element_width),
                 ('product_square', '=', line.product_id.product_square)
                 ],
                context=context)
            product_id = product_product_pool.browse(
                cr, uid, product_id, context=context)
            if not product_id:
                vals = {
                    'profile_density': line.product_id.profile_density,
                    'categ_id': material.categ_id.id,
                    'material_id': material.id,
                    'length': length,
                    'product_tmpl_id': line.product_id.product_tmpl_id.id,
                    'active': True,
                    'product_element_width':
                        line.product_id.product_element_width,
                    'product_element_height':
                        line.product_id.product_element_height,
                    'product_square': line.product_id.product_square,
                }

                if not new_parameters.get('material_id') \
                        and new_parameters.get('length'):
                    vals.update(
                        {'extruder_product_id':
                             line.product_id.extruder_product_id.id})
                new_product_id = product_product_pool.create(
                    cr, uid, vals, context=context)
                post = {}
                product_id = product_product_pool.browse(
                    cr, uid, new_product_id, context=context)
                product_id.create_article()
                product_id.create_second_name()
                if (new_parameters.get('material_id') and
                            line.product_id.product_tmpl_id.owned_by == 'Customer'):
                    extruder_line_id = create_extruder_product(
                        cr, uid, pool, line, new_product_id, context)
            try:
                product_id = product_id[0]
            except:
                pass
            order_line_pool.write(cr, uid, int(line_id),
                                  {'product_id': product_id.id,
                                   'product_uom_qty': amount, },
                                  context=context)
            if line.product_id.product_tmpl_id.owned_by == 'Customer':
                add_extruder_product(
                    cr, uid, pool, line.id,
                    {'final_tooling_price': 0},
                    context)
            line = order_line_pool.browse(cr, uid, int(line_id),
                                          context=context)
            try:
                extruder_line = order_line_pool.browse(
                    cr, uid, extruder_line_id, context=context)
                delete_line(cr, uid, pool, context,
                            extruder_line.order_id.id, extruder_line)
            except:
                pass
            if product_id.rule:
                surcharge = product_id.surcharge
            else:
                surcharge = 0.00

            further_processing_pool = pool.get('further.processing')
            further_processing_ids = further_processing_pool.search(

                cr, uid, [('active', '=', True)])
            further_processing_objects = further_processing_pool.browse(
                cr, uid, further_processing_ids, context=context)
            further_processing = []
            for further in further_processing_objects:
                if new_parameters.get(further.identifier):
                    further_processing.append(further.id)

            option_values = {
                'additional_amount': surcharge,
                'e_low_open_groove': new_parameters.get('e_low_open_groove'),
                'gluewire': new_parameters.get('glue'),
                'color_laser_marking': new_parameters.get(
                    'color_laser_marking'),
                'print_logo': new_parameters.get('print_logo'),
                'logo_for_print': new_parameters.get('logo_for_print'),
                'further_processing_ids': [(6, 0, further_processing)],
            }

            order_line_pool.write(
                cr, SUPERUSER_ID, line.id, option_values, context=context)
            res.append(line.id)
    lines = sale_order.order_line
    for line in lines:
        if line.product_id.extruder:
            delete_line(cr, uid, pool, context, sale_order.id, line)
    return res


def dublicate_lines(cr, uid, pool, context, post):
    res = []
    order_line_pool = pool.get('sale.order.line')
    product_category_ids = pool.get('product.category').search(
        cr, SUPERUSER_ID,
        [('name', 'in', ('Wooden Box', 'Steel Box', 'Service Units'))])
    for line_id in post.get('products'):
        order_line = order_line_pool.browse(cr, uid, int(line_id),
                                            context=context)
        if not order_line.product_id.extruder \
                and order_line.product_id.categ_id.id not in product_category_ids:
            line_id = order_line_pool.copy(
                cr, uid, order_line.id, context=context)
            res.append(int(line_id))
    return res


def line_change_function(cr, uid, pool, context, post):
    res = {}
    sale_order_line_pool = pool.get('sale.order.line')

    sale_order_line = sale_order_line_pool.browse(
        cr, uid, int(post.get('line_id')), context=context)
    quantity = 0
    if post.get('quantity'):
        quantity = float(post.get('quantity').replace(',', '.'))
    if not quantity or quantity == 0:
        order_id = sale_order_line.order_id.id

        if sale_order_line.product_id.material_id:
            res['product_tmpl_id'] = sale_order_line.product_id.product_tmpl_id
            res['material_id'] = sale_order_line.product_id.material_id

        delete_line(cr, uid, pool, context, order_id, sale_order_line)

    elif quantity and quantity > 0:
        new_quantity = float(post.get('quantity'))
        if not sale_order_line.product_id.extruder:
            new_quantity = sale_order_line.check_qty(new_quantity)
            try:
                new_quantity = new_quantity[0]
            except:
                pass
        sale_order_line_pool.write(cr, uid, sale_order_line.id,
                                   {'product_uom_qty': new_quantity})
        res['line_ids'] = sale_order_line.id
    return res


def delete_line(cr, uid, pool, context, order_id, sale_order_line):
    sale_order_line_pool = pool.get('sale.order.line')
    res = {}
    if not sale_order_line.product_id.extruder:
        extruder_product_id = sale_order_line.product_id.extruder_product_id
        product_id = sale_order_line.product_id
        sale_order_line_pool.unlink(
            cr, uid, [sale_order_line.id], context=context)
        try:
            if product_id.product_tmpl_id.owned_by == "Customer":
                clean_database(cr, uid, pool, [product_id.product_tmpl_id.id],
                               context=context)
        except:
            pass
    else:
        extruder_product_id = sale_order_line.product_id
    if extruder_product_id:
        sale_order_line_ids = sale_order_line_pool.search(
            cr, uid, [('order_id', '=', order_id)])
        sale_order_lines = sale_order_line_pool.browse(
            cr, uid, sale_order_line_ids, context=context)
        can_delete = True
        for order_line in sale_order_lines:
            if order_line.product_id.extruder_product_id == extruder_product_id \
                    and order_line != sale_order_line:
                can_delete = False
        if can_delete:
            for order_line in sale_order_lines:
                if order_line.product_id == extruder_product_id:
                    sale_order_line_pool.unlink(
                        cr, uid, [int(order_line.id)], context=context)
    return res


def clean_database(cr, uid, pool, product_template_ids, context=None):
    product_template_pool = pool.get('product.template')
    product_product_pool = pool.get('product.product')
    for product_template_id in product_template_ids:
        try:
            product_product_ids = product_product_pool.search(
                cr, uid, [('product_tmpl_id', '=', product_template_id)])
        except:
            pass

        if product_product_ids:
            if len(product_product_ids) == 1:
                try:
                    product_product_id = product_product_ids[0]
                except:
                    product_product_id = product_product_ids

                product = product_product_pool.browse(
                    cr, uid, product_product_id, context=context)
                if product.product_tmpl_id.owned_by == 'Customer':
                    cr.execute(
                        """
                          SELECT id
                          FROM sale_order_line
                          WHERE product_id = %s
                        """ % product_product_id)
                    sale_order_line_ids = cr.fetchall()
                    cr.execute(
                        """
                          SELECT id
                          FROM purchase_order_line
                          WHERE product_id = %s
                        """ % product_product_id)
                    purchase_order_line_ids = cr.fetchall()
                    cr.execute(
                        """
                          SELECT id
                          FROM procurement_order
                          WHERE product_id = %s
                        """ % product_product_id)
                    procurement_order_ids = cr.fetchall()
                    if not sale_order_line_ids \
                            and not purchase_order_line_ids \
                            and not procurement_order_ids:
                        try:
                            product_template_pool.unlink(
                                cr, SUPERUSER_ID, [product_template_id],
                                context=context)
                        except:
                            pass
            else:
                cr.execute(
                    """
                      SELECT id
                      FROM sale_order_line
                      WHERE product_id IN %s
                    """ % (tuple(product_product_ids),))
                sale_order_line_ids = cr.fetchall()
                cr.execute(
                    """
                      SELECT id
                      FROM purchase_order_line
                      WHERE product_id IN %s
                    """ % (tuple(product_product_ids),))
                purchase_order_line_ids = cr.fetchall()
                cr.execute(
                    """
                      SELECT id
                      FROM procurement_order
                      WHERE product_id IN %s
                    """ % (tuple(product_product_ids),))
                procurement_order_ids = cr.fetchall()
                if not sale_order_line_ids \
                        and not purchase_order_line_ids \
                        and not procurement_order_ids:
                    try:
                        product_template = product_template_pool.browse(
                            cr, uid, product_template_id, context=context)
                        if product_template.owned_by == 'Customer':
                            product_template_pool.unlink(
                                cr, uid, [product_template_id], context=context)
                    except:
                        pass
                else:
                    for product_product_id in product_product_ids:
                        product = product_product_pool.browse(
                            cr, uid, product_product_id, context=context)
                        if product.product_tmpl_id.owned_by == 'Customer':
                            cr.execute(
                                """
                                  SELECT id
                                  FROM sale_order_line
                                  WHERE product_id = %s
                                """ % product_product_id)
                            sale_order_line_ids = cr.fetchall()
                            cr.execute(
                                """
                                  SELECT id
                                  FROM purchase_order_line
                                  WHERE product_id = %s
                                """ % product_product_id)
                            purchase_order_line_ids = cr.fetchall()
                            cr.execute(
                                """
                                  SELECT id
                                  FROM procurement_order
                                  WHERE product_id = %s
                                """ % product_product_id)
                            procurement_order_ids = cr.fetchall()
                            if not sale_order_line_ids \
                                    and not purchase_order_line_ids \
                                    and not procurement_order_ids:
                                try:
                                    product_product_pool.unlink(
                                        cr, uid, [product_product_id],
                                        context=context)
                                except:
                                    pass


def delete_lines(cr, uid, pool, context, post):
    res = []
    order_line_pool = pool.get('sale.order.line')
    for line_id in post.get('products'):
        order_line = order_line_pool.browse(
            cr, uid, int(line_id), context=context)

        try:
            if order_line.product_id.material_id:
                res.append({
                    'product_tmpl_id': order_line.product_id.product_tmpl_id,
                    'material_id': order_line.product_id.material_id,
                })
            delete_line(cr, uid, pool, context, order_line.order_id.id,
                        order_line)
        except:
            pass
    return res


def header_cart_html(**post):
    header_cart_number = '<a href="#" class="cart-link">' \
                         '  <span style="padding-left:10px;">%s</span>' \
                         '</a>' % len(
        request.website.sale_get_order().order_line)
    return header_cart_number


def shopping_cart_html(sale_order, sale_order_lines):
    header_cart_number = u'<a href="#" class="cart-link">' \
                         u'     <span style="padding-left:10px;">' \
                         u'         %s' \
                         u'     </span>' \
                         u'</a>' % len(sale_order_lines)

    header_cart_item_template = u'<div class="cart_item">' \
                                u'    <div class="cart_img">' \
                                u'    </div>' \
                                u'    <div class="cart_link">' \
                                u'        <a href="#">' \
                                u'            <strong>%s,</strong> ' \
                                u'            %s</a>' \
                                u'    </div>' \
                                u'</div>'
    header_option_template = u'Article № %s'

    header_cart_template = u'<div class="header_cart" style="display: block;">' \
                           u'    %s' \
                           u'    <div class="header_cart_close"/>' \
                           u'</div>'

    shopping_cart = u'<div class="shopping_cart">' \
                    u'   <form action="/shop/delivery_address" method="POST">' \
                    u'       <div class="select_all_wrap">' \
                    u'           <input id="select_all" type="checkbox" name="select_all"/>' \
                    u'           <label for="select_all">Insulating Strips THERMEVO EVOBREAK</label>' \
                    u'       </div>' \
                    u'       %s' \
                    u'       <div class="total" id="total">' \
                    u'           Total cost, €: ' \
                    u'           <span>%s</span>' \
                    u'       </div>' \
                    u'       <div class="tax" id="tax">' \
                    u'           Taxes, €: ' \
                    u'           <span>%s</span>' \
                    u'       </div>' \
                    u'       <div class="total_cost" id="total_cost">' \
                    u'           Total Cost incl. Taxes, €: ' \
                    u'           <span>%s</span>' \
                    u'       </div>' \
                    u'       %s' \
                    u'       <div class="form-action">' \
                    u'          <button type="submit" class="red-btn-link">' \
                    u'               Add a delivery adress' \
                    u'           </button>' \
                    u'       </div>' \
                    u'   </form>' \
                    u'</div>'
    commission_block = u'<div class="tax" id="commission" t-if="saleperson">' \
                       u'    1.Year BC+TC, €: <span>%s </span>' \
                       u'</div>'

    customer_option_template = u'           <p>' \
                               u'               <strong>Article №</strong> %s' \
                               u'           </p> ' \
                               u'           <p>' \
                               u'               <strong> Option: </strong> %s' \
                               u'           </p> ' \
                               u'           <p>' \
                               u'               <strong> Q-ty in pallet: </strong>%s Pcs x %s m = %s m' \
                               u'           </p> ' \
                               u'           <p>' \
                               u'               <strong> Minimum order Q-ty: </strong> %s m' \
                               u'           </p> ' \
                               u'           <p> ' \
                               u'               Width: <span type="width" class="number editble" contenteditable="true">%s</span> mm, ' \
                               u'               height: <span type="height" class="number editble" contenteditable="true">%s</span> mm,' \
                               u'               area: <span type="area" class="number editble" contenteditable="true">%s</span> mm2' \
                               u'           </p> ' \

    thermevo_option_template = u'           <p>' \
                               u'               <strong>Article №</strong> %s' \
                               u'           </p> ' \
                               u'           <p>' \
                               u'               <strong> Option: </strong> %s' \
                               u'           </p> ' \
                               u'           <p>' \
                               u'               <strong> Q-ty in pallet: </strong>%s Pcs x %s m = %s m' \
                               u'           </p> ' \
                               u'           <p>' \
                               u'               <strong> Minimum order Q-ty: </strong> %s m' \
                               u'           </p> ' \
                               u'           <p> ' \
                               u'               Width: <span type="width">%s mm</span>, ' \
                               u'               height: <span type="height">%s mm</span>,' \
                               u'               area: <span type="area">%s mm2</span> ' \
                               u'           </p> ' \

    customer_title = u'<span type="name" class="editble title" contenteditable="true" >%s</span>'
    thermevo_title = u'<span type="name" class="title editble" style="color:black;border:none">%s</span>'

    block_checkbox_template = u'       <input id="select_line_%s" type="checkbox" name="select_%s" value="%s"/>' \
                              u'       <label for="select_line_%s"/>'

    extruder_block_checkbox_template = u' '

    product_item_template = u'<div class="product_item_wrap">' \
                            u'   <div class="product_item">' \
                            u'       <input type="hidden" name="order_line_%s" value="%s" class="order_line"/>' \
                            u'       <input type="hidden" name="product_%s" value="%s" class="product_order_line"/>' \
                            u'       <div class="select_product">' \
                            u'           %s' \
                            u'       </div>' \
                            u'       <div class="image">' \
                            u'           <img src="data:image/png;base64, %s" alt="ttt"/>' \
                            u'       </div>' \
                            u'       <div class="text">' \
                            u'           <p>' \
                            u'               %s' \
                            u'           </p>' \
                            u'           <p style="font-size:11px;line-height: 14px;">%s</p>' \
                            u'       </div>' \
                            u'   </div>' \
                            u'   <div class="product_quantity">' \
                            u'       %s' \
                            u'       %s' \
                            u'   </div>' \
                            u'</div>'

    product_amount_template = u'   <div class="left_cost">' \
                              u'       <div class="annual ajax_count">' \
                              u'           <span>Annual purchase, meters:</span>' \
                              u'           <input type="text" name="annual_product_%s" value="%s" disabled="disabled"/>' \
                              u'           <a href="#" class="save_product">Save</a>' \
                              u'           <a href="#" class="delete_product">Delete</a>' \
                              u'       </div>' \
                              u'       <div class="quantity ajax_count">' \
                              u'           <span>Quantity, meters:</span>' \
                              u'           <input type="text" class="input_quantity" name="%s" value="%s"/>' \
                              u'           <a href="#" class="save_product">Save</a>' \
                              u'           <a href="#" class="delete_product">Delete</a>' \
                              u'       </div>' \
                              u'   </div>'

    cost_template = u'   <div class="right_cost">' \
                    u'       <div class="unit">' \
                    u'           Unit price, €:' \
                    u'              <span>%s</span>' \
                    u'       </div>' \
                    u'       <div class="cost">' \
                    u'           Subtotal, €:' \
                    u'           <span>%s</span>' \
                    u'       </div>' \
                    u'   </div>'

    saleperson_cost_template = u'    <div class="right_cost">' \
                               u'        <div class="unit hsp">' \
                               u'            Unit price, €:' \
                               u'            <span>%s</span>' \
                               u'            <p>' \
                               u'                Discount, : ' \
                               u'                <span>' \
                               u'                    <input type="text" class="discount" value="%s"/>' \
                               u'                </span>' \
                               u'            </p>' \
                               u'       </div>' \
                               u'       <div class="cost">' \
                               u'           Subtotal, €:' \
                               u'           <span>%s</span>' \
                               u'       </div>' \
                               u'   </div>'
    cr = request.cr
    uid = request.uid
    context = request.context
    pool = request.registry

    product_category_ids = pool.get('product.category').search(
        cr, SUPERUSER_ID,
        [('name', 'in', ('Wooden Box', 'Steel Box', 'Service Units'))])

    res_users_pool = pool.get('res.users')
    res_user = res_users_pool.browse(cr, uid, uid, context=context)

    crm_case_section_pool = request.registry.get('crm.case.section')
    crm_case_section_ids = crm_case_section_pool.search(
        cr, uid, [], context=context)
    crm_case_sections = crm_case_section_pool.browse(
        cr, uid, crm_case_section_ids, context=context)

    user_crm_case_section = []
    for crm_case_section in crm_case_sections:
        if res_user in crm_case_section.member_ids:
            user_crm_case_section.append(crm_case_section.id)

    saleperson = False
    res_partner_pool = pool.get('res.partner')
    per_res_partner_ids = res_partner_pool.search(
        cr, uid, [('user_id', '=', uid)],
        context=context)
    team_res_partner_ids = res_partner_pool.search(
        cr, uid, [('section_id', 'in', user_crm_case_section)],
        context=context)
    res_partner_ids = per_res_partner_ids + team_res_partner_ids
    res_partners = res_partner_pool.browse(
        cr, uid, res_partner_ids, context=context)
    if user_crm_case_section:
        saleperson = True

    product_items = ''
    header_product_items = ''
    for line in sale_order_lines:
        try:
            item_info_header = u''
            if line.product_id.extruder:
                item_info = ''
                for l in sale_order_lines:
                    try:
                        if l.product_id.extruder_product_id.id == line.product_id.id:
                            item_info += str(l.short_name) + u'</br>'
                    except:
                        item_info += u'</br>'
                title = thermevo_title % line.product_id.second_name
            elif line.product_id.categ_id.id in product_category_ids:
                item_info = u''
                title = thermevo_title % line.name
            else:

                if line.product_id.owned_by == 'Customer':
                    item_info = customer_option_template % (
                        str(line.line_article),
                        str(line.website_description_sale_line),
                        str(line.product_id.qty_in_pallet),
                        str(line.product_id.length),
                        str(line.product_id.qty_in_pallet * line.product_id.length),
                        str(line.minimum_qty),
                        str(line.product_id.product_element_width),
                        str(line.product_id.product_element_height),
                        str(line.product_id.product_square)
                    )
                    title = customer_title % line.short_name

                else:
                    item_info = thermevo_option_template % (
                        str(line.line_article),
                        str(line.website_description_sale_line),
                        str(line.product_id.qty_in_pallet),
                        str(line.product_id.length),
                        str(line.product_id.qty_in_pallet * line.product_id.length),
                        str(line.minimum_qty),
                        str(line.product_id.product_element_width),
                        str(line.product_id.product_element_height),
                        str(line.product_id.product_square)
                    )
                    if not line.short_name:
                        title = thermevo_title % str(line.product_id.name)
                    else:
                        title = thermevo_title % str(line.short_name)

                item_info_header = \
                    header_option_template % str(line.line_article)
            product_amount = ''
            if not line.product_id.extruder:
                product_amount = product_amount_template % (
                    str(line.product_id.id),
                    str(line.product_uom_qty),
                    str(line.product_id.id), str(line.product_uom_qty),
                )

            default_pricelist = True
            product_pricelist_pool = pool.get('product.pricelist')
            product_pricelist_id = product_pricelist_pool.search(
                cr, uid, [('default', '=', True)])
            try:
                product_pricelist_id = product_pricelist_id[0]
            except:
                pass
            if sale_order.pricelist_id.id != product_pricelist_id:
                default_pricelist = False

            if not saleperson:
                cost = cost_template % (str(line.price_unit),
                                        str(line.price_subtotal))
            else:
                if line.product_id.extruder \
                        or line.product_id.categ_id.id in product_category_ids:
                    cost = cost_template % (str(line.price_unit),
                                            str(line.price_subtotal))
                else:
                    if default_pricelist:
                        cost = saleperson_cost_template % (
                            str(line.price_unit),
                            str(line.discount),
                            str(line.price_subtotal))
                    else:
                        cost = cost_template % (str(line.price_unit),
                                                str(line.price_subtotal))

            block_checkbox = block_checkbox_template % (
                str(line.id), str(line.id), str(line.id),
                str(line.id),)

            if line.product_id.extruder \
                    or line.product_id.categ_id.id in product_category_ids:
                block_checkbox = extruder_block_checkbox_template

            product_item = product_item_template % (
                str(line.id), str(line.id),
                str(line.id), str(line.id),
                block_checkbox,
                str(line.product_image),
                str(title),
                item_info,
                product_amount,
                cost
            )
            header_product_item = header_cart_item_template % (
                line.product_id.name,
                item_info_header
            )
            header_product_items += header_product_item
            product_items += product_item
        except:
            item_info = u'</br>'
    if saleperson:
        commission_block = commission_block % (
            str(sale_order.commission))
    else:
        commission_block = ''
    shopping_cart = shopping_cart % (
        product_items,
        sale_order.amount_untaxed,
        sale_order.amount_tax,
        sale_order.amount_total,
        commission_block,
    )
    header_cart = header_cart_template % header_product_items
    result = header_cart_number + header_cart + shopping_cart

    if not sale_order_lines:
        result = {'empty_cart': True}

    return result


def create_extruder_product(cr, uid, pool, line, product_id, context):
    order_line_pool = pool.get('sale.order.line')
    product_material_pool = pool.get('product.material')
    product_product_pool = pool.get('product.product')
    new_product = product_product_pool.browse(
        cr, uid, product_id, context=context)
    material_group = new_product.material_id.product_extruder_group_id
    materials = product_material_pool.search(cr, uid, [
        ('product_extruder_group_id', '=', material_group.id)],
                                             context=context)
    product_product_ids = product_product_pool.search(
        cr, SUPERUSER_ID, [
            ('product_tmpl_id', '=', new_product.product_tmpl_id.id),
            ('material_id', 'in', materials),
            ('product_element_width', '=', new_product.product_element_width),
            ('product_element_height', '=', new_product.product_element_height),
            ('product_square', '=', new_product.product_square)],
        context=context)
    extruder_line_id = ''
    extruder_added = False
    if product_product_ids:
        order_line_ids = order_line_pool.search(
            cr, uid, [
                ('order_id', '=', line.order_id.id)],
            context=context)
        order_lines = order_line_pool.browse(
            cr, uid, order_line_ids, context=context)
        for order_line in order_lines:
            if order_line.product_id.id in product_product_ids \
                    and order_line.id != line.id \
                    and not extruder_added:
                product_product_pool.write(
                    cr, SUPERUSER_ID, product_id,
                    {'extruder_product_id':
                         order_line.product_id.extruder_product_id.id},
                    context=context)
                extruder_added = True
    if not product_product_ids or not extruder_added:
        extruder_vals = {
            'product_tmpl_id':
                line.product_id.product_tmpl_id.extruder_template_id.id,
            'second_name': '%s for <%s>' % (
                line.product_id.product_tmpl_id.extruder_template_id.name,
                line.product_id.second_name)
        }
        extruder_product_id = product_product_pool.create(
            cr, uid, extruder_vals, context=context)

        product_product_pool.write(
            cr, uid, product_id,
            {'extruder_product_id': extruder_product_id},
            context=context)
        old_extruder_id = line.product_id.extruder_product_id
        extruder_line_id = order_line_pool.search(
            cr, uid, [
                ('product_id', '=', old_extruder_id.id),
                ('order_id', '=', line.order_id.id)],
            context=context)

    return extruder_line_id


def add_extruder_product(cr, uid, pool, line_id, google_answer, context):
    res = {}
    sale_order_line_pool = pool.get('sale.order.line')
    product_product_pool = pool.get('product.product')
    order_line = sale_order_line_pool.browse(cr, SUPERUSER_ID, line_id,
                                             context=context)
    sale_order = order_line.order_id
    sale_order_line_ids = sale_order_line_pool.search(
        cr, SUPERUSER_ID, [('order_id', '=', sale_order.id)], context=context)
    sale_order_lines = sale_order_line_pool.browse(
        cr, SUPERUSER_ID, sale_order_line_ids, context=context)
    have_extruder = False
    for line in sale_order_lines:

        if line != order_line \
                and (line.product_id.extruder_product_id ==
                         order_line.product_id):
            have_extruder = True
    if not have_extruder and not order_line.product_id.sell:
        if order_line.product_id.extruder_product_id.id:
            product_product_pool.write(
                cr, SUPERUSER_ID, order_line.product_id.extruder_product_id.id,
                {'variant_price': google_answer.get('total_tooling_price')},
                context=context)
        else:
            extruder_product_id = product_product_pool.search(
                cr, uid,
                [('product_tmpl_id', '=',
                  order_line.product_id.extruder_template_id.id)], limit=1,
                context=context)
            if extruder_product_id:
                extruder_product = product_product_pool.browse(
                    cr, uid, extruder_product_id, context=context)
                new_extruder_product = extruder_product.copy()
                product_product_pool.write(
                    cr, SUPERUSER_ID, order_line.product_id.id,
                    {'extruder_product_id': new_extruder_product.id},
                    context=context)
            else:
                temp_product = product_product_pool.search(
                    cr, SUPERUSER_ID, [('extruder_product_id', '!=', False)],
                    limit=1, context=context)
                new_extruder_product = temp_product.copy()
                product_product_pool.write(
                    cr, SUPERUSER_ID, order_line.product_id.id,
                    {'extruder_product_id': new_extruder_product.id},
                    context=context)

            product_product_pool.write(
                cr, SUPERUSER_ID, order_line.product_id.extruder_product_id.id,
                {'variant_price': google_answer.get('total_tooling_price')},
                context=context)
        result = request.website.sale_get_order(force_create=1)._cart_update(
            product_id=int(order_line.product_id.extruder_product_id.id),
            add_qty=1, set_qty=1)
        return result


def check(sale_order_line, order_line):
    res = False
    if sale_order_line and order_line:
        if sale_order_line.product_id.product_tmpl_id.id == order_line.product_id.product_tmpl_id.id \
                and sale_order_line.product_id.material_id.id == order_line.product_id.material_id.id \
                and sale_order_line.product_id.product_element_width == order_line.product_id.product_element_width \
                and sale_order_line.product_id.product_element_height == order_line.product_id.product_element_height \
                and sale_order_line.product_id.product_square == order_line.product_id.product_square \
                and sale_order_line.gluewire == order_line.gluewire \
                and sale_order_line.color_laser_marking == order_line.color_laser_marking:
            res = True
            for further_processing in sale_order_line.further_processing_ids:
                if further_processing not in order_line.further_processing_ids:
                    res = False
    return res


def calculate_new_price(cr, uid, pool, res, context):
    sale_order_line_pool = pool.get('sale.order.line')
    change_type = res.get('type')
    product_category_ids = pool.get('product.category').search(
        cr, SUPERUSER_ID, [('name', 'in', ('Wooden Box', 'Steel Box', 'Service Units'))])
    line_ids = []
    for order_line in sale_order_line_pool.browse(
            cr, uid, res.get('line_ids'), context=context):
        if order_line.product_id.categ_id.id not in product_category_ids:
            if not order_line.product_id.extruder:
                if order_line.product_id.type != 'service':
                    line_ids.append(order_line.id)

    result = []
    if change_type == 'add':
        for line_id in line_ids:
            order_line = sale_order_line_pool.browse(cr, uid, int(line_id),
                                                     context=context)
            sale_order_line_ids = sale_order_line_pool.search(cr, uid, [
                ('order_id', '=', order_line.order_id.id)], context=context)
            sale_order_lines = sale_order_line_pool.browse(cr, uid,
                                                           sale_order_line_ids,
                                                           context=context)
            change_line = []
            total_amount = 0
            for sale_order_line in sale_order_lines:
                if sale_order_line.product_id.product_tmpl_id == order_line.product_id.product_tmpl_id \
                        and sale_order_line.product_id.material_id == order_line.product_id.material_id \
                        and sale_order_line.product_id.product_element_width == order_line.product_id.product_element_width \
                        and sale_order_line.product_id.product_element_height == order_line.product_id.product_element_height \
                        and sale_order_line.product_id.product_square == order_line.product_id.product_square:
                    further = True
                    for further_processing in sale_order_line.further_processing_ids:
                        if further_processing not in order_line.further_processing_ids:
                            further = False
                    if further:
                        change_line.append(sale_order_line)
                        total_amount += sale_order_line.product_uom_qty
            result = [change_line]
    elif change_type == 'line_change' and res.get('line_ids'):
        for line_id in line_ids:
            order_line = sale_order_line_pool.browse(cr, uid, int(line_id),
                                                     context=context)
            sale_order_line_ids = sale_order_line_pool.search(cr, uid, [
                ('order_id', '=', order_line.order_id.id)], context=context)
            sale_order_lines = sale_order_line_pool.browse(
                cr, uid, sale_order_line_ids, context=context)
            change_line = []
            for sale_order_line in sale_order_lines:

                if check(sale_order_line, order_line):
                    change_line.append(sale_order_line)
            result = [change_line]
    elif change_type == 'line_change' and not line_ids:
        change_type = 'delete'
    elif change_type == 'delete':

        sale_order_pool = pool.get('sale.order')
        sale_order_id = request.session.get('sale_order_id')
        sale_order = sale_order_pool.browse(
            cr, uid, sale_order_id, context=context)
        sale_order_lines = sale_order.order_line
        lines = []
        change_lines = []
        for sale_order_line in sale_order_lines:
            if not sale_order_line.product_id.extruder:
                change_lines.append(sale_order_line)

        for need_line in change_lines:
            change_line = []
            for chan_line in change_lines:
                if check(chan_line, need_line):
                    change_line.append(chan_line)
            lines.append(sorted(change_line))
        for line in lines:
            if line not in result:
                result.append(line)
    elif change_type == 'dublicate':
        check_order_lines = sale_order_line_pool.browse(
            cr, uid, line_ids, context=context)
        lines = []
        sale_order_pool = pool.get('sale.order')
        sale_order_id = request.session.get('sale_order_id')
        sale_order = sale_order_pool.browse(
            cr, uid, sale_order_id, context=context)
        sale_order_lines = sale_order.order_line

        for need_line in check_order_lines:
            change_line = []
            for chan_line in sale_order_lines:
                if check(chan_line, need_line):
                    change_line.append(chan_line)
            lines.append(sorted(change_line))
        for line in lines:
            if line not in result:
                result.append(line)
    elif change_type == 'option':
        check_order_lines = sale_order_line_pool.browse(
            cr, uid, line_ids, context=context)
        lines = []

        sale_order_pool = pool.get('sale.order')
        sale_order_id = request.session.get('sale_order_id')
        sale_order = sale_order_pool.browse(
            cr, uid, sale_order_id, context=context)
        sale_order_lines = sale_order.order_line

        for need_line in check_order_lines:
            change_line = []
            for chan_line in sale_order_lines:
                if check(chan_line, need_line):
                    change_line.append(chan_line)
            lines.append(sorted(change_line))
        for line in lines:
            if line not in result:
                result.append(line)
    for res in result:
        all_qty = 0
        for sale_order_line in res:
            if sale_order_line:
                all_qty += sale_order_line.product_uom_qty
        if res and sale_order_line and sale_order_line.product_id:
            google_values = {
                'registry': pool,
                'product_product_objects': sale_order_line.product_id,
                'have_glue': False,
                'length': sale_order_line.product_id.length,
                'qty': all_qty,
                'sale_order_name': sale_order_line.order_id.name,
                'sale_order_line': sale_order_line,
            }
            product_category_ids = pool.get('product.category').search(
                cr, SUPERUSER_ID, [('name', 'in', ('Wooden Box', 'Steel Box', 'Service Units'))])
            if not sale_order_line.product_id.extruder \
                    and sale_order_line.product_id.type != 'service' \
                    and sale_order_line.product_id.categ_id.id not in product_category_ids:
                google_answer = google_sheet_dublicate(
                    cr, SUPERUSER_ID, google_values, context=context)
                for sale_order_line in res:
                    res = request.website.sale_get_order(
                        force_create=1)._cart_update(
                        product_id=int(sale_order_line.product_id.id),
                        line_id=int(sale_order_line.id),
                        add_qty=float(sale_order_line.product_uom_qty),
                        set_qty=float(sale_order_line.product_uom_qty))
                    sale_order_line.write({'minimum_qty':
                                               google_answer.get('minimum_qty')})
                check_extruder_order_line_ids = sale_order_line_pool.search(
                    cr, SUPERUSER_ID,
                    [('order_id', '=', sale_order_line.order_id.id)])
                check_extruder_order_lines = sale_order_line_pool.browse(
                    cr, SUPERUSER_ID, check_extruder_order_line_ids,
                    context=context)
                for extruder_order_line in check_extruder_order_lines:
                    try:
                        if extruder_order_line.product_id \
                                == sale_order_line.product_id.extruder_product_id:
                            line = request.website.sale_get_order(
                                force_create=1)._cart_update(
                                product_id=int(extruder_order_line.product_id.id),
                                line_id=int(extruder_order_line.id),
                                add_qty=float(extruder_order_line.product_uom_qty),
                                set_qty=float(extruder_order_line.product_uom_qty))
                            sale_order_line_pool.write(
                                cr, SUPERUSER_ID, int(line.get('line_id')),
                                {'price_unit':
                                     google_answer.get('total_tooling_price')})
                    except:
                        pass

    return res


class TableCompute(object):
    def __init__(self):
        self.table = {}

    def _check_place(self, pos_x, pos_y, size_x, size_y):
        res = True
        for y in range(size_y):
            for x in range(size_x):
                if pos_x + x >= product_per_row:
                    res = False
                    break
                row = self.table.setdefault(pos_y + y, {})
                if row.setdefault(pos_x + x) is not None:
                    res = False
                    break
            for x in range(product_per_row):
                self.table[pos_y + y].setdefault(x, None)
        return res

    def process(self, products):
        # Compute products positions on the grid
        min_pos = 0
        index = 0
        max_y = 0
        for p in products:
            x = min(max(p.website_size_x, 1), product_per_row)
            y = min(max(p.website_size_y, 1), product_per_row)
            if index >= product_per_page:
                x = y = 1

            pos = min_pos
            while not self._check_place(pos % product_per_row,
                                        pos / product_per_row, x, y):
                pos += 1
            if index >= product_per_page and (
                        (pos + 1.0) / product_per_row) > max_y:
                break

            if x == 1 and y == 1:  # simple heuristic for CPU optimization
                min_pos = pos / product_per_row

            for y2 in range(y):
                for x2 in range(x):
                    self.table[(pos / product_per_row) + y2][
                        (pos % product_per_row) + x2] = False
            self.table[pos / product_per_row][pos % product_per_row] = {
                'product': p, 'x': x, 'y': y,
                'class': " ".join(
                    map(lambda x: x.html_class or '', p.website_style_ids))
            }
            if index <= product_per_page:
                max_y = max(max_y, y + (pos / product_per_row))
            index += 1

        # Format table according to HTML needs
        rows = self.table.items()
        rows.sort()
        rows = map(lambda x: x[1], rows)
        for col in range(len(rows)):
            cols = rows[col].items()
            cols.sort()
            x += len(cols)
            rows[col] = [c for c in map(lambda x: x[1], cols) if c]

        return rows


class QueryURL(object):
    def __init__(self, path='', **args):
        self.path = path
        self.args = args

    def __call__(self, path=None, **kw):
        if not path:
            path = self.path
        for k, v in self.args.items():
            kw.setdefault(k, v)
        l = []
        for k, v in kw.items():
            if v:
                if isinstance(v, list) or isinstance(v, set):
                    l.append(werkzeug.url_encode([(k, i) for i in v]))
                else:
                    l.append(werkzeug.url_encode([(k, v)]))
        if l:
            path += '?' + '&'.join(l)
        return path


def send_shop_test_message(values):
    cr = request.cr
    uid = request.uid
    context = request.context
    pool = request.registry
    res_users_pool = pool.get('res.users')
    test_manager_ids = res_users_pool.search(
        cr, uid, [('test_manager', '=', True)], context=context)
    test_managers = res_users_pool.browse(
        cr, uid, test_manager_ids, context=context)
    user = res_users_pool.browse(cr, uid, uid, context=context)
    if not user.test_user:
        mail_message_pool = pool.get('mail.message')
        for manager in test_managers:
            reply_to = email = manager.partner_id.email
            title = 'Shop'
            message_text = ''
            user_link = '<a href="%s/web#id=%s&' \
                        'view_type=form&model=res.users&action=76">%s</a>' % \
                        (user.partner_id.company_id.website, user.id, user.name)

            if values.get('subscribe_type') == 'first_step_cart':
                message_text = 'User %s has created new shopping ' \
                               'cart with ids: %s' % (
                                   user_link, values.get('sale'))

            elif values.get('subscribe_type') == 'price_q':
                message_text = 'User %s has created new shopping ' \
                               'PQ’s with id:' % (user_link,
                                                  values.get('sale'))
            try:
                mail_message_pool.create(cr, SUPERUSER_ID, {
                    'model': 'res.users',
                    'res_id': manager.id,
                    'record_name': title,
                    'email_from': email,
                    'reply_to': reply_to,
                    'subject': title,
                    'body': message_text,
                    'type': 'email',
                }, context=context)
            except:
                pass

        return True
    else:
        return False


def cart_update(cr, uid, pool, kw, post, context=None):
    product_product_pool = pool.get('product.product')
    product_template_pool = pool.get('product.template')
    sale_order_line_pool = pool.get('sale.order.line')
    product_material_pool = pool.get('product.material')
    product_category_pool = pool.get('product.category')
    surcharge = 0
    if not post:
        post = kw[0]
    product_material_id = product_material_pool.search(
        cr, uid, [('filter_name', '=', post.get('material_name'))],
        context=context)
    if not post.get('material_name'):
        product_material_ids = product_material_pool.search(
            cr, uid, [], context=context)
        if product_material_ids:
            product_material_id = product_material_ids[0]
    product_material = product_material_pool.browse(
        cr, uid, product_material_id, context=context)
    product_template = post.get('product')
    if not product_template:
        product_template = product_template_pool.browse(
            cr, uid, int(post.get('product_id')), context=context)
    if not product_template:
        return False
    product_product_ids = product_product_pool.search(
        cr, SUPERUSER_ID, [
            ('product_tmpl_id', '=', product_template.id),
            ('material_id', '=', product_material_id),
            ('length', '=', 6.0),
            ('product_element_width', '=', product_template.element_width),
            ('product_element_height', '=',
             product_template.element_height),
            ('product_square', '=', product_template.square)],
        context=context, limit=1)
    product_product = product_product_pool.browse(
        cr, uid, product_product_ids, context=context)

    sale_order = request.website.sale_get_order(force_create=1)
    optimal_quantity = 0
    optimal_qty = product_template.product_optimal_qty_ids
    for optimal in optimal_qty:
        if product_material_id == optimal.material_id:
            optimal_quantity = optimal.optimal_qty
    if not optimal_quantity:
        optimal_quantity = product_material.minimum_purchase

    lq = math.ceil(optimal_quantity / 6.0)
    optimal_quantity = 6.0 * lq
    if product_product:

        if product_product.rule:
            surcharge = product_product.surcharge
        else:
            surcharge = 0.00
        line = request.website.sale_get_order(force_create=1)._cart_update(
            product_id=int(product_product.id),
            add_qty=optimal_quantity,
            set_qty=optimal_quantity)
        sale_order_line_pool.write(
            cr, SUPERUSER_ID, int(line.get('line_id')),
            {'additional_amount': surcharge},
            context=context)
    else:
        vals = {
            'profile_density': product_material.density,
            'categ_id': product_material.categ_id.id,
            'material_id': product_material.id,
            'have_glue': False,
            'length': 6.0,
            'product_element_width': product_template.element_width,
            'product_element_height':
                product_template.element_height,
            'product_square':
                product_template.square,
            'default_code': product_template.article,
        }
        try:
            vals.update({
                'product_tmpl_id': product_template.id,
                'active': True})
        except:
            pass
        product_id = product_product_pool.create(
            cr, SUPERUSER_ID, vals, context=context)
        product_product = product_product_pool.browse(
            cr, SUPERUSER_ID, product_id, context=context)
        if product_template.owned_by == 'Customer':

            product_template.create_article()
            product_product = product_product_pool.browse(
                cr, SUPERUSER_ID, product_id, context=context)
            if product_product.rule:
                surcharge = product_product.surcharge
            else:
                surcharge = 0.00
            product_product.create_second_name()
        else:
            product_product.create_second_name()
        product_product = product_product_pool.browse(
            cr, uid, product_product.id, context=context)
        line = request.website.sale_get_order(force_create=1)._cart_update(
            product_id=int(product_product.id),
            add_qty=optimal_quantity,
            set_qty=optimal_quantity)
        if product_template.owned_by == 'Customer':
            sale_order_line_pool.write(
                cr, SUPERUSER_ID, int(line.get('line_id')),
                {'additional_amount': surcharge, 'color_laser_marking':'no'},
                context=context)
            calculate_commission(sale_order)
    try:
        extruder_product_id = product_product.extruder_product_id
    except:
        extruder_product_id = ''

    if not extruder_product_id \
            and product_product.product_tmpl_id.owned_by == 'Customer':
        material_group = product_product.material_id.product_extruder_group_id
        materials = product_material_pool.search(cr, uid, [
            ('product_extruder_group_id', '=', material_group.id)],
                                                 context=context)

        product_product_ids = product_product_pool.search(
            cr, SUPERUSER_ID, [
                ('id', '!=', product_product.id),
                ('product_tmpl_id', '=', product_template.id),
                ('material_id', 'in', materials),
                ('product_element_width', '=',
                 product_template.element_width),
                ('product_element_height', '=',
                 product_template.element_height),
                ('product_square', '=', product_template.square),
                ('extruder_product_id', '!=', None)],
            context=context)
        if not product_product_ids:
            second_name = '{name} for <{second_name}>'.format(
                name=product_template.extruder_template_id.name,
                second_name=product_product.second_name)
            extruder_vals = {
                'product_tmpl_id': product_template.extruder_template_id.id,
                'second_name': second_name,
                'description': second_name,
            }
            extruder_product_id = product_product_pool.create(
                cr, uid, extruder_vals, context=context)
            product_category_ids = product_category_pool.search(
                cr, uid, [('name', '=', 'Thermevo')], context=context)
            product_category_objects = product_category_pool.browse(
                cr, uid, product_category_ids, context=context)
            category_id = ''
            for product_category_object in product_category_objects:
                if product_category_object.parent_id:
                    category_id = product_category_object.id
            if category_id:
                product_product_pool.write(
                    cr, uid, extruder_product_id, {'categ_id': category_id},
                    context=context)
            product_product_pool.write(
                cr, SUPERUSER_ID, product_product.id,
                {'extruder_product_id': extruder_product_id},
                context=context)
        else:
            product_product_pool.write(
                cr, SUPERUSER_ID, product_product.id,
                {'extruder_product_id': product_product_pool.browse(
                    cr, uid, product_product_ids[0],
                    context=context).extruder_product_id.id},
                context=context)
    if product_template.owned_by == 'Customer':
        add_extruder_product(
            cr, uid, pool, int(line.get('line_id')),
            {'final_tooling_price': 0}, context)
    res = {
        'type': 'add',
        'line_ids': [int(line.get('line_id'))]
    }
    calculate_new_price(cr, SUPERUSER_ID, pool, res, context)
    sale_order_pool = pool.get('sale.order')
    sale_order_id = request.session.get('sale_order_id')
    sale_order = sale_order_pool.browse(
        cr, SUPERUSER_ID, sale_order_id, context=context)
    sale_order.button_dummy()
    sale_order.with_context(only_line=True).check_opportunity()
    sale_order.check_count_box()

    return shopping_cart_html(sale_order, sale_order.order_line)
