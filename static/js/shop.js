/**
 * Created by rokealva on 05.11.16.
 */
$(document).ready(function () {
    $('#sort_width_id').change(function () {
        $('#sort_product_form_id').submit();
    });

    // $("#search_form").find('input')[2].value = '';

    $("#material_selector").on('change', function () {
        openerp.jsonRpc("/shop/change_product_material/", 'call', {
            'material_id': $(this).val()
        }).then(function (data) {
            if (data) {
                $('#min_length').text(data['min_length']);
                $('#max_length').text(data['max_length']);
                $('#check_int').val(data['minimum_purchase']);
                $('.title #foam_type').text(data['foam_type']);
                $('#interval').text(data['interval']);
                if (data['gluewire']) {
                    $('#gluewire').css('display', 'block')
                }
                else {
                    $('#gluewire').css('display', 'none')
                }
                if (data['foam_type'] == ' PUR') {
                    $('#matrix_foam_pe').css('display', 'none');
                    $('#matrix_foam_pur').css('display', 'block');
                    $('#length').val('');
                    $('#width').val('');
                    $('.matrix-label-checked').removeClass('matrix-label-checked')
                }
                if (data['foam_type'] == ' PE') {
                    $('#matrix_foam_pe').css('display', 'block');
                    $('#matrix_foam_pur').css('display', 'none');
                    $('#length').val('');
                    $('#width').val('');
                    $('.matrix-label-checked').removeClass('matrix-label-checked')
                }
            }
        });
    });

    $(".form_item").find('#vat').on('change', function () {
        if ($(this).val()) {
            openerp.jsonRpc("/shop/delivery_address/checkvat", 'call', {
                'vat': $(this).val(),
                'country': $('#country_id').val(),
            }).then(function (data) {
                $('#vat_verify').val(data['check_vat']);
                if (!data['check_vat']) {
                    $('#euro_vat_error').addClass("error");
                    $('#euro_vat_success').removeClass("success");
                    $('#euro_vat_error span span').text(data['msg']);
                    $('#property_account_position').val(data['msg']);
                    $('#euro_vat_success_germany').removeClass("germanysuccess");
                } else {
                    $('#euro_vat_success').addClass("success");
                    $('#euro_vat_error').removeClass("error");
                    $('#euro_vat_success span span').text(data['msg']);
                    $('#property_account_position').val(data['msg']);
                    $('#euro_vat_success_germany').removeClass("germanysuccess");
                }
            });
        }
        var newVATNumber = checkVATNumber(document.getElementById('vat').value);
        if (newVATNumber) {
            document.getElementById('vat').value = newVATNumber;
        }
    });

    $(".form_item").find('#country_id').on('change', function () {
        if ($(this).val()) {
            openerp.jsonRpc("/shop/delivery_address/check_country", 'call', {
                'country': $(this).val(),
            }).then(function (data) {
                if (data['banned'] == '1') {
                    $('.banned_country_msg').css('display', 'block');
                }
                else {
                    $('.banned_country_msg').css('display', 'none');
                }
            });
        }
    });


    $(function () {
        $(".chosen-select").chosen();
    });

    $("#send_email_invoice_page").click(function () {
        var account_invoice = $("#account_invoice").val();
        openerp.jsonRpc("/shop/invoice/invoice_send", 'call', {
            'account_invoice': account_invoice
        }).then(function (data) {
            $("#send_email_invoice_page_success_msg").css('display', 'block');
        });
    });


    $(".change_customer").find('#customer_selector').on('change', function () {
        var customer_id = $(this).val();
        if (customer_id == '0') {
            $('#customer_name').css('display', 'block');
        }
        else {
            if (customer_id) {
                $('#customer_name').css('display', 'none');
                $('.upload_preload_data_start').css('display', 'block');
                $('.update_quote').css('display', 'block');
                openerp.jsonRpc("/shop/shopping_cart/change_customer", 'call', {
                    'customer': $(this).val()
                });
                setTimeout(function () {
                    location.reload();
                }, 2000);
            }
        }
    });


    $(".change_opportunity").find('#case_selector').on('change', function () {
        var case_id = $(this).val();
        if (case_id == '0') {
            $('#case_name').css('display', 'block');
        }
        else {
            if (case_id) {
                $('#case_name').css('display', 'none');
                $('.upload_preload_data_start').css('display', 'block');
                $('.update_quote').css('display', 'block');
                openerp.jsonRpc("/shop/shopping_cart/change_case", 'call', {
                    'case': $(this).val()
                }).then(function (data) {
                    setTimeout(function () {
                        location.reload();
                    }, 400);
                });
            }
        }
    });


    $(".form_item").find("#create_customer").click(function () {
        var link = $(this).parent().find('.link').val();
        var customer_name = $(this).parent().find('.customer_name').val();
        var customer = $(this).parent().find('.customer').val();
        openerp.jsonRpc(link, 'call', {
            'customer_name': customer_name,
            'customer': customer
        }).then(function (data) {
            setTimeout(function () {
                location.reload(true);
            }, 1500);
        });
    });

    $("#create_case").on('click', function () {
        var link = $(this).parent().find('.link').val();
        var case_name = $(this).parent().find('.case_name').val();
        var case_id = $(this).parent().find('.case').val();
        openerp.jsonRpc(link, 'call', {
            'case_name': case_name,
            'case': case_id
        }).then(function (data) {
            if (data['link'] != '') {
                window.location = data['link'];
            }
        });
    });

    $('#change_opportunity').on('click', function () {
        var link = $(this).parent().find('.opportunity_id').val();
        window.location = link;
    })

    $("#delivery_address_form .form_part .form_item #assign_to_me").on('click', function () {
        var link = $(this).attr('href');
        openerp.jsonRpc(link, 'call', {}).then(function (data) {

            setTimeout(function () {
                location.reload(true);
            }, 1000);
        });
        return false;
    });

    $("#another_address").on('change', function () {
        if ($(this).prop("checked")) {
            $("#another_address_block").css('display', 'block');
            document.getElementById("shipping_name").required = 'required';
            document.getElementById("shipping_street").required = 'required';
            document.getElementById("shipping_city").required = 'required';
            document.getElementById("shipping_country_id").required = 'required';
            document.getElementById("shipping_phone").required = 'required';
            document.getElementById("shipping_zip").required = 'required';
            document.getElementById("shipping_last_name").required = 'required';
        }
        else {
            $("#another_address_block").css('display', 'none');
            document.getElementById("shipping_name").required = false;
            document.getElementById("shipping_street").required = false;
            document.getElementById("shipping_city").required = false;
            document.getElementById("shipping_country_id").required = false;
            document.getElementById("shipping_phone").required = false;
            document.getElementById("shipping_zip").required = false;
            document.getElementById("shipping_last_name").required = false;
        }
    });

    $("#delivery_address").on('change', function () {
        var delivery_address_id = $(this).val();
        if (delivery_address_id == 0) {
            $('#shipping_name').val('');
            $('#shipping_street').val('');
            $('#shipping_city').val('');
            $('#shipping_country_id').val('');
            $('#shipping_phone').val('');
            $('#shipping_zip').val('');
        }
        else {
            openerp.jsonRpc("/shop/change_shipping_address", 'call', {
                'delivery_address_id': delivery_address_id
            }).then(function (data) {
                $('#shipping_name').val(data['shipping_name']);
                $('#shipping_street').val(data['shipping_street']);
                $('#shipping_city').val(data['shipping_city']);
                $('#shipping_country_id').val(data['shipping_country_id']);
                $('#shipping_phone').val(data['shipping_phone']);
                $('#shipping_zip').val(data['shipping_zip']);
            });
        }
    });

    // $(".form_item").find('#other_length').on('keyup', function () {
    //     var lenght = $("#other_length").val();
    //     var max_lenght = $("#max_length").html();
    //     var min_lenght = $("#min_length").html();
    //     if ($.isNumeric(lenght) != true) {
    //         $(this).addClass('error');
    //         $("#error_from_input_text_length").css("display", "block");
    //         $("#error_from_lenth_is_small").css("display", "none");
    //         $("#error_from_lenth_is_big").css("display", "none");
    //         $('#other_length').addClass('error');
    //         $(this).parent().parent().find('.form_action').css("display", "none");
    //         return;
    //         event.preventDefault();
    //     } else {
    //         if (parseFloat(lenght) < parseFloat(min_lenght)) {
    //             $("#error_from_lenth_is_small").css("display", "block");
    //             $("#error_from_input_text_length").css("display", "none");
    //             $("#error_from_lenth_is_big").css("display", "none");
    //             $('#other_length').addClass('error');
    //             $(this).parent().parent().find('.form_action').css("display", "none");
    //             return;
    //         }
    //         else {
    //             if (parseFloat(lenght) > parseFloat(max_lenght)) {
    //                 $("#error_from_lenth_is_big").css("display", "block");
    //                 $("#error_from_input_text_length").css("display", "none");
    //                 $("#error_from_lenth_is_small").css("display", "none");
    //                 $('#other_length').addClass('error');
    //                 $(this).parent().parent().find('.form_action').css("display", "none");
    //                 return;
    //             }
    //             else {
    //                 $("#error_from_input_text_length").css("display", "none");
    //                 $("#error_from_lenth_is_small").css("display", "none");
    //                 $("#error_from_lenth_is_big").css("display", "none");
    //                 $('#other_length').removeClass('error');
    //                 $(this).parent().parent().find('.form_action').css("display", "block");
    //                 return;
    //             }
    //         }
    //     }
    //     if (lenght == '') {
    //         $('#other_length').parent('div').removeClass('error');
    //         $("#error_from_input_text_length").css("display", "none");
    //         $("#error_from_lenth_is_small").css("display", "none");
    //         $("#error_from_lenth_is_big").css("display", "none");
    //     }
    // });

    $(".copy_to_cart").click(function () {
        $('.upload_preload_data_start').css('display', 'block');
        $('.update_quote').css('display', 'block');
    });

    $("body").on('change', '.right_cost .hsp input', function () {
        var obj = $(this).closest('.right_cost');
        var discount = parseFloat($.trim($(this).val()));
        var line_id = $(this).closest('.product_item_wrap').find('.product_item .order_line').val();
        var min_commiss = parseFloat($.trim($('#min_commiss').val()));
        var max_commiss = parseFloat($.trim($('#max_commiss').val()));
        if (min_commiss > discount) {
            obj.find('input').addClass('error');
        }
        else {
            if (discount > max_commiss) {
                obj.find('input').addClass('error');
            } else {
                openerp.jsonRpc("/shop/added_discount", 'call', {
                    'discount': discount,
                    'line_id': line_id

                }).then(function (data) {
                    obj.find('input').removeClass('error');
                    obj.find('.cost span').text(data['price_subtotal']);
                    $('#total span').text(data['amount_untaxed']);
                    $('#tax span').text(data['amount_tax']);
                    $('#total_cost span').text(data['amount_total']);
                    $('#commission .commission').text(data['commission']);
                    $('#commission .percent_commission').text(data['percent_commission']);
                    $(data['line_id']).val(data['discount']);
                });
            }
        }
    });

    $('#default_pricelist').on('change', function () {
        var default_pricelist = $(this).val();
        $('.upload_preload_data_start').css('display', 'block');
        $('.update_quote').css('display', 'block');
        openerp.jsonRpc("/shop/shopping_cart/use_default_pricelist", 'call', {
            'default_pricelist': default_pricelist
        }).then(function (data) {
            if (data) {
                location.reload(true);
            }
            else {
                $('.upload_preload_data_start').css('display', 'none');
                $('.update_quote').css('display', 'none');
            }
        })
    });

    $('.supplier_comment').on('click', function () {
        var link = $(this).find('.link a').attr('href');
        openerp.jsonRpc(link, 'call', {}).then(function (data) {
            if (data) {
                $("#flixo_popup_upload_file").find('.supplier_comment_text').html('').html(data);
                $("#flixo_popup_bg").css('display', 'block');
                $("#flixo_popup_upload_file").css('display', 'block')
            }
        })
    });

    $('#supplier_delivery_plus').on('click', function () {
        $("#supplier_delivery_plus").css('display', 'none');
        $("#delivery_document").css('display', 'block');
        $("#scheduled_date").css('display', 'block');
        $("#supplier_delivery_minus").css('display', 'block');
    });

    $('#supplier_delivery_minus').on('click', function () {
        $("#supplier_delivery_plus").css('display', 'block');
        $("#delivery_document").css('display', 'none');
        $("#scheduled_date").css('display', 'none');
        $("#supplier_delivery_minus").css('display', 'none');
    });

    $('#supplier_invoice_plus').on('click', function () {
        $("#supplier_invoice_plus").css('display', 'none');
        $("#invoice_document").css('display', 'block');
        $("#supplier_invoice_minus").css('display', 'block');
    });

    $('#supplier_invoice_minus').on('click', function () {
        $("#supplier_invoice_plus").css('display', 'block');
        $("#invoice_document").css('display', 'none');
        $("#supplier_invoice_minus").css('display', 'none');
    });

    $('.scheduled_date_accept').on('click', function () {
        var delivery_document_id = $(this).parent().find('.delivery_document_id').val();
        var document_scheduled_date = $(this).parent().find('.document_scheduled_date').val();
        var block = $(this).parent();
        openerp.jsonRpc('/shop/supplier_order/add_scheduled_date/', 'call', {
            'delivery_document_id': delivery_document_id,
            'document_scheduled_date': document_scheduled_date
        }).then(function (data) {
            if (data) {
                if (data['scheduled_date']) {
                    block.find('.document_scheduled_date').css('display', 'none');
                    block.html(data['scheduled_date']);
                }
            }
        })
    });

    $(".print_invoice").click(function () {
        var sale_order = $(this).parent().find('.sale_order_id').val();
        openerp.jsonRpc('/shop/get_invoice/', 'call', {
            'sale_order': sale_order,
        }).then(function (data) {
            if (data['return_invoice']) {
                $("#quotation_popup").find('#invoice_data').html('').html(data['return_invoice']);
                $("#quotation_popup_bg").css('display', 'block');
                $("#quotation_popup").css('display', 'block')
            }
        })
    });

    $(".print_delivery").click(function () {
        var sale_order = $(this).parent().find('.sale_order_id').val();
        openerp.jsonRpc('/shop/get_delivery/', 'call', {
            'sale_order': sale_order,
        }).then(function (data) {
            if (data['return_invoice']) {
                $("#quotation_popup").find('#invoice_data').html('').html(data['return_invoice']);
                $("#quotation_popup_bg").css('display', 'block');
                $("#quotation_popup").css('display', 'block')
            }
        })
    });

    $('.quotation_upload_file_cancel').click(function () {
        $("#quotation_popup_bg").css('display', 'none');
        $("#quotation_popup").css('display', 'none');
    });

    $('.product_filter_name').on('click', function () {
        var span = $(this).closest('ul').find('li span')
        jQuery.each(span, function () {
            $(this).find('span').attr('style','')
        });
        $(this).find('span').attr('style', 'color:red');
        var product_filter_name = $(this).find('span').html();
        var product_filter = $(this).closest('.product_filter');
        product_filter.find('#active_material').attr('value', product_filter_name);
        product_filter.closest('.catalog').find('.title span').html(product_filter_name);
        $('#switcher-block').find('input').each(function () {
            if ($(this).prop('checked') == true) {
                var swich = $(this).attr('value');
                var search = document.getElementById('search_input').value;
                var filter_name = document.getElementById('active_material').value;
                openerp.jsonRpc('/shop/get_personal_product/', 'call', {
                    'product_type': swich,
                    'search': search,
                    'filter_name': filter_name
                }).then(function (data) {
                    $('#list-products').html(data['result'])
                })
            }
        });
    });

    $('#switcher-block input').on('change', function () {
        var swich = $(this).attr('value');
        var name_block = $(this).closest('#switcher-block').find('.switcher-title');
        var search = document.getElementById('search_input').value;
        var filter_name = document.getElementById('active_material').value;

        if (swich == 'personal') {
            name_block.html('User product')
        }
        else {
            if (swich == 'company') {
                name_block.html('Company product')
            } else {
                name_block.html('All product')
            }
        }

        openerp.jsonRpc('/shop/get_personal_product/', 'call', {
            'product_type': swich,
            'search': search,
            'filter_name':filter_name
        }).then(function (data) {
            $('#list-products').html(data['result'])
        })
    });


    $('.search_submit').on('click', function () {
         $('#switcher-block').find('input').each(function () {
            if ($(this).prop('checked') == true) {
                var swich = $(this).attr('value');
                var search = document.getElementById('search_input').value;
                var filter_name = document.getElementById('active_material').value;
                openerp.jsonRpc('/shop/get_personal_product/', 'call', {
                    'product_type': swich,
                    'search': search,
                    'filter_name': filter_name
                }).then(function (data) {
                    $('#list-products').html(data['result'])
                })
            }
        });

    })

});


