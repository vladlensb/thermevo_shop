function getCursorPosition(element) {
    if (element.selectionStart) return element.selectionStart;
    else if (document.selection) {
        element.focus();
        var r = document.selection.createRange();
        if (r == null) return 0;

        var re = element.createTextRange(),
            rc = re.duplicate();
        re.moveToBookmark(r.getBookmark());
        rc.setEndPoint('EndToStart', re);
        return rc.text.length;
    }
    return 0;
}

function demoMatchClick(el) {
    var re = new RegExp("^[0-9]*[.][0-9]+$");
    if (el.match(re)) {
        return true;
    } else {
        return false;
    }
}
$(document).ready(function () {

    $('.operation_list .change').click(function () {
        if ($(this).hasClass('active')) {
            $('.operation_list a').removeClass('active');
            $('.change_option').slideUp();
        } else {
            $('.operation_list a').removeClass('active');
            $(this).addClass('active');
            $('.foam_option').slideUp(400, function () {
                $('.change_option').slideDown();
            });
        }
        return false;
    });
    $('.operation_list .foam').click(function () {
        if ($(this).hasClass('active')) {
            $('.operation_list a').removeClass('active');
            $('.foam_option').slideUp();
        } else {
            $('.operation_list a').removeClass('active');
            $(this).addClass('active');
            $('.change_option').slideUp(400, function () {
                $('.foam_option').slideDown();
            });
        }
        return false;
    });
    $('body').on('change', '.select_all_wrap #select_all', function () {
        if ($(this).prop('checked') == true) {
            $('.product_item_wrap .select_product input').prop('checked', true);
        } else {
            $('.product_item_wrap .select_product input').prop('checked', false);
        }
    });
    $('body').on('keyup', '.shopping_cart .ajax_count input', function (e) {
        if ($(this).val() != '') {
            if ($(this).val() == 0) {
                $(this).closest('.ajax_count').find('a').css('display', 'none');
                $(this).closest('.ajax_count').find('.delete_product').css('display', 'block');
            } else {
                $(this).closest('.ajax_count').find('a').css('display', 'none');
                $(this).closest('.ajax_count').find('.save_product').css('display', 'block');
            }
        } else {
            /*$(this).val('0');*/
            $(this).closest('.ajax_count').find('a').css('display', 'none');
            $(this).closest('.ajax_count').find('.delete_product').css('display', 'block');
        }
    });
    $('body').on("keypress", '.product_item_wrap .ajax_count input,.option_wrap .change_option', function (event) {
        var inputCode = event.which;
        var currentValue = $(this).val();
        if (inputCode > 0 && (inputCode < 48 || inputCode > 57)) {
            if (inputCode == 46) {
                if (getCursorPosition(this) == 0 && currentValue.charAt(0) == '-') return false;
                if (currentValue.match(/[.]/)) return false;
            }
            else if (inputCode == 45) {
                if (currentValue.charAt(0) == '-') return false;
                if (getCursorPosition(this) != 0) return false;
            }
            else if (inputCode == 8) return true;
            else return false;

        }
        else if (inputCode > 0 && (inputCode >= 48 && inputCode <= 57)) {
            if (currentValue.charAt(0) == '-' && getCursorPosition(this) == 0) return false;
        }
    });

    $(".shopping_content").on("keypress", ".editble.number", function (event) {
        if (event.which == 8) {

        } else if (event.which == 46 & $(this).html().indexOf('.') != -1) {
            event.preventDefault();
        } else if (event.which < 46 || event.which > 59) {
            event.preventDefault();
        }

        if (event.which == 46 && $(this).val().indexOf('.') != -1) {
            event.preventDefault();
        } // prevent if already dot
    });
    $(".shopping_content").on("blur", ".editble", function () {
        var editable = $(this);
        var line_id = $(this).closest('.product_item_wrap').find('.product_item .order_line').val();
        if ($(this).hasClass('number')) {
            if ($.isNumeric(editable.html()) || demoMatchClick(editable.html())) {
                editable.css('color', '#015995');
            } else {
                editable.css('color', 'red');
                return false;
            }
        }
        /*$.ajax({
         type: "GET",
         url: "/ajax/shopping_return_data.php",
         success: function (data) {
         $('.upload_preload_data_start').css('display', 'none');
         $('body').append('<div class="hidden_ajax" style="display:none">' + data + '</div>');
         $('.header_cart').html($('.hidden_ajax').find('.header_cart').html());
         $('.shopping_cart').html($('.hidden_ajax').find('.shopping_cart').html());
         $('.cart-link').html($('.hidden_ajax').find('.cart-link').html());
         $('.hidden_ajax').remove();
         }
         });*/
        $('.upload_preload_data_start').css('display', 'block');
        $('.calculation_product').css('display', 'block');
        openerp.jsonRpc('/shop/shopping_cart/change_product_parametr/', 'call', {
            'text': editable.html(),
            'line_id': line_id,
            'type': editable.attr('type')
        }).then(function (data) {
            $('.upload_preload_data_start').css('display', 'none');
            $('.calculation_product').css('display', 'none');
            $('body').append('<div class="hidden_ajax" style="display:none">' + data + '</div>');
            $('.header_cart').html($('.hidden_ajax').find('.header_cart').html());
            $('.shopping_cart').html($('.hidden_ajax').find('.shopping_cart').html());
            $('.cart-link').html($('.hidden_ajax').find('.cart-link').html());
            if ($.trim($('.cart-link').text()) > 0) {
                $('.cart-link').addClass('active');
            } else {
                $('.cart-link').removeClass('active');
            }
            $('.hidden_ajax').remove();
        });
    });

    $('body').on('click', '.quantity a', function () {
        var el = $(this);
        var quantity = $(this).closest('.ajax_count').find('.input_quantity').val();
        var line_id = $(this).closest('.product_item_wrap').find('.product_item .order_line').val();
        $('.upload_preload_data_start').css('display', 'block');
        $('.calculation_product').css('display', 'block');

        /*$.ajax({
         type: "GET",
         url: "/ajax/shopping_return_data.php",
         success: function (data) {
         $('body').append('<div class="hidden_ajax" style="display:none">' + data + '</div>');
         $('.header_cart').html($('.hidden_ajax').find('.header_cart').html());
         $('.shopping_cart').html($('.shopping_cart').find('.header_cart').html());
         $('.hidden_ajax').remove();
         }
         });*/
        openerp.jsonRpc('/shop/shopping_cart_change', 'call', {
            'quantity': quantity,
            'line_id': line_id,
            'type': 'line_change'
        }).then(function (data) {
            $('.upload_preload_data_start').css('display', 'none');
            $('.calculation_product').css('display', 'none');
            $('body').append('<div class="hidden_ajax" style="display:none">' + data + '</div>');
            $('.header_cart').html($('.hidden_ajax').find('.header_cart').html());
            $('.shopping_cart').html($('.hidden_ajax').find('.shopping_cart').html());
            $('.cart-link').html($('.hidden_ajax').find('.cart-link').html());
            if ($.trim($('.cart-link').text()) > 0) {
                $('.cart-link').addClass('active');
            } else {
                $('.cart-link').removeClass('active');
            }
            $('.hidden_ajax').remove();
        });

        return false;
    });

    $('body').on('click', '.annual a', function () {
        var el = $(this);
        var quantity = $(this).closest('.ajax_count').find('input').val();
        var line_id = $(this).closest('.product_item_wrap').find('.product_item .order_line').val();
        $('.upload_preload_data_start').css('display', 'block');
        $('.calculation_product').css('display', 'block');

        /*$.ajax({
         type: "GET",
         url: "/ajax/shopping_return_data.php",
         success: function (data) {
         $('body').append('<div class="hidden_ajax" style="display:none">' + data + '</div>');
         $('.header_cart').html($('.hidden_ajax').find('.header_cart').html());
         $('.shopping_cart').html($('.shopping_cart').find('.header_cart').html());
         $('.hidden_ajax').remove();
         }
         });*/
        openerp.jsonRpc('/shop/shopping_cart_change', 'call', {
            'quantity': quantity,
            'line_id': line_id,
            'type': 'annual_change'
        }).then(function (data) {
            $('.upload_preload_data_start').css('display', 'none');
            $('.calculation_product').css('display', 'none');
            $('body').append('<div class="hidden_ajax" style="display:none">' + data + '</div>');
            $('.header_cart').html($('.hidden_ajax').find('.header_cart').html());
            $('.shopping_cart').html($('.hidden_ajax').find('.shopping_cart').html());
            $('.cart-link').html($('.hidden_ajax').find('.cart-link').html());
            if ($.trim($('.cart-link').text()) > 0) {
                $('.cart-link').addClass('active');
            } else {
                $('.cart-link').removeClass('active');
            }
            $('.hidden_ajax').remove();
        });
        return false;
    });
    $(".change_option input[type='submit']").on('click', function () {
        var arr_order_line = [];

        var formData = $(".change_option form").serializeArray();

        $('.shopping_cart .product_item_wrap .select_product input').each(function () {
            if ($(this).prop('checked') == true) {
                arr_order_line.push($(this).closest('.product_item_wrap').find('.order_line').val());
            }
        });
        var all_data = {
            'form': formData,
            'products': arr_order_line,
            'type': 'option'
        };
        $('.upload_preload_data_start').css('display', 'block');
        $('.calculation_product').css('display', 'block');
        /*$.ajax({
         type: "GET",
         data: all_data,
         url: "/ajax/shopping_return_data.php",
         success: function (data) {
         $('body').append('<div class="hidden_ajax" style="display:none">' + data + '</div>');
         $('.header_cart').html($('.hidden_ajax').find('.header_cart').html());
         $('.shopping_cart').html($('.shopping_cart').find('.header_cart').html());
         $('.hidden_ajax').remove();
         $('.operation_list a').removeClass('active');
         $('.change_option').slideUp();
         }
         });*/

        openerp.jsonRpc('/shop/shopping_cart_change', 'call', {
            'data': all_data
        }).then(function (data) {
            $('.upload_preload_data_start').css('display', 'none');
            $('.calculation_product').css('display', 'none');
            $('body').append('<div class="hidden_ajax" style="display:none">' + data + '</div>');
            $('.header_cart').html($('.hidden_ajax').find('.header_cart').html());
            $('.shopping_cart').html($('.hidden_ajax').find('.shopping_cart').html());
            $('.cart-link').html($('.hidden_ajax').find('.cart-link').html());
            if ($.trim($('.cart-link').text()) > 0) {
                $('.cart-link').addClass('active');
            } else {
                $('.cart-link').removeClass('active');
            }
            $('.hidden_ajax').remove();
            $('.operation_list a').removeClass('active');
            $('.change_option').slideUp();
        });
        return false;
    });
    $(".foam_option input[type='submit']").on('click', function () {
        var arr_order_line = [];

        var formData = $(".foam_option form").serializeArray();

        $('.shopping_cart .product_item_wrap .select_product input').each(function () {
            if ($(this).prop('checked') == true) {
                arr_order_line.push($(this).closest('.product_item_wrap').find('.order_line').val());
            }
        });
        var all_data = {
            'form': formData,
            'products': arr_order_line,
            'type': 'foam'
        };
        $('.upload_preload_data_start').css('display', 'block');
        $('.calculation_product').css('display', 'block');
        /*$.ajax({
         type: "GET",
         data: all_data,
         url: "/ajax/shopping_return_data.php",
         success: function (data) {
         $('body').append('<div class="hidden_ajax" style="display:none">' + data + '</div>');
         $('.header_cart').html($('.hidden_ajax').find('.header_cart').html());
         $('.shopping_cart').html($('.shopping_cart').find('.header_cart').html());
         $('.hidden_ajax').remove();
         $('.operation_list a').removeClass('active');
         $('.foam_option').slideUp();
         }
         });*/

        openerp.jsonRpc('/shop/shopping_cart_change', 'call', {
            'data': all_data
        }).then(function (data) {
            $('.upload_preload_data_start').css('display', 'none');
            $('.calculation_product').css('display', 'none');
            $('body').append('<div class="hidden_ajax" style="display:none">' + data + '</div>');
            $('.header_cart').html($('.hidden_ajax').find('.header_cart').html());
            $('.shopping_cart').html($('.hidden_ajax').find('.shopping_cart').html());
            $('.cart-link').html($('.hidden_ajax').find('.cart-link').html());
            if ($.trim($('.cart-link').text()) > 0) {
                $('.cart-link').addClass('active');
            } else {
                $('.cart-link').removeClass('active');
            }
            $('.hidden_ajax').remove();
            $('.operation_list a').removeClass('active');
            $('.foam_option').slideUp();
        });
        return false;
    });
    $(".shopping_content .dublicate").on('click', function () {
        var arr_order_line = [];

        $('.shopping_cart .product_item_wrap .select_product input').each(function () {
            if ($(this).prop('checked') == true) {
                arr_order_line.push($(this).closest('.product_item_wrap').find('.order_line').val());
            }
        });
        var all_data = {'products': arr_order_line, 'type': 'dublicate'};
        $('.upload_preload_data_start').css('display', 'block');
        $('.dublicate_product').css('display', 'block');
        /*$.ajax({
         type: "GET",
         data: all_data,
         url: "/ajax/shopping_return_data.php",
         success: function (data) {
         $('body').append('<div class="hidden_ajax" style="display:none">' + data + '</div>');
         $('.header_cart').html($('.hidden_ajax').find('.header_cart').html());
         $('.shopping_cart').html($('.shopping_cart').find('.header_cart').html());
         $('.cart-link').html($('.hidden_ajax').find('.cart-link').html());
         if ($.trim($('.cart-link').text()) > 0) {
         $('.cart-link').addClass('active');
         } else {
         $('.cart-link').removeClass('active');
         }
         $('.hidden_ajax').remove();
         }
         });*/

        openerp.jsonRpc('/shop/shopping_cart_change', 'call', {
            'data': all_data
        }).then(function (data) {
            $('.upload_preload_data_start').css('display', 'none');
            $('.dublicate_product').css('display', 'none');
            $('body').append('<div class="hidden_ajax" style="display:none">' + data + '</div>');
            $('.header_cart').html($('.hidden_ajax').find('.header_cart').html());
            $('.shopping_cart').html($('.hidden_ajax').find('.shopping_cart').html());
            $('.cart-link').html($('.hidden_ajax').find('.cart-link').html());
            if ($.trim($('.cart-link').text()) > 0) {
                $('.cart-link').addClass('active');
            } else {
                $('.cart-link').removeClass('active');
            }
            $('.hidden_ajax').remove();
        });
        return false;
    });
    $(".shopping_content .delete").on('click', function () {
        var arr_order_line = [];

        $('.shopping_cart .product_item_wrap .select_product input').each(function () {
            if ($(this).prop('checked') == true) {
                arr_order_line.push($(this).closest('.product_item_wrap').find('.order_line').val());
            }
        });
        var all_data = {'products': arr_order_line, 'type': 'delete'};
        $('.upload_preload_data_start').css('display', 'block');
        $('.calculation_product').css('display', 'block');
        /*$.ajax({
         type: "GET",
         data: all_data,
         url: "/ajax/shopping_return_data.php",
         success: function (data) {
         $('body').append('<div class="hidden_ajax" style="display:none">' + data + '</div>');
         $('.header_cart').html($('.hidden_ajax').find('.header_cart').html());
         $('.shopping_cart').html($('.shopping_cart').find('.header_cart').html());
         $('.hidden_ajax').remove();
         }
         });*/

        openerp.jsonRpc('/shop/shopping_cart_change', 'call', {
            'data': all_data
        }).then(function (data) {
            $('.upload_preload_data_start').css('display', 'none');
            $('.calculation_product').css('display', 'none');
            $('body').append('<div class="hidden_ajax" style="display:none">' + data + '</div>');
            $('.header_cart').html($('.hidden_ajax').find('.header_cart').html());
            $('.shopping_cart').html($('.hidden_ajax').find('.shopping_cart').html());
            $('.cart-link').html($('.hidden_ajax').find('.cart-link').html());
            if ($.trim($('.cart-link').text()) > 0) {
                $('.cart-link').addClass('active');
            } else {
                $('.cart-link').removeClass('active');
            }
            $('.hidden_ajax').remove();
            if (data['empty_cart']) {
                window.location.href = '/shop'
            }
        });

        return false;
    });
});
