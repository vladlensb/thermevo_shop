$('document').ready(function() {
    var swiper_insulating_strip = new Swiper('.insulating_strip .swiper-container', {

        slidesPerView: 3,
        loop: true,
        nextButton: '.insulating_strip .swiper-button-next',
        prevButton: '.insulating_strip .swiper-button-prev',
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            1279: {
                slidesPerView: 2,
                spaceBetween: 30
            }
        }

    });
    $('.uw_tabs .tabs_link li.active').each(function(){
        $('.uw_tabs #'+$(this).find('a').attr('rel')).addClass('active');
    });
    $('.uw_tabs .tabs_link li a').on('click', function(){
        $('.uw_tabs .tab').removeClass('active');
        $('.uw_tabs .tabs_link li a').removeClass('active');
        $(this).closest('li').addClass('active');
        $('.uw_tabs #'+$(this).attr('rel')).addClass('active');
    });

});