# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-Today Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


import logging

_logger = logging.getLogger(__name__)


def check(order_line, line):
    res = False
    if order_line and line:
        _logger.info(order_line.product_id.product_tmpl_id)
        _logger.info(line.product_id.product_tmpl_id)
        _logger.info('\n\n')


        if order_line.product_id.product_tmpl_id.id == line.product_id.product_tmpl_id.id \
                and order_line.product_id.material_id.id == line.product_id.material_id.id \
                and order_line.product_id.product_element_width == line.product_id.product_element_width \
                and order_line.product_id.product_element_height == line.product_id.product_element_height \
                and order_line.product_id.product_square == line.product_id.product_square \
                and order_line.gluewire == line.gluewire \
                and order_line.color_laser_marking == line.color_laser_marking:
            res = True
            for further_processing in order_line.further_processing_ids:
                if further_processing not in line.further_processing_ids:
                    res = False
    return res
