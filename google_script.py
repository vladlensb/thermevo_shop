# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-Today Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import SUPERUSER_ID
from openerp.http import Controller, route, request
from datetime import date
import httplib2
import math
from apiclient import discovery
from oauth2client.service_account import ServiceAccountCredentials
import logging
from openerp.http import request

_logger = logging.getLogger(__name__)


def google_sheet_dublicate(cr, uid, val, context=None):
    config_parameter_pool = request.registry.get('ir.config_parameter')
    config_parameter_id = config_parameter_pool.search(
        cr, uid, [('key', '=', 'credentials_file')])

    CREDENTIALS_FILE = config_parameter_pool.browse(
        cr, uid, config_parameter_id, context=context).value

    # CREDENTIALS_FILE = '/home/rokealva/Production/thermevo/odoo-8.0/openerp/addons/thermevo_uys/static/thermevo-63a1fc53c8ad.json'
    # CREDENTIALS_FILE = '/home/odoo/openerp/addons/thermevo_uys/static/thermevo-63a1fc53c8ad.json'
    # CREDENTIALS_FILE = '/home/odoo/thermevo_uys/static/thermevo-63a1fc53c8ad.json'
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        CREDENTIALS_FILE, ['https://www.googleapis.com/auth/spreadsheets',
                           'https://www.googleapis.com/auth/drive'])

    httpAuth = credentials.authorize(httplib2.Http())
    registry = val.get('registry')
    service = discovery.build('sheets', 'v4', http=httpAuth)
    scheme_price_config_pool = registry.get('scheme.price.config')
    scheme_price_config_ids = scheme_price_config_pool.search(
        cr, SUPERUSER_ID, [], context=context)
    scheme_price_config_object = scheme_price_config_pool.browse(
        cr, SUPERUSER_ID, scheme_price_config_ids, context=context)
    try:
        scheme_price_config_object = scheme_price_config_object[0]
    except:
        pass
    res_users_pool = request.registry.get('res.users')
    res_users = res_users_pool.browse(cr, uid, request.uid, context=context)
    path = 'result!A2:T2'
    product_product_object = val.get('product_product_objects')
    qty = val.get('qty')
    typ = product_product_object.product_tmpl_id.element_value
    if not typ:
        typ = 1
    laser_foil = protection_foil = e_low = punching = sandblasting = 0

    if val.get('sale_order_line'):
        for further_processing in \
                val.get('sale_order_line').further_processing_ids:
            if further_processing.identifier == 'protection_foil':
                protection_foil = 1
            if further_processing.identifier == 'e_low':
                e_low = 1
            if further_processing.identifier == 'sandblasting':
                sandblasting = 1
            if further_processing.identifier == 'punching':
                punching = 1
        sale_order_line = val.get('sale_order_line')
        if sale_order_line.gluewire:
            if sale_order_line.color_laser_marking == 'no':
                laser_foil = 4
            if sale_order_line.color_laser_marking != 'no':
                if not sale_order_line.print_logo:
                    laser_foil = 2
                else:
                    if sale_order_line.color_laser_marking == 'grey':
                        laser_foil = 2
                    else:
                        laser_foil = 1

        elif not sale_order_line.gluewire:
            if sale_order_line.color_laser_marking != 'no':
                laser_foil = 3
            else:
                laser_foil = 0

    if request.website:
        image_url = request.website.image_url(product_product_object, 'image')
    else:
        image_url = ''
    stege_breite = 0
    if product_product_object.stege_breite:
        stege_breite = product_product_object.stege_breite

    google_values = [
        [product_product_object.product_article,
         image_url,
         product_product_object.length,
         product_product_object.material_id.table_name_material,
         val.get('sale_order_name'),
         res_users.name,
         date.today().strftime('%Y_%m_%d'),
         product_product_object.number_of_tails,
         qty,
         0,
         stege_breite,
         product_product_object.product_element_width,
         product_product_object.product_element_height,
         product_product_object.product_square,
         typ,
         laser_foil,
         protection_foil,
         e_low,
         sandblasting,
         punching,
         ]
    ]
    service.spreadsheets().values().batchUpdate(
        spreadsheetId=scheme_price_config_object.spreadsheet_id, body={
            "valueInputOption": "USER_ENTERED",
            "data": [
                {"range": path,
                 "majorDimension": "ROWS",
                 "values": google_values,
                 },
            ]
        }).execute()
    result = service.spreadsheets().values().get(
        spreadsheetId=scheme_price_config_object.spreadsheet_id,
        range='result').execute()
    values = result.get('values', [])

    try:
        price = float(values[1][-5].split(' ')[0].replace(',', '.'))
    except:
        price = 0.00
    minimum_qty = values[1][-1]
    qty = ''
    for element in minimum_qty:
        if element in ['1', '2', '3', '4',
                       '5', '6', '7', '8',
                       '9', '0', ',', '.']:
            qty += element
    if qty:
        qty = qty.replace(',', '.')
        minimum_qty = float(qty)
    qty_in_pallet = (
        product_product_object.qty_in_pallet * product_product_object.length)
    if minimum_qty < qty_in_pallet:
        minimum_qty = qty_in_pallet
    else:
        koef = math.ceil(minimum_qty / qty_in_pallet)
        minimum_qty = qty_in_pallet * koef

    res = {
        'minimum_qty': minimum_qty,
        'roi_15': values[1][-2],
        'roi_20': values[1][-3],
        'price_kg': values[1][-4],
        'price': price,
        'speed': values[1][-6],
        'output_2': values[1][-7],
        'stage_total': values[1][-8],
        'output_1': values[1][-9],
        'extruder': values[1][-10],
        'final_tooling_price': values[1][-11],
        'total_tooling_price': values[1][-12],
    }

    registry.get('product.product').write(
        cr, SUPERUSER_ID, product_product_object.id,
        {'variant_price': price,
         'standard_price': price},
        context=context)
    if product_product_object.extruder_product_id:
        registry.get('product.product').write(
            cr, SUPERUSER_ID, product_product_object.extruder_product_id.id,
            {'variant_price': values[1][-12]}, context=context)

    key = 3
    # i = 1
    if not values:
        print('No data found.')
    else:
        if len(values) >= 2:
            key = len(values) + 1
    path = '%s!A%s:AF%s' % (scheme_price_config_object.range_names, key, key)
    google_values[0] = values[1]
    service.spreadsheets().values().batchUpdate(
        spreadsheetId=scheme_price_config_object.spreadsheet_id, body={
            "valueInputOption": "USER_ENTERED",
            "data": [
                {"range": path,
                 "majorDimension": "ROWS",
                 "values": google_values,
                 },
            ]
        }).execute()

    return res


def google_sheet_dublicate_po(cr, uid, val, context=None):

    config_parameter_pool = request.registry.get('ir.config_parameter')
    config_parameter_id = config_parameter_pool.search(
        cr, uid, [('key', '=', 'credentials_file')])

    CREDENTIALS_FILE = config_parameter_pool.browse(
        cr, uid, config_parameter_id, context=context).value

    # CREDENTIALS_FILE = '/home/rokealva/Production/thermevo/odoo-8.0/openerp/addons/thermevo_uys/static/thermevo-63a1fc53c8ad.json'
    # CREDENTIALS_FILE = '/home/odoo/openerp/addons/thermevo_uys/static/thermevo-63a1fc53c8ad.json'
    # CREDENTIALS_FILE = '/home/odoo/thermevo_uys/static/thermevo-63a1fc53c8ad.json'
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        CREDENTIALS_FILE, ['https://www.googleapis.com/auth/spreadsheets',
                           'https://www.googleapis.com/auth/drive'])
    httpAuth = credentials.authorize(httplib2.Http())
    registry = val.get('registry')
    service = discovery.build('sheets', 'v4', http=httpAuth)
    scheme_price_config_pool = registry.get('scheme.price.config')
    scheme_price_config_ids = scheme_price_config_pool.search(
        cr, SUPERUSER_ID, [], context=context)
    scheme_price_config_object = scheme_price_config_pool.browse(
        cr, SUPERUSER_ID, scheme_price_config_ids, context=context)
    try:
        scheme_price_config_object = scheme_price_config_object[0]
    except:
        pass
    res_users_pool = request.registry.get('res.users')
    res_users = res_users_pool.browse(cr, uid, request.uid, context=context)
    path = 'result!A3:T3'
    product_product_object = val.get('product_product_objects')
    qty = val.get('qty')
    typ = product_product_object.product_tmpl_id.element_value
    if not typ:
        typ = 1
    laser_foil = 0
    protection_foil = e_low = punching = sandblasting = 0

    if val.get('sale_order_line'):
        for further_processing \
                in val.get('sale_order_line').further_processing_ids:
            if further_processing.identifier == 'protection_foil':
                protection_foil = 1
            if further_processing.identifier == 'e_low':
                e_low = 1
            if further_processing.identifier == 'sandblasting':
                sandblasting = 1
            if further_processing.identifier == 'punching':
                punching = 1
        if val.get('sale_order_line').gluewire:
            laser_foil = 1

    if request.website:
        image_url = request.website.image_url(product_product_object, 'image')
    else:
        image_url = ''
    stege_breite = 0
    if product_product_object.stege_breite:
        stege_breite = product_product_object.stege_breite

    google_values = [
        [product_product_object.product_article,
         image_url,
         product_product_object.length,
         product_product_object.material_id.table_name_material,
         val.get('sale_order_name'),
         res_users.name,
         date.today().strftime('%Y_%m_%d'),
         product_product_object.number_of_tails,
         qty,
         0,
         stege_breite,
         product_product_object.product_element_width,
         product_product_object.product_element_height,
         product_product_object.product_square,
         typ,
         laser_foil,
         protection_foil,
         e_low,
         sandblasting,
         punching,
         ]
    ]

    _logger.info(google_values)

    service.spreadsheets().values().batchUpdate(
        spreadsheetId=scheme_price_config_object.spreadsheet_id, body={
            "valueInputOption": "USER_ENTERED",
            "data": [
                {"range": path,
                 "majorDimension": "ROWS",
                 "values": google_values,
                 },
            ]
        }).execute()
    result = service.spreadsheets().values().get(
        spreadsheetId=scheme_price_config_object.spreadsheet_id,
        range='result').execute()
    values = result.get('values', [])

    try:
        price = float(values[2][-4].split(' ')[0].replace(',', '.'))
    except:
        price = 0.00
    res = {
        'roi_15': values[2][-1],
        'roi_20': values[2][-2],
        'price_kg': values[2][-3],
        'price': price,
        'speed': values[2][-5],
        'output_2': values[2][-6],
        'stage_total': values[2][-7],
        'output_1': values[2][-8],
        'extruder': values[2][-9],
        'final_tooling_price': values[2][-10],
        'total_tooling_price': values[2][-11],
    }
    return res