# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import fields, models, api
from datetime import date, datetime
import base64
from pyexcel_xlsx import get_data
import json


class SupplierPricelistWizard(models.TransientModel):
    _name = "supplier.pricelist.wizard"

    pricelist = fields.Binary(string='Pricelist')

    @api.multi
    def added_supplier_pricelist(self):
        config_parameter = self.env['ir.config_parameter'].search(
            [('key', '=', 'price_download_path')])
        pricelist = base64.b64decode(self.pricelist)
        price_file = open(config_parameter.value + 'price.xlsx', 'wb')
        price_file.write(pricelist)
        price_file.close()
        data = get_data(config_parameter.value + 'price.xlsx')
        pricelist_data = json.loads(json.dumps(data))
        keys = pricelist_data.keys()
        partner_id = self.env.context.get('active_id')
        partner_fixed_product_pool = self.env['partner.fixed.product']
        supplier_stock_information_pool = self.env['supplier.stock.information']
        products = supplier_stock_information_pool.search(
            [('partner_id', '=', partner_id)])
        if products:
            products.unlink()
        for key in keys:
            list_data = pricelist_data.get(key)
            del list_data[0]
            for d in list_data:
                further_processing = d[7]
                further_processing_ids = []
                if further_processing:
                    further_processing_list = further_processing.split(' / ')
                    further_processing_pool = self.env['further.processing']
                    for further in further_processing_list:
                        further_processing_ids.append(
                            further_processing_pool.search(
                                [('name', '=', further)]))

                price = d[8]
                qty = d[9]
                values = {'partner_id': partner_id,
                          'product_id': d[0],
                          'e_low_open_groove': d[3],
                          'gluewire': d[4],
                          'color_laser_marking': d[5],
                          'print_logo': d[6],
                          'price_unit': d[8],
                          'further_processing_ids':
                              [(6, 0, further_processing_ids)]}

                val = values.copy()

                if price > 0.0:
                    values.update(price_unit=d[8])
                    products = partner_fixed_product_pool.search(
                        [('partner_id', '=', partner_id),
                         ('product_id', '=', d[0]),
                         ('e_low_open_groove', '=', d[3]),
                         ('gluewire', '=', d[4]),
                         ('color_laser_marking', '=', d[5]),
                         ('print_logo', '=', d[6])])
                    if products:
                        need_product = []
                        if further_processing_ids:
                            for product in products:
                                if product.further_processing_ids.ids ==\
                                        further_processing_ids:
                                    need_product.append(product)
                        if not need_product:
                            need_product = products
                        if len(need_product) > 1:
                            raise
                        else:
                            need_product.write({'price_unit': d[8]})
                    else:
                        partner_fixed_product_pool.create(values)

                if qty > 0.0:
                    val.update(product_qty=d[9], date_upload=datetime.now())
                    products = supplier_stock_information_pool.search(
                        [('partner_id', '=', partner_id),
                         ('product_id', '=', d[0]),
                         ('e_low_open_groove', '=', d[3]),
                         ('gluewire', '=', d[4]),
                         ('color_laser_marking', '=', d[5]),
                         ('print_logo', '=', d[6])])
                    need_product = []
                    if further_processing_ids:
                        for product in products:
                            if product.further_processing_ids.ids == \
                                    further_processing_ids:
                                need_product.append(product)
                    if not need_product:
                        need_product = products
                    if len(need_product) > 1:
                        old_qty = sum([p.product_qty for p in need_product])
                        print '\n old_qty >>>>>> %s' % old_qty
                        raise
                    elif not need_product or need_product.state == 'reserved':
                        supplier_stock_information_pool.create(val)
                    else:
                        need_product.write({'product_qty': d[9],
                                            'date_upload': date.today()})
