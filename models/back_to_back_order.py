##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.exceptions import ValidationError
from openerp.osv import fields, osv, expression
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, \
    DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP
from openerp.tools.float_utils import float_compare
from openerp import SUPERUSER_ID
import logging
from openerp.addons.thermevo_shop.google_script \
    import google_sheet_dublicate_po

from openerp.addons.thermevo_shop.check_line_method \
    import check

_logger = logging.getLogger(__name__)


class BackToBackOrder(osv.osv_memory):
    _inherit = "back.to.back.order"

    def create_order(self, cr, uid, ids, context):
        error_product = []
        product_category_ids = self.pool.get(
            'product.category').search(
            cr, uid, [('name', 'in',
                       ('Wooden Box', 'Steel Box', 'Service Units'))])
        for po in self.browse(cr, uid, ids):
            for line in po.line_ids:
                if not line.product_id.extruder:
                    if not line.product_id.stege_breite \
                            or not line.product_id.product_element_height \
                            or not line.product_id.product_element_width \
                            or not line.product_id.product_square:

                        if line.product_id.categ_id.id \
                                not in product_category_ids:
                            error_product.append(line.product_id)
        if error_product:
            product_name = ',\n'.join(product.name for product in error_product)
            raise ValidationError(_('Cannot create the PO - '
                                    'missing input field(s) in the product.:'
                                    '\n %s' % product_name))
        purchase_obj = self.pool.get('purchase.order')
        sale_obj = self.pool.get('sale.order')
        sale_order = sale_obj.browse(
            cr, uid, context.get('active_id'), context=context)
        purchase_line_obj = self.pool.get('purchase.order.line')
        product_uom = self.pool.get('product.uom')
        product_product = self.pool.get('product.product')
        supplierinfo = False
        res = {}
        purchase_setting_pool = self.pool.get('purchase.config.settings')
        purchase_setting_id = purchase_setting_pool.search(cr, uid, [])
        purchase_setting = purchase_setting_pool.browse(
            cr, uid, purchase_setting_id, context=context)
        try:
            purchase_setting = purchase_setting[0]
        except:
            pass
        purchase_id = None
        for po in self.browse(cr, uid, ids):
            vals = {
                'partner_id': po.partner_id.id,
                'date_order': po.date_order,
                'picking_type_id': po.picking_type_id.id,
                'location_id': po.location_id.id,
                'pricelist_id':
                    po.partner_id.property_product_pricelist_purchase
                    and po.partner_id.property_product_pricelist_purchase.id,
                'validator': uid,
                'partner_safe_stock': sale_order.partner_safe_stock
            }
            if purchase_setting:
                vals['invoice_method'] = purchase_setting.default_invoice_method
            if sale_order.partner_safe_stock:
                partner_ids = [sale_order.partner_id.id, ]
                location_id = None
                if sale_order.partner_id.parent_id:
                    partner_ids = [sale_order.partner_id.parent_id.id] + [
                        p.id for p in sale_order.partner_id.parent_id.child_ids]
                if partner_ids:
                    location_id = self.pool.get('stock.location').search(
                        cr, uid, [('partner_id', 'in', partner_ids)],
                        limit=1)

                    if not location_id:
                        location_id = sale_order.partner_id.safe_stock_id.id
                    else:
                        try:
                            location_id = location_id[0]
                        except:
                            pass
                if location_id:
                    vals['location_id'] = location_id
            purchase_id = purchase_obj.create(cr, uid, vals, context=context)
            context_partner = context.copy()
            if po.partner_id.id:
                lang = po.partner_id.lang
                context_partner.update(
                    {'lang': lang, 'partner_id': po.partner_id.id})
            for line in po.line_ids:
                if line.qty <= 0:
                    continue
                else:
                    date_order = fields.datetime.now()
                    product = product_product.browse(
                        cr, uid, line.product_id.id, context=context_partner)
                    name = line.name
                    if product.description_purchase:
                        name += '\n' + product.description_purchase
                    precision = self.pool.get(
                        'decimal.precision').precision_get(
                        cr, uid, 'Product Unit of Measure')
                    for supplier in product.seller_ids:
                        if po.partner_id.id \
                                and supplier.name.id == po.partner_id.id:
                            supplierinfo = supplier
                            if (supplierinfo.product_uom.id !=
                                    line.product_uom.id):
                                res['warning'] = {
                                    'title': _('Warning!'),
                                    'message': _('The selected supplier '
                                                 'only sells this product'
                                                 ' by %s')
                                               % supplierinfo.product_uom.name}
                            min_qty = product_uom._compute_qty(
                                cr, uid, supplierinfo.product_uom.id,
                                supplierinfo.min_qty,
                                to_uom_id=line.product_uom.id)
                            if float_compare(min_qty,
                                             line.qty,
                                             precision_digits=precision) == 1:
                                if line.qty:
                                    res['warning'] = {
                                        'title': _('Warning!'),
                                        'message':
                                            _('The selected supplier '
                                              'has a minimal quantity '
                                              'set to %s %s, you should'
                                              ' not purchase less.') %
                                            (supplierinfo.min_qty,
                                             supplierinfo.product_uom.name)}
                                line.qty = min_qty
                    dt = purchase_line_obj._get_date_planned(
                        cr, uid, supplierinfo, date_order,
                        context=context).strftime(
                        DEFAULT_SERVER_DATETIME_FORMAT)
                    tax_ids = [tax.id for tax in line.taxes_ids]
                    further_processing_ids = [
                        proces.id for proces
                        in line.sale_order_line_id.further_processing_ids]
                    purchase_line_pool = self.pool.get('purchase.order.line')
                    values = {
                        'product_id': line.product_id.id,
                        'name': name,
                        'date_planned': dt,
                        'product_qty': line.qty,
                        'price_unit': 0,
                        'price_subtotal': 0,
                        'order_id': purchase_id,
                        'sale_order_id': context['active_id'],
                        'sale_order_line_id': line.sale_order_line_id.id,
                        'taxes_id': [(6, 0, tax_ids)],
                        'product_uom': line.product_id.uom_po_id.id,
                        'further_processing_ids': further_processing_ids,
                        'e_low_open_groove': line.sale_order_line_id.e_low_open_groove,
                        'gluewire': line.sale_order_line_id.gluewire,
                        'color_laser_marking':
                            line.sale_order_line_id.color_laser_marking,
                        'print_logo': line.sale_order_line_id.print_logo,
                    }
                    purchase_line_pool.create(
                        cr, uid, values, context=context)


            line_blocks = []
            check_lines = []
            purchase_lines = purchase_obj.browse(
                cr, uid, purchase_id, context=context).order_line
            for purchase_line in purchase_lines:
                lines = []
                if purchase_line not in check_lines \
                        and not purchase_line.product_id.extruder \
                        and purchase_line.product_id.categ_id.id \
                                not in product_category_ids:

                    check_lines.append(purchase_line)
                    line_blocks.append(purchase_line)
                    for check_purchase_line in purchase_lines:
                        if check(check_purchase_line.sale_order_line_id,
                                 purchase_line.sale_order_line_id) \
                                and check_purchase_line not in lines:
                            check_lines.append(check_purchase_line)
                            line_blocks.append(check_purchase_line)
            for line_block in line_blocks:
                all_qty = 0
                for line in line_block:
                    all_qty += line.product_qty
                fix_line = None
                for partner_line in po.partner_id.fixed_product_id:
                    result = check(partner_line, line.sale_order_line_id)
                    if result:
                        fix_line = partner_line
                if not fix_line and po.partner_id.parent_id:
                    for partner_line in po.partner_id.parent_id.fixed_product_id:
                        result = check(partner_line, line.sale_order_line_id)
                        if result:
                            fix_line = partner_line
                if fix_line:
                    line.price_unit = fix_line.price_unit
                else:
                    google_values = {
                        'registry': self.pool,
                        'product_product_objects': line.product_id,
                        'have_glue': False,
                        'length': line.product_id.length,
                        'qty': all_qty,
                        'sale_order_name': line.order_id.name,
                        'sale_order_line': line.sale_order_line_id,
                    }
                    google_answer = google_sheet_dublicate_po(
                        cr, SUPERUSER_ID, google_values, context=context)

                    for line in line_block:
                        line.price_unit = google_answer.get('price')

                    for purchase_line in purchase_lines:
                        if line_block \
                                and purchase_line.product_id.extruder \
                                and (line_block[
                                         0].product_id.extruder_product_id.id
                                         == purchase_line.product_id.id):
                            purchase_line.price_unit = google_answer.get(
                                'final_tooling_price')

        po_dict = []
        char_purchase_ids = ''
        for line in sale_order.purchase_line_ids:
            if line.order_id.id not in po_dict:
                char_purchase_ids += '%s, ' % line.order_id.name
        sale_obj.write(
            cr, uid, context.get('active_id'),
            {'char_purchase_ids': char_purchase_ids,
             'purchase_id': purchase_id},
            context=context)
        purchase_order = purchase_obj.browse(
            cr, uid, purchase_id, context=context)
        for line in purchase_order.order_line:
            if line.sale_order_line_id:
                line.sale_order_line_id.purchase_price = line.price_unit



        return True
