# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields, models, api, SUPERUSER_ID
from openerp.osv import osv
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _
from datetime import datetime, date
import math
import logging
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, \
    DEFAULT_SERVER_DATETIME_FORMAT
import time
from openerp.addons.thermevo_shop.google_script \
    import google_sheet_dublicate
from openerp.addons.thermevo_shop.check_line_method \
    import check
from openerp.exceptions import Warning

_logger = logging.getLogger(__name__)


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    commission = fields.Float(string="Commission",
                              digits_compute=dp.get_precision('Discount'),
                              default=0)
    minimum_qty = fields.Float(string="Minimum Order Qty")
    website_description_sale_line = fields.Text(
        'Website Description Sale Line')

    fixed_price = fields.Boolean(string="Fixed Price",
                                 default=False)

    @api.multi
    def change_default_code(self):
        new_default_code = '000000.0/00000'

        try:
            if self:
                product = self.product_id
            else:
                product = self.env['product.product'].browse(
                    self.env.context.get('product_id'))
        except:
            return new_default_code
        product_category = self.env['product.category'].search(
            [('name', 'in', ('Wooden Box', 'Steel Box', 'Service Units'))])
        if product.extruder or product.categ_id in product_category:
            return product.name.lower()
        old_default_code = product.default_code
        default_code_dict = None
        if old_default_code:
            default_code_dict = old_default_code.split('/')
            static_default_code = default_code_dict[0].split('.')[0]
        else:
            static_default_code = old_default_code

        if default_code_dict and len(default_code_dict) > 1:
            length = default_code_dict[1]
        else:
            length = product.length

        geometry = 2

        if self.gluewire:
            if self.color_laser_marking == 'no':
                geometry = 2
            if self.color_laser_marking != 'no':
                if not self.print_logo:
                    geometry = 4
                else:
                    if self.color_laser_marking == 'grey':
                        geometry = 6
                    else:
                        geometry = 8
        else:
            if self.color_laser_marking and self.color_laser_marking != 'no':
                if not self.print_logo:
                    geometry = 3
                else:
                    if self.color_laser_marking == 'grey':
                        geometry = 5
                    else:
                        geometry = 7
            else:
                if self.e_low_open_groove:
                    geometry = 0
                else:
                    geometry = 1
        further = 0
        laser = 0
        foam = 0
        color = 0
        default_code = u'%s%s%s%s%s'

        # for further_processing in order_line.further_processing_ids:
        #     further = further_processing.processing_article_number

        if product.material_id.material_color:
            color = product.material_id.material_color

        new_default_code = u'%s.%s/%s' % (
            static_default_code,
            default_code % (geometry, further, laser, foam, color),
            length)
        if product.extruder:
            new_default_code = u''
        return new_default_code

    @api.multi
    def create_short_name(self):
        if not self:
            product = self.env['product.product'].browse(
                self.env.context.get('product_id'))
            product_category = self.env['product.category'].search(
                [('name', 'in', ('Wooden Box', 'Steel Box', 'Service Units'))])
            if product.extruder or product.categ_id in product_category:
                return None
            name_template = u'Insulating Strip THERMEVO EvoBreak '

            name_template += u'%s, ' % product.material_id.name

            name_template += u'%s mm x %s mm, ' % (
                product.product_element_width,
                product.product_element_height)
            name_template += u'%s m' % str(product.length)

            if product.product_tmpl_id.owned_by == 'Customer':
                crm_lead = product.product_tmpl_id.lead_id
                if crm_lead:
                    try:
                        dxf_filename = crm_lead.dxf_filename.replace(
                            '.input', '')
                        name_template += ', cust. ref# %s' % dxf_filename
                    except:
                        pass
            return name_template
        for line in self:
            name_template = u'Insulating Strip THERMEVO EvoBreak '
            try:
                product = line.product_id
                if not product:
                    product = self.env['product.product'].browse(
                        self.env.context.get('product_id'))
            except:
                return name_template
            product_category = self.env['product.category'].search(
                [('name', 'in', ('Wooden Box', 'Steel Box', 'Service Units'))])
            if product.extruder or product.categ_id in product_category:
                return None

            name_template += u'%s, ' % product.material_id.name

            name_template += u'%s mm x %s mm, ' % (
                product.product_element_width,
                product.product_element_height)
            name_template += u'%s m' % str(product.length)

            if product.product_tmpl_id.owned_by == 'Customer':
                crm_lead = product.product_tmpl_id.lead_id
                if crm_lead:
                    try:
                        dxf_filename = crm_lead.dxf_filename.replace(
                            '.input', '')
                        name_template += ', cust. ref# %s' % dxf_filename
                    except:
                        pass
            return name_template

    @api.one
    def create_website_description_sale_line(self):
        try:
            product = self.product_id
        except:
            product = self.env['product.product'].browse(
                self.env.context.get('product_id'))
        product_category = self.env['product.category'].search(
            [('name', 'in', ('Wooden Box', 'Steel Box', 'Service Units'))])
        if product.extruder or product.categ_id in product_category:
            return product.name
        website_description_sale_description = \
            u'{name},  length: {length} m'.format(name=product.material_id.name,
                                                  length=product.length)
        glue_laser_option = u''
        try:
            if self.gluewire and self.color_laser_marking != 'no':
                glue_laser_option += u'Glue Wire'
                if not self.print_logo:
                    if self.color_laser_marking == 'grey':
                        glue_laser_option += u' Grey Laser Marking'
                    elif self.color_laser_marking == 'white':
                        glue_laser_option += u' White Laser Marking'
                else:
                    if self.color_laser_marking == 'grey':
                        glue_laser_option += u' Grey Laser Marking Logo'
                    elif self.color_laser_marking == 'white':
                        glue_laser_option += u' White Laser Marking Logo'
            elif self.gluewire and self.color_laser_marking == 'no':
                glue_laser_option += u'Glue Wire'
            elif not self.gluewire and self.color_laser_marking != 'no':
                if not self.print_logo:
                    if self.color_laser_marking == 'grey':
                        glue_laser_option += u'Grey Laser Marking'
                    elif self.color_laser_marking == 'white':
                        glue_laser_option += u'White Laser Marking'
                else:
                    if self.color_laser_marking == 'grey':
                        glue_laser_option += u'Grey Laser Marking Logo'
                    elif self.color_laser_marking == 'white':
                        glue_laser_option += u'White Laser Marking Logo'
                if self.color_laser_marking:
                    glue_laser_option += u' No Gluewire with open groove'
            website_description_sale_description += u', %s' % glue_laser_option
            for further_processing in self.further_processing_ids:
                further = further_processing.name
                if website_description_sale_description:
                    website_description_sale_description += u';\n{name}'.format(
                        name=further)
                else:
                    website_description_sale_description += u'{name}'.format(
                        name=further)
            return website_description_sale_description
        except:
            return website_description_sale_description

    @api.multi
    def create_description(self):

        description = None
        short_name = None
        product_article = None
        website_description = None
        try:
            if self:
                product = self.product_id
            else:
                product = self.env['product.product'].browse(
                    self.env.context.get('product_id'))
        except:
            return description, short_name, product_article, website_description

        product_category = self.env['product.category'].search(
            [('name', 'in', ('Wooden Box', 'Steel Box', 'Service Units'))])

        short_name = self.create_short_name()
        product_article = u'%s' % self.change_default_code()
        width = u''
        height = u''
        square = u''
        if product.product_element_width:
            width = u'Width: %s mm' % round(
                float(product.product_element_width), 2)
        if product.product_element_height:
            height = u'Height: %s mm' % round(
                float(self.product_id.product_element_height), 2)
        if product.square:
            square = u'Area: %s mm2' % product.square
        if not product.extruder:
            if product.categ_id not in product_category:
                description = u'%s;\n %s;\n ' % (short_name, product_article)
                if width:
                    description += u'%s; ' % width
                if height:
                    description += u'%s; ' % height
                if square:
                    description += u'%s;' % square
        else:
            description = product.name
        website_description = self.create_website_description_sale_line()
        if not product.extruder:
            if product.categ_id not in product_category:
                if website_description:
                    description += u'\n Options: %s' % website_description[0]
                    website_description = website_description[0]
        description_sale = self.product_id.create_description_sale()
        if not product.extruder:
            if product.categ_id not in product_category:
                description += u'\n %s' % description_sale
        if not description:
            description = product.name
        if self.id:
            if product.type == 'service' \
                    or product.categ_id in product_category:
                vals = {}
                if not self.name:
                    vals['name'] = product.name
                if not self.short_name:
                    vals['short_name'] = product.name
                if not self.line_article:
                    vals['line_article'] = product.name
                if not self.website_description_sale_line:
                    vals['website_description_sale_line'] = product.name
            else:
                self.write({'name': description,
                            'short_name': short_name,
                            'line_article': product_article,
                            'website_description_sale_line':
                                website_description})
        else:
            return description, short_name, \
                   product_article, website_description
        return description, short_name, product_article, website_description

    @api.multi
    def write(self, values):
        for line in self:
            if values.get('product_id') or values.get('soundblasting') \
                    or values.get('protection_foil') or values.get('e_low') \
                    or values.get('punching'):
                if not self.env.context.get('make_description'):
                    line.create_description()

            if line.fixed_price and values.get('price_unit'):
                del values['price_unit']

            # Change qty in stock.move
            if values.get('product_uom_qty'):
                stock_move_ids = self.env['stock.move'].search(
                    [('sale_order_line_id', '=', line.id)])

                if stock_move_ids:

                    done_moves = stock_move_ids.filtered(lambda item:
                                                         item.state == 'done')

                    not_done_moves = stock_move_ids.filtered(
                        lambda item: item.state in ['confirmed', 'assigned',
                                                    'partially_available',
                                                    'waiting']
                    )[0]

                    not_done_moves.picking_id.do_unreserve()
                    not_done_moves.picking_id.action_assign()

                    if done_moves and not_done_moves:
                        res = values.get('product_uom_qty') - \
                              sum([x.product_uom_qty for x in done_moves])
                        if res < 0:
                            raise Warning(_(
                                'You can not select fewer goods than '
                                'you already have in Delivery Order'
                            ))
                        not_done_moves.write(dict(
                            product_uom_qty=res
                        ))
                    elif not_done_moves:
                        not_done_moves.write(dict(
                            product_uom_qty=values.get('product_uom_qty')
                        ))

        res = super(SaleOrderLine, self).write(values)
        return res

    @api.cr_uid_ids_context
    def product_id_change(self, cr, uid, ids, pricelist, product, qty=0,
                          uom=False, qty_uos=0, uos=False, name='',
                          partner_id=False, lang=False, update_tax=True,
                          date_order=False, packaging=False,
                          fiscal_position=False, flag=False, context=None):
        if product:
            added_qty = qty
            context = context or {}
            if not partner_id:
                raise osv.except_osv(_('No Customer Defined!'),
                                     _('Before choosing a product,\n '
                                       'select a customer in the sales form.'))
            warning = False

            product_uom_obj = self.pool.get('product.uom')
            partner_obj = self.pool.get('res.partner')
            product_obj = self.pool.get('product.product')
            order_line_obj = self.pool.get('sale.order.line')

            partner = partner_obj.browse(cr, uid, partner_id)
            lang = partner.lang
            context_partner = context.copy()
            context_partner.update({'lang': lang, 'partner_id': partner_id})
            if not product:
                return {'value': {'th_weight': 0,
                                  'product_uos_qty': qty},
                        'domain': {'product_uom': [],
                                   'product_uos': []}}
            if not date_order:
                date_order = time.strftime(DEFAULT_SERVER_DATE_FORMAT)

            result = {}
            warning_msgs = ''
            product_obj = product_obj.browse(
                cr, uid, product, context=context_partner)
            context_partner.update({'product_id': product_obj.id})
            uom2 = False
            if uom:
                uom2 = product_uom_obj.browse(cr, uid, uom)
                if product_obj.uom_id.category_id.id != uom2.category_id.id:
                    uom = False
            if uos:
                if product_obj.uos_id:
                    uos2 = product_uom_obj.browse(cr, uid, uos)
                    if product_obj.uos_id.category_id.id != uos2.category_id.id:
                        uos = False
                else:
                    uos = False

            if not fiscal_position:
                fpos = partner.property_account_position or False
            else:
                fpos = self.pool.get('account.fiscal.position').browse(
                    cr, uid, fiscal_position)

            if partner and partner.property_account_position:
                fpos = partner.property_account_position

            if uid == SUPERUSER_ID and context.get('company_id'):
                taxes = product_obj.taxes_id.filtered(
                    lambda r: r.company_id.id == context['company_id'])
            else:
                taxes = product_obj.taxes_id
            result['tax_id'] = self.pool.get('account.fiscal.position').map_tax(
                cr, uid, fpos, taxes, context=context)
            if not ids:
                description, short_name, \
                product_article, website_description = \
                    self.create_description(
                        cr, uid, ids, context=context_partner)
                if description:
                    result.update({'name': description,
                                   'short_name': short_name,
                                   'line_article': product_article,
                                   'website_description_sale_line':
                                       website_description})
            else:
                self.create_description(cr, uid, ids, context=context_partner)
            result['fixed_amount'] = product_obj.fixed_amount
            if product_obj.description_sale and result.get('name'):
                result['name'] += '\n' + product_obj.description_sale

            domain = {}
            product_category_ids = self.pool.get('product.category').search(
                cr, uid, [('name', 'in',
                           ('Wooden Box', 'Steel Box', 'Service Units'))])
            min_qty = product_obj.qty_in_pallet * product_obj.length

            if ids:
                result['product_uom_qty'] = self.check_qty(
                    cr, uid, ids, qty, context=context)[0]
            else:
                if (not uom) and (not uos):
                    result['product_uom'] = product_obj.uom_id.id

                    if qty and product_obj.categ_id.id \
                            not in product_category_ids:
                        if product_obj.per_length:
                            lq = math.ceil(
                                qty / product_obj.length)
                            qty = product_obj.length * lq
                        else:
                            lq = math.ceil(qty / min_qty)
                            qty = min_qty * lq
                    if product_obj.uos_id:
                        result['product_uos'] = product_obj.uos_id.id
                        result['product_uos_qty'] = qty * product_obj.uos_coeff
                        result['product_uom_qty'] = qty * product_obj.uos_coeff
                        uos_category_id = product_obj.uos_id.category_id.id
                    else:
                        result['product_uos'] = False
                        result['product_uos_qty'] = qty
                        result['product_uom_qty'] = qty
                        uos_category_id = False
                    result['th_weight'] = qty * product_obj.weight
                    domain = {
                        'product_uom':
                            [('category_id', '=',
                              product_obj.uom_id.category_id.id)],
                        'product_uos':
                            [('category_id', '=', uos_category_id)]}
                elif uos and not uom:
                    if qty and product_obj.categ_id.id \
                            not in product_category_ids:
                        if product_obj.per_length:
                            lq = math.ceil(
                                qty / product_obj.length)
                            qty = product_obj.length * lq
                        else:
                            lq = math.ceil(qty_uos / min_qty)
                            qty = min_qty * lq
                    result['product_uom'] = product_obj.uom_id \
                                            and product_obj.uom_id.id
                    result['product_uom_qty'] = qty_uos / product_obj.uos_coeff
                    result['th_weight'] = result['product_uom_qty'] * \
                                          product_obj.weight
                elif uom:  # whether uos is set or not
                    if qty and product_obj.categ_id.id \
                            not in product_category_ids:
                        if product_obj.per_length:
                            lq = math.ceil(
                                qty / product_obj.length)
                            qty = product_obj.length * lq
                        else:
                            lq = math.ceil(qty / min_qty)
                            qty = min_qty * lq
                    default_uom = product_obj.uom_id and product_obj.uom_id.id
                    q = product_uom_obj._compute_qty(
                        cr, uid, uom, qty, default_uom)
                    if product_obj.uos_id:
                        result['product_uos'] = product_obj.uos_id.id
                        result['product_uos_qty'] = qty * product_obj.uos_coeff
                        result['product_uom_qty'] = qty * product_obj.uos_coeff
                    else:
                        result['product_uos'] = False
                        result['product_uos_qty'] = qty
                        result['product_uom_qty'] = qty
                    result['th_weight'] = q * product_obj.weight

            product_category_ids = self.pool.get('product.category').search(
                cr, SUPERUSER_ID,
                [('name', 'in', ('Wooden Box', 'Steel Box', 'Service Units'))])
            if product_obj.categ_id.id in product_category_ids:
                result['product_uos_qty'] = added_qty
                result['product_uom_qty'] = added_qty

            if not uom2:
                uom2 = product_obj.uom_id
            # get unit price
            if not pricelist:
                warn_msg = _('You have to select a pricelist or '
                             'a customer in the sales form !\n'
                             'Please set one before choosing a product.')
                warning_msgs += _("No Pricelist ! : ") + warn_msg + "\n\n"
            else:
                ctx = dict(context, uom=uom or result.get('product_uom'),
                           date=date_order, )
                price = None
                list_price = self.pool.get('product.pricelist').price_get(
                    cr, uid, [pricelist], product, qty or 1.0,
                    partner_id, ctx)[pricelist]
                frm_cur = self.pool.get('res.users').browse(
                    cr, uid, uid).company_id.currency_id.id
                to_cur = self.pool.get('product.pricelist').browse(
                    cr, uid, [pricelist])[0].currency_id.id
                p_price = product_obj.standard_price
                to_uom = result.get('product_uom', uom)
                if to_uom != product_obj.uom_id.id:
                    p_price = self.pool['product.uom']._compute_price(
                        cr, uid, product_obj.uom_id.id, p_price, to_uom)
                pctx = context.copy()
                pctx['date'] = date_order
                purchase_price = self.pool.get('res.currency').compute(
                    cr, uid, frm_cur, to_cur, p_price, round=False,
                    context=pctx)
                result['purchase_price'] = purchase_price
                product_category_ids = self.pool.get(
                    'product.category').search(
                    cr, uid, [('name', 'in',
                               ('Wooden Box', 'Steel Box', 'Service Units'))])
                if product_obj.categ_id.id not in product_category_ids:
                    price = list_price
                if ids:
                    try:
                        line = order_line_obj.browse(
                            cr, uid, ids, context=context)
                        if line.fixed_price:
                            price = line.price_unit
                        elif not line.fixed_price and line.order_id.crm_lead_id:
                            # if line.order_id.sale_use_default_pricelist == 2:
                            opportunity_price = \
                                line.with_context(
                                    only_line=True).check_opportunity_line()
                            if opportunity_price:
                                price = opportunity_price[0]
                    except:
                        pass
                if not price:
                    price = product_obj.standard_price
                if price is False:
                    warn_msg = _("Cannot find a pricelist line matching "
                                 "this product and quantity.\n"
                                 "You have to change either the product, "
                                 "the quantity or the pricelist.")
                    warning_msgs += _(
                        "No valid pricelist line found ! :") + warn_msg + "\n\n"
                else:
                    price = self.pool.get(
                        'account.tax')._fix_tax_included_price(
                        cr, uid, price, taxes, result['tax_id'])
                    result.update({'price_unit': price})
                    if context.get('uom_qty_change', False):
                        values = {'price_unit': price,
                                  'product_uom_qty': result['product_uom_qty']}
                        if result.get('product_uos_qty'):
                            values['product_uos_qty'] = result[
                                'product_uos_qty']
                        return {'value': values, 'domain': {}, 'warning': False}
            if warning_msgs:
                warning = {'title': _('Configuration Error!'),
                           'message': warning_msgs}
            return {'value': result, 'domain': domain, 'warning': warning}

    @api.one
    def check_opportunity_line(self):
        price = self.price_unit
        if self.fixed_price:
            return price
        try:
            contract = None
            for order in self.order_id.crm_lead_id.sale_order_ids:
                if order.default_pricelist and order != self.order_id:
                    contract = order
            if contract:
                recalculate_line = []
                contract_line = []
                for order_line in contract.order_line:
                    price_unit = self.price_unit
                    if (self.product_id.product_tmpl_id ==
                            order_line.product_id.product_tmpl_id
                            and self.product_id.material_id ==
                            order_line.product_id.material_id
                            and self.gluewire == order_line.gluewire
                            and self.color_laser_marking ==
                            order_line.color_laser_marking
                            and self.print_logo == order_line.print_logo
                            and len(self.further_processing_ids)
                            == len(order_line.further_processing_ids)):
                        check = True

                        for f in self.further_processing_ids:
                            fg_ids = order_line.further_processing_ids
                            if f not in fg_ids:
                                check = False
                        if check:
                            price = order_line.price_unit
                            contract_line.append(self.id)
                        else:
                            price = price_unit
                    else:
                        if self.id not in recalculate_line \
                                and self.id not in contract_line:
                            recalculate_line.append(self.id)
                if recalculate_line and not self.env.context.get('only_line'):
                    self.order_id.calculate_new_price(
                        {'line_ids': recalculate_line, 'type': u'option'})
            else:
                recalculate_line = [l.id for l in self.order_id.order_line]
                if not self.env.context.get('only_line'):
                    self.order_id.calculate_new_price(
                        {'line_ids': recalculate_line, 'type': u'option'})
        except:
            print '>>>>>>>>>>> FUCK >>>>>>>'
        return price

    @api.one
    def check_qty(self, new_quantity):
        qty = 0
        product_category = self.env['product.category'].search(
            [('name', 'in', ('Wooden Box', 'Steel Box', 'Service Units'))])

        if self.product_id.categ_id not in product_category:
            if self.order_id and \
                    (not self.order_id.crm_lead_id \
                     or (self.order_id.crm_lead_id
                         and not self.order_id.crm_lead_id.part_pallet)):
                qty = new_quantity
                if self.product_id.length:
                    if self.product_id.per_length:
                        lq = math.ceil(new_quantity / self.product_id.length)
                        qty = self.product_id.length * lq
                    elif self.product_id.qty_in_pallet:
                        min_qty = (
                                self.product_id.qty_in_pallet *
                                self.product_id.length)
                        lq = math.ceil(new_quantity / min_qty)
                        qty = min_qty * lq
            elif self.order_id.crm_lead_id \
                    and self.order_id.crm_lead_id.part_pallet:
                lq = math.ceil(new_quantity / self.product_id.length)
                qty = self.product_id.length * lq
        return qty


class SaleOrder(models.Model):
    _inherit = "sale.order"

    commission = fields.Float(string="Commission",
                              digits_compute=dp.get_precision('Discount'),
                              default=0)
    percent_commission = fields.Float(string="Commission",
                                      digits_compute=dp.get_precision(
                                          'Discount'),
                                      default=0)
    sale_use_default_pricelist = fields.Integer(string="Use Pricelist")
    minimum_planned_date = fields.Date(string='Expected Date')
    no_box = fields.Boolean(string="No Box", default=False)
    manual_box = fields.Boolean(string="Manual Box", default=False)
    partner_safe_stock = fields.Boolean(string="Safe Stock", default=False)
    sale_order_type = fields.Selection(
        selection=[('product', 'Product')],
        string='Sale Order Type', translate=True,
        default='product')

    def action_set_draft(self, cr, uid, ids, context=None):

        for order in self.browse(cr, uid, ids, context=context):
            for line in order.order_line:
                line.write({'state': 'draft'})
                for procurement_id in line.procurement_ids:
                    procurement_id.write({'state': 'draft'})
            order.write({'state': 'draft'})

    # def action_button_confirm(self, cr, uid, ids, context=None):
    #     if not context:
    #         context = {}
    #     assert len(ids) == 1, \
    #         'This option should only be used for a single id at a time.'
    #     self.write(cr, uid, ids, {'user_id': uid}, context=context)
    #     self.signal_workflow(cr, uid, ids, 'order_confirm')
    #     if context.get('send_email'):
    #         self.force_quotation_send(cr, uid, ids, context=context)
    #     return True

    def check_opportunity(self, cr, uid, ids, context=None):
        sale_order = self.browse(cr, uid, ids, context=context)
        product_category_ids = self.pool.get('product.category').search(
            cr, SUPERUSER_ID, [
                ('name', 'in', ('Wooden Box', 'Steel Box', 'Service Units'))])
        for line in sale_order.order_line:

            try:
                if line.product_id.categ_id.id not in product_category_ids:
                    if not line.product_id.extruder:
                        opportunity_price = line.check_opportunity_line()
                        try:
                            opportunity_price = opportunity_price[0]
                        except:
                            pass
                        if opportunity_price:
                            line.write({'price_unit': opportunity_price})
            except:
                return False
        return True

    def button_dummy(self, cr, uid, ids, context=None):
        sale_order = self.browse(cr, uid, ids, context=context)
        ctx = context.copy()
        ctx['uom_qty_change'] = True
        for line in sale_order.order_line:
            try:
                if line.product_id and not line.product_id.extruder \
                        and line.product_id.type != 'service':
                    product_uom_qty = line.check_qty(line.product_uom_qty)[0]
                    res = line.product_id_change(sale_order.pricelist_id.id,
                                                 line.product_id.id,
                                                 qty=product_uom_qty,
                                                 partner_id=sale_order.partner_id.id,
                                                 uom=line.product_uom.id,
                                                 context=ctx)
                    if res.get('value').get('price_unit'):
                        try:
                            val = {'price_unit': res.get('value').get(
                                'price_unit')}
                            self.pool.get('sale.order.line').write(
                                cr, SUPERUSER_ID, line.id,
                                val, context=context)
                        except:
                            pass
            except:
                print '>>>>> FUCK >>>>>'
        return True

    @api.multi
    def write(self, values):
        super(SaleOrder, self).write(values)
        if len(self) == 1:
            self.check_count_box()
            self.button_dummy()
        return True

    @api.multi
    def check_count_box(self):
        sale_order_pricelist = None
        if not self.no_box and not self.manual_box:
            if self.crm_lead_id:
                for order in self.crm_lead_id.sale_order_ids:
                    if order.default_pricelist:
                        sale_order_pricelist = order
            if not self.env.context.get('check_count_box'):
                product_category = self.env['product.category'].search(
                    [('name', 'in', ('Wooden Box', 'Steel Box'))])
                pallet_quantity = 0
                order_line_pallet = None
                check_line = []
                for sale_order in self:
                    for order_line in sale_order.order_line:
                        if order_line.product_id.categ_id \
                                not in product_category:
                            if not order_line.product_id.extruder \
                                    and order_line.product_id.type != 'service':
                                check_line.append(order_line)
                        elif order_line.product_id.categ_id in product_category:
                            order_line_pallet = order_line
                if sale_order_pricelist and self.crm_lead_id.part_pallet:
                    tmp_pallet_qty = 0
                    i = 0
                    for order_line in check_line:
                        i += 1
                        product_uom_qty = order_line.product_uom_qty
                        qty_in_pallet = order_line.product_id.qty_in_pallet
                        if 0 < tmp_pallet_qty < 0.999:
                            free_pallet = 0.98 - tmp_pallet_qty
                            add_qty = (
                                    order_line.product_id.qty_in_pallet *
                                    free_pallet * order_line.product_id.length)

                            if add_qty > order_line.product_uom_qty:

                                tmp_pallet_quantity = (
                                        product_uom_qty /
                                        order_line.product_id.length /
                                        qty_in_pallet)
                                tmp_pallet_quantity_floor = math.floor(
                                    tmp_pallet_quantity)
                                tmp_pallet_qty = round(
                                    (tmp_pallet_quantity -
                                     tmp_pallet_quantity_floor), 3)
                                free_pallet -= tmp_pallet_qty
                            else:
                                product_uom_qty = (
                                        order_line.product_uom_qty - add_qty)
                                pallet_quantity += 1
                        elif tmp_pallet_qty != 0:
                            pallet_quantity += 1

                        tmp_pallet_quantity = (product_uom_qty /
                                               order_line.product_id.length /
                                               qty_in_pallet)
                        tmp_pallet_quantity_floor = \
                            math.floor(tmp_pallet_quantity)
                        tmp_pallet_qty = round(
                            (tmp_pallet_quantity -
                             tmp_pallet_quantity_floor), 3)
                        pallet_quantity += tmp_pallet_quantity_floor
                    if i == len(check_line) and tmp_pallet_qty:
                        pallet_quantity += 1
                else:
                    for order_line in check_line:
                        product_uom_qty = order_line.product_uom_qty
                        qty_in_pallet = order_line.product_id.qty_in_pallet
                        if order_line.product_id.length and qty_in_pallet:
                            pallet_quantity += math.ceil(
                                product_uom_qty /
                                order_line.product_id.length /
                                qty_in_pallet)
                if order_line_pallet and pallet_quantity:
                    order_line_pallet.with_context(check_count_box=True).write(
                        {'product_uom_qty': pallet_quantity})


                elif order_line_pallet and not pallet_quantity:
                    order_line_pallet.unlink()
                elif not order_line_pallet and pallet_quantity:
                    product_category = self.env['product.category'].search(
                        [('name', '=', 'Wooden Box')])
                    try:
                        product_category = product_category[0]
                    except:
                        pass
                    pallet_product = self.env['product.product'].search(
                        [('categ_id', '=', product_category.id)], limit=1)
                    if self.crm_lead_id and self.crm_lead_id.pallet_product:
                        pallet_product = self.crm_lead_id.pallet_product

                    line_values = self.env[
                        'sale.order.line'].product_id_change(
                        pricelist=self.pricelist_id.id,
                        product=pallet_product.id,
                        qty=pallet_quantity,
                        uom=False, qty_uos=0, uos=False, name='',
                        partner_id=self.partner_id.id, lang=False,
                        update_tax=True, date_order=False, packaging=False,
                        fiscal_position=False, flag=False)
                    values = line_values.get('value')
                    values.update(order_id=self.id,
                                  product_id=pallet_product.id,
                                  name=pallet_product.name)
                    self.env['sale.order.line'].with_context(
                        check_count_box=True).create(values)
        elif self.no_box and not self.manual_box:
            product_category = self.env['product.category'].search(
                [('name', 'in', ('Wooden Box', 'Steel Box'))])
            for sale_order in self:
                for line in sale_order.order_line:
                    if line.product_id.categ_id in product_category:
                        line.state = 'draft'
                        line.unlink()

    @api.cr_uid_ids_context
    def delivery_set(self, cr, uid, ids, context=None):
        line_obj = self.pool.get('sale.order.line')
        grid_obj = self.pool.get('delivery.grid')
        carrier_obj = self.pool.get('delivery.carrier')
        acc_fp_obj = self.pool.get('account.fiscal.position')
        self._delivery_unset(cr, uid, ids, context=context)
        currency_obj = self.pool.get('res.currency')
        line_ids = []
        for order in self.browse(cr, uid, ids, context=context):
            grid_id = carrier_obj.grid_get(
                cr, uid, [order.carrier_id.id], order.partner_shipping_id.id)
            if not grid_id:
                raise osv.except_osv(_('No Grid Available!'),
                                     _('No grid matching for this carrier!'))

            if order.state not in ('draft', 'sent'):
                raise osv.except_osv(_('Order not in Draft State!'),
                                     _('The order state have to be draft '
                                       'to add delivery lines.'))

            grid = grid_obj.browse(cr, uid, grid_id, context=context)

            taxes = grid.carrier_id.product_id.taxes_id.filtered(
                lambda t: t.company_id.id == order.company_id.id)
            fpos = order.fiscal_position or False
            taxes_ids = acc_fp_obj.map_tax(
                cr, uid, fpos, taxes, context=context)
            price_unit = grid_obj.get_price(
                cr, uid, grid.id, order, time.strftime('%Y-%m-%d'), context)
            if order.company_id.currency_id.id != \
                    order.pricelist_id.currency_id.id:
                price_unit = currency_obj.compute(
                    cr, uid, order.company_id.currency_id.id,
                    order.pricelist_id.currency_id.id,
                    price_unit, context=dict(context or {},
                                             date=order.date_order))
            values = {
                'order_id': order.id,
                'name': grid.carrier_id.name,
                'product_uom_qty': 1,
                'product_uom': grid.carrier_id.product_id.uom_id.id,
                'product_id': grid.carrier_id.product_id.id,
                'price_unit': price_unit,
                'tax_id': [(6, 0, taxes_ids)],
                'is_delivery': True,
            }
            res = line_obj.product_id_change(
                cr, uid, [], order.pricelist_id.id, values['product_id'],
                qty=values['product_uom_qty'], uom=False, qty_uos=0, uos=False,
                name='', partner_id=order.partner_id.id, lang=False,
                update_tax=True, date_order=False, packaging=False,
                fiscal_position=False, flag=False, context=None)
            if res['value'].get('purchase_price'):
                values['purchase_price'] = res['value'].get('purchase_price')
            if order.order_line:
                values['sequence'] = order.order_line[-1].sequence + 1
            line_id = line_obj.create(cr, uid, values, context=context)
            line_ids.append(line_id)
        return line_ids

    @api.one
    def calculate_new_price(self, res):
        cr = self.env.cr
        uid = self.env.uid
        context = self.env.context
        sale_order_line_pool = self.pool.get('sale.order.line')
        change_type = res.get('type')

        product_category_ids = self.pool.get('product.category').search(
            cr, SUPERUSER_ID,
            [('name', 'in', ('Wooden Box', 'Steel Box', 'Service Units'))])
        line_ids = []
        for order_line in sale_order_line_pool.browse(
                cr, uid, res.get('line_ids'), context=context):
            if order_line.product_id.categ_id.id not in product_category_ids:
                if not order_line.product_id.extruder:
                    if order_line.product_id.type != 'service':
                        line_ids.append(order_line.id)

        result = []
        if change_type == 'add':
            for line_id in line_ids:
                order_line = sale_order_line_pool.browse(cr, uid, int(line_id),
                                                         context=context)
                sale_order_line_ids = sale_order_line_pool.search(cr, uid, [
                    ('order_id', '=', order_line.order_id.id)], context=context)
                sale_order_lines = sale_order_line_pool.browse(cr, uid,
                                                               sale_order_line_ids,
                                                               context=context)
                change_line = []
                total_amount = 0
                for sale_order_line in sale_order_lines:
                    if sale_order_line.product_id.product_tmpl_id == order_line.product_id.product_tmpl_id \
                            and sale_order_line.product_id.material_id == order_line.product_id.material_id \
                            and sale_order_line.product_id.product_element_width == order_line.product_id.product_element_width \
                            and sale_order_line.product_id.product_element_height == order_line.product_id.product_element_height \
                            and sale_order_line.product_id.product_square == order_line.product_id.product_square:
                        further = True
                        for further_processing in sale_order_line.further_processing_ids:
                            if further_processing not in order_line.further_processing_ids:
                                further = False
                        if further:
                            change_line.append(sale_order_line)
                            total_amount += sale_order_line.product_uom_qty
                result = [change_line]
        if change_type == 'line_change' and res.get('line_ids'):
            for line_id in line_ids:
                order_line = sale_order_line_pool.browse(cr, uid, int(line_id),
                                                         context=context)
                sale_order_line_ids = sale_order_line_pool.search(cr, uid, [
                    ('order_id', '=', order_line.order_id.id)], context=context)
                sale_order_lines = sale_order_line_pool.browse(
                    cr, uid, sale_order_line_ids, context=context)
                change_line = []
                for sale_order_line in sale_order_lines:

                    if check(sale_order_line, order_line):
                        change_line.append(sale_order_line)
                result = [change_line]
        elif change_type == 'line_change' and not line_ids:
            change_type = 'delete'
        if change_type == 'delete':

            sale_order_id = self.session.get('sale_order_id')
            sale_order_lines = self.order_line
            lines = []
            change_lines = []
            for sale_order_line in sale_order_lines:
                if not sale_order_line.product_id.extruder:
                    change_lines.append(sale_order_line)

            for need_line in change_lines:
                change_line = []
                for chan_line in change_lines:
                    if check(chan_line, need_line):
                        change_line.append(chan_line)
                lines.append(sorted(change_line))
            for line in lines:
                if line not in result:
                    result.append(line)
        if change_type == 'dublicate':
            check_order_lines = sale_order_line_pool.browse(
                cr, uid, line_ids, context=context)
            lines = []
            sale_order_lines = self.order_line

            for need_line in check_order_lines:
                change_line = []
                for chan_line in sale_order_lines:
                    if check(chan_line, need_line):
                        change_line.append(chan_line)
                lines.append(sorted(change_line))
            for line in lines:
                if line not in result:
                    result.append(line)
        if change_type == 'option':
            check_order_lines = sale_order_line_pool.browse(
                cr, uid, line_ids, context=context)
            lines = []

            sale_order_lines = self.order_line

            for need_line in check_order_lines:
                change_line = []
                for chan_line in sale_order_lines:
                    if check(chan_line, need_line):
                        change_line.append(chan_line)
                lines.append(sorted(change_line))
            for line in lines:
                if line not in result:
                    result.append(line)
        for res in result:
            all_qty = 0
            for sale_order_line in res:
                if sale_order_line:
                    all_qty += sale_order_line.product_uom_qty
            if res and sale_order_line and sale_order_line.product_id:
                google_values = {
                    'registry': self.pool,
                    'product_product_objects': sale_order_line.product_id,
                    'have_glue': False,
                    'length': sale_order_line.product_id.length,
                    'qty': all_qty,
                    'sale_order_name': sale_order_line.order_id.name,
                    'sale_order_line': sale_order_line,
                }
                google_answer = google_sheet_dublicate(
                    cr, SUPERUSER_ID, google_values, context=context)
                for sale_order_line in res:
                    res = sale_order_line.order_id._cart_update(
                        product_id=int(sale_order_line.product_id.id),
                        line_id=int(sale_order_line.id),
                        add_qty=float(sale_order_line.product_uom_qty),
                        set_qty=float(sale_order_line.product_uom_qty))
                    sale_order_line.write({'minimum_qty':
                        google_answer.get(
                            'minimum_qty')})
                check_extruder_order_line_ids = sale_order_line_pool.search(
                    cr, SUPERUSER_ID,
                    [('order_id', '=', sale_order_line.order_id.id)])
                check_extruder_order_lines = sale_order_line_pool.browse(
                    cr, SUPERUSER_ID, check_extruder_order_line_ids,
                    context=context)
                for extruder_order_line in check_extruder_order_lines:
                    try:
                        if extruder_order_line.product_id \
                                == sale_order_line.product_id.extruder_product_id:
                            line = res = sale_order_line.order_id._cart_update(
                                product_id=int(
                                    extruder_order_line.product_id.id),
                                line_id=int(extruder_order_line.id),
                                add_qty=float(
                                    extruder_order_line.product_uom_qty),
                                set_qty=float(
                                    extruder_order_line.product_uom_qty))
                            sale_order_line_pool.write(
                                cr, SUPERUSER_ID, int(line.get('line_id')),
                                {'price_unit':
                                     google_answer.get('total_tooling_price')})
                    except:
                        pass

        return res

    def onchange_crm_lead_id(self, cr, uid, ids, lead_id, order_line,
                             context=None):
        order_line = []
        lead = self.pool.get('crm.lead').browse(
            cr, uid, lead_id, context=context)
        product_category_ids = self.pool.get('product.category').search(
            cr, SUPERUSER_ID,
            [('name', 'in', ('Wooden Box', 'Steel Box', 'Service Units'))])
        val = {}
        for sale_order in lead.sale_order_ids:
            if sale_order.default_pricelist:
                for line in sale_order.order_line:
                    if line.product_id.categ_id.id not in product_category_ids:
                        order_line.append(
                            {'product_id': line.product_id, 'state': 'draft',
                             'name': line.name,
                             'product_image': line.product_image,
                             'product_uom_qty': line.product_uom_qty,
                             'product_uos_qty': line.product_uos_qty,
                             'product_uom': line.product_uom,
                             'price_unit': line.price_unit,
                             'purchase_price': line.purchase_price,
                             'tax_id': line.tax_id,
                             'short_name': line.short_name,
                             'line_article': line.line_article,
                             'further_processing_ids':
                                 line.further_processing_ids,
                             'logo_for_print': line.logo_for_print,
                             'gluewire': line.gluewire,
                             'color_laser_marking': line.color_laser_marking,
                             'print_logo': line.print_logo,
                             })
        # if not order_line:
        val = {'order_line': order_line}
        return {'value': val}

    @api.multi
    def action_user_confirm(self):
        for order in self:
            order.state = 'user_confirmed'
            order.order_line.write({'state': 'user_confirmed'})

    @api.onchange('date_order')
    def onchange_date_order(self):
        date_order = self.date_order.split(' ')[0]
        date_order = datetime.strptime(
            date_order.replace('-', ''), "%Y%m%d").date()
        if date_order < date.today():
            self.date_order = datetime.now()

    @api.model
    def check_empty_order(self):
        sale_order_pool = self.env['sale.order']
        sale_orders = sale_order_pool.search([('order_line', '=', False),
                                              ('state', '=', 'draft')])
        if sale_orders:
            sale_orders.unlink()

    @api.multi
    def action_invoice_create(self, grouped=False, states=None,
                              date_invoice=False):
        inv_id = super(SaleOrder, self).action_invoice_create(
            grouped=grouped, states=states, date_invoice=date_invoice)
        invoice = self.env['account.invoice'].browse(inv_id)
        invoice.write({'account_invoice_type': self.sale_order_type})
        return inv_id


class ProcurementOrder(models.Model):
    _inherit = "procurement.order"

    @api.cr_uid_ids_context
    def _run_move_create(self, cr, uid, ids, context):
        values = super(ProcurementOrder, self)._run_move_create(
            cr, uid, ids, context)
        if ids.sale_line_id:

            sale_order = ids.sale_line_id.order_id
            if sale_order.partner_safe_stock:

                partner_ids = [sale_order.partner_id.id, ]
                location_id = None
                if sale_order.partner_id.parent_id:
                    partner_ids = [sale_order.partner_id.parent_id.id] + [
                        p.id for p in sale_order.partner_id.parent_id.child_ids]
                if partner_ids:
                    location_id = self.pool['stock.location'].search(
                        cr, uid, [('partner_id', 'in', partner_ids)], limit=1)

                    if not location_id:
                        location_id = sale_order.partner_id.safe_stock_id.id
                    else:
                        try:
                            location_id = location_id[0]
                        except:
                            pass
                if location_id:
                    values['location_id'] = location_id
            values['sale_order_line_id'] = ids.sale_line_id.id
        return values
