# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields, models, api
from openerp import SUPERUSER_ID
from openerp.addons.web.http import request


class PrintPrice(models.Model):
    _name = "print.pricelist"

    product_ids = fields.Many2many('product.template',
                                   'rel_print_product_template',
                                   'print_id',
                                   'product_id',
                                   string="Product", )
    category_ids = fields.Many2many('product.public.category',
                                    'rel_print_public_category',
                                    'print_id',
                                    'category_id',
                                    string="Product", )
    filter = fields.Char(string="Filter")
    image = fields.Binary(string='Image')
