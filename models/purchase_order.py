# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-Today Libre Comunication (<erpsystem.com.ua@gmail.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields, models, api
from datetime import date, datetime
import time
import httplib2
from apiclient import discovery
from oauth2client.service_account import ServiceAccountCredentials
from openerp import SUPERUSER_ID
from openerp.addons.thermevo_shop.google_script \
    import google_sheet_dublicate_po

from openerp.addons.thermevo_shop.check_line_method \
    import check
import logging

_logger = logging.getLogger(__name__)


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    @api.multi
    def _get_last_message(self):
        mail_message_pool = self.env['mail.message']
        for order in self:
            mail_message = mail_message_pool.search(
                [('model', '=', self._name),
                 ('res_id', '=', order.id),
                 ('type', '=', 'comment')])
            value = {}
            if mail_message:
                mail_message = mail_message[0]

                order.last_comment_id = mail_message.id
                order.last_comment = mail_message.body[0:50].replace('  ', ' ')
                value['last_comment'] = mail_message.body[0:50].replace(
                    '  ', ' ')
            if order.purchase_confirmation_document:
                confirmation_document_state = 'upload'
            else:
                create_date = order.create_date.split(' ')[0]
                create_date = datetime.strptime(
                    create_date.replace('-', ''), "%Y%m%d").date()
                new_date = date.today()
                days = (new_date - create_date).days
                if days > 2:
                    confirmation_document_state = 'miss'
                else:
                    confirmation_document_state = 'new'
            if confirmation_document_state:
                value['confirmation_document_state'] = \
                    confirmation_document_state
            if value:
                order.write(value)

    last_comment_id = fields.Many2one('mail.message',
                                      compute='_get_last_message',
                                      string="Last Comment ID")
    last_comment = fields.Text(string="Last Comment")

    purchase_confirmation_document = fields.Many2one(
        'ir.attachment',
        string='Confirmation Document')

    confirmation_document_state = fields.Selection([
        ('new', 'New'),
        ('upload', 'Upload'),
        ('miss', 'Miss')],
        string='Confirmation Document State')

    delivery_document = fields.One2many('purchase.delivery.document',
                                        'purchase_order_id',
                                        string='Delivery Document')

    invoice_document = fields.One2many('purchase.invoice.document',
                                       'purchase_order_id',
                                       string='Invoice Document')

    date_confirmation = fields.Date(string="Date Confirmation")
    partner_safe_stock = fields.Boolean(string="Safe Stock",
                                        default=False)

    @api.multi
    def write(self, values):
        if values.get('delivery_document'):
            for order in self:
                partner_ref = u', '.join(
                    document.attachment_id.name.split('.')[0]
                    for document in order.delivery_document)
                values['partner_ref'] = partner_ref
        return super(PurchaseOrder, self).write(values)

    @api.multi
    def re_calculate_time(self):

        config_parameter = self.env['ir.config_parameter'].search(
            [('key', '=', 'credentials_file')])
        CREDENTIALS_FILE = config_parameter.value

        # CREDENTIALS_FILE = '/home/rokealva/Production/thermevo/odoo-8.0/openerp/addons/thermevo_uys/static/thermevo-63a1fc53c8ad.json'
        # CREDENTIALS_FILE = '/home/odoo/openerp/addons/thermevo_uys/static/thermevo-63a1fc53c8ad.json'
        # CREDENTIALS_FILE = '/home/odoo/thermevo_uys/static/thermevo-63a1fc53c8ad.json'
        credentials = ServiceAccountCredentials.from_json_keyfile_name(
            CREDENTIALS_FILE, ['https://www.googleapis.com/auth/spreadsheets',
                               'https://www.googleapis.com/auth/drive'])
        httpAuth = credentials.authorize(httplib2.Http())
        service = discovery.build('sheets', 'v4', http=httpAuth)
        production_scheme_config_pool = self.env['production.scheme.config']
        production_scheme_config_object = \
            production_scheme_config_pool.search([])

        path_1 = u'{page}!B12:F12'.format(
            page=production_scheme_config_object.range_names)
        path_2 = u'{page}!M12:S12'.format(
            page=production_scheme_config_object.range_names)
        for line in self.order_line:
            value_1 = [[
                line.product_id.default_code,
                line.product_qty,
                line.product_id.stege_breite,
                line.product_id.line_type_id.name,
                line.product_id.line_type_id.maintenance_order
            ]]
            value_2 = [[
                self.id,
                line.id,
                self.state,
                'заказ и т.о.',
                '',
                '',
                'да',
            ]]
            service.spreadsheets().values().batchUpdate(
                spreadsheetId=production_scheme_config_object.spreadsheet_id,
                body={
                    "valueInputOption": "USER_ENTERED",
                    "data": [
                        {"range": path_1,
                         "majorDimension": "ROWS",
                         "values": value_1,
                         },
                        {"range": path_2,
                         "majorDimension": "ROWS",
                         "values": value_2,
                         },
                    ]
                }).execute()
            time.sleep(2)
            result = service.spreadsheets().values().get(
                spreadsheetId=production_scheme_config_object.spreadsheet_id,
                range=production_scheme_config_object.range_names).execute()
            values = result.get('values', [])
            if values and len(values) > 10:
                date = values[10][9]
                date_planned = datetime.strptime(
                    date.split(' ')[0].replace('.', ''), "%d%m%y").date()
                line.date_planned = date_planned

                sale_order_id = None
                try:
                    sale_order_id = line.orer_id.sale_order_id
                except:
                    pass
                if sale_order_id:
                    sale_order_id.write({'minimum_planned_date': date_planned})

    def button_dummy(self, cr, uid, ids, context=None):
        product_category_ids = self.pool.get(
            'product.category').search(
            cr, uid, [('name', 'in',
                       ('Wooden Box', 'Steel Box', 'Service Units'))])
        super(PurchaseOrder, self).button_dummy(
            cr, uid, ids, context=context)

        purchase_order = self.browse(cr, uid, ids, context=context)
        line_blocks = []
        check_lines = []
        for line in purchase_order.order_line:
            lines = []
            if line not in check_lines \
                    and not line.product_id.extruder \
                    and line.product_id.categ_id.id \
                            not in product_category_ids:

                check_lines.append(line)
                line_blocks.append(line)
                for check_purchase_line in purchase_order.order_line:
                    if check(check_purchase_line.sale_order_line_id,
                             line.sale_order_line_id) \
                            and check_purchase_line not in lines:
                        check_lines.append(check_purchase_line)
                        line_blocks.append(check_purchase_line)
        for line_block in line_blocks:
            all_qty = 0
            for line in line_block:
                all_qty += line.product_qty
            fix_line = None
            for partner_line in purchase_order.partner_id.fixed_product_id:
                result = check(partner_line, line)
                if result:
                    fix_line = partner_line
            if not fix_line and purchase_order.partner_id.parent_id:
                for partner_line in purchase_order.partner_id.parent_id.fixed_product_id:
                    result = check(partner_line, line.sale_order_line_id)
                    if result:
                        fix_line = partner_line
            if fix_line:
                line.price_unit = fix_line.price_unit
            else:
                google_values = {
                    'registry': self.pool,
                    'product_product_objects': line.product_id,
                    'have_glue': False,
                    'length': line.product_id.length,
                    'qty': all_qty,
                    'sale_order_name': line.order_id.name,
                    'sale_order_line': line.sale_order_line_id,
                }
                google_answer = google_sheet_dublicate_po(
                    cr, SUPERUSER_ID, google_values, context=context)

                for line in line_block:
                    line.price_unit = google_answer.get('price')

                for purchase_line in purchase_order.order_line:
                    if line_block \
                            and purchase_line.product_id.extruder \
                            and (line_block[
                                     0].product_id.extruder_product_id.id
                                     == purchase_line.product_id.id):
                        purchase_line.price_unit = google_answer.get(
                            'final_tooling_price')
            if line.sale_order_line_id:
                line.sale_order_line_id.purchase_price = line.price_unit

        return True


class PurchaseOrderLine(models.Model):
    _inherit = "purchase.order.line"

    sale_order_line_id = fields.Many2one('sale.order.line',
                                         string="Sale Order Line")
    further_processing_ids = fields.Many2many(
        "further.processing",
        'further_processing_line_rel',
        'further_processing_id',
        'line_id',
        string="Further Processing")
    e_low_open_groove = fields.Boolean('Allow Open Groove Gluewire')
    gluewire = fields.Boolean('Gluewire')
    color_laser_marking = fields.Selection([('no', 'No'),
                                            ('grey', 'Grey'),
                                            ('white', 'White')],
                                           string='Color Laser Marking',
                                           default="no")
    print_logo = fields.Boolean('Print Logo')


class PurchaseDeliveryDocument(models.Model):
    _name = "purchase.delivery.document"

    purchase_order_id = fields.Many2one("purchase.order", 'Purchase Order')
    attachment_id = fields.Many2one("ir.attachment", 'Delivery Document')
    scheduled_date = fields.Date(string="Scheduled Date")

    def default_get(self, cr, uid, fields, context=None):
        res = super(PurchaseDeliveryDocument, self).default_get(
            cr, uid, fields, context=context)
        purchase_id = context.get('purchase_id')
        res['purchase_order_id'] = purchase_id
        return res


class PurchaseInvoiceDocument(models.Model):
    _name = "purchase.invoice.document"

    purchase_order_id = fields.Many2one("purchase.order", 'Purchase Order')
    attachment_id = fields.Many2one("ir.attachment", 'Invoice Document')
    scheduled_date = fields.Date(string="Scheduled Date")
