from openerp import models, api


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.multi
    def write(self, values):
        # Change qty in sale_order_line
        if values.get('move_lines'):
            for line in values['move_lines']:
                if line[2]:
                    stock_move_pool = self.env['stock.move']
                    so_line = stock_move_pool.browse(line[1]).sale_order_line_id

                    stock_move_ids = stock_move_pool.search(
                        [('sale_order_line_id', '=', so_line.id)])

                    done_moves = stock_move_ids.filtered(lambda item:
                                                         item.state == 'done')

                    so_line.write(dict(
                        product_uom_qty=sum([x.product_uom_qty
                                             for x in done_moves]) +
                                        line[2].get('product_uom_qty')
                    ))

        return super(StockPicking, self).write(values)
