# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields, models, api
import openerp.addons.decimal_precision as dp


class ResPartner(models.Model):
    _inherit = "res.partner"

    fixed_product_id = fields.One2many('partner.fixed.product',
                                       'partner_id',
                                       string="Fixed Products")
    supplier_stock_information_ids = fields.One2many(
        'supplier.stock.information',
        'partner_id',
        string="Stock Information")
    safe_stock_id = fields.Many2one('stock.location',
                                    string='Partner Safe Stock')


class PartnerFixedProduct(models.Model):
    _name = "partner.fixed.product"

    partner_id = fields.Many2one('res.partner',
                                 'Partner', )
    product_id = fields.Many2one('product.product',
                                 'Product', )
    further_processing_ids = fields.Many2many(
        "further.processing",
        'further_processing_line_rel',
        'further_processing_id',
        'line_id',
        string="Further Processing")
    e_low_open_groove = fields.Boolean('Allow Open Groove Gluewire')
    gluewire = fields.Boolean('Gluewire')
    color_laser_marking = fields.Selection([('no', 'No'),
                                            ('grey', 'Grey'),
                                            ('white', 'White')],
                                           string='Color Laser Marking',
                                           default="no")
    print_logo = fields.Boolean('Print Logo')
    price_unit = fields.Float(string='Unit Price',
                              required=True,
                              digits_compute=dp.get_precision('Payment Term'))


class SupplierStockInformation(models.Model):
    _name = "supplier.stock.information"

    partner_id = fields.Many2one('res.partner',
                                 string='Partner')
    product_id = fields.Many2one('product.product',
                                 string='Product', )
    further_processing_ids = fields.Many2many(
        "further.processing",
        'further_processing_line_rel',
        'further_processing_id',
        'line_id',
        string="Further Processing")
    e_low_open_groove = fields.Boolean(string='Allow Open Groove Gluewire')
    gluewire = fields.Boolean(string='Gluewire')
    color_laser_marking = fields.Selection([('no', 'No'),
                                            ('grey', 'Grey'),
                                            ('white', 'White')],
                                           string='Color Laser Marking',
                                           default="no")
    print_logo = fields.Boolean('Print Logo')
    product_qty = fields.Float(string='Product Qty',
                               required=True)
    parent_id = fields.Many2one('supplier.stock.information',
                                string='Parent')
    state = fields.Selection([('new', 'New'),
                              ('reserved', 'Reserved')],
                             string='State',
                             default="new")
    reserved_partner_id = fields.Many2one('res.partner',
                                          string='Reserved to partner')
    date_upload = fields.Datetime(string='Upload Date')
    reserved_date = fields.Datetime(string='Reserved Date')
    sale_order_id = fields.Many2one('sale.order',
                                    string='Sale order')

