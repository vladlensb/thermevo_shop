# -*- coding: UTF-8 -*-

##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields, models, api


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    account_invoice_type = fields.Selection(
        selection=[('product', 'Product')],
        string='Invoice Type', translate=True)

    @api.multi
    def print_null_invoice(self):
        assert len(self) == 1
        res = self.env['report'].get_action(
            self, 'thermevo_shop.invoice_null_doc')
        return res



class account_tax(models.Model):
    _inherit = "account.tax"

    @api.cr_uid
    def _fix_tax_included_price(self, cr, uid, price, prod_taxes, line_taxes):
        incl_tax = [tax for tax in prod_taxes
                    if tax.id not in line_taxes and tax.price_include]
        if incl_tax:
            return self._unit_compute_inv(
                cr, uid, incl_tax, price)[0]['price_unit']
        return price
